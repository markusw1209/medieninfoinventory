import { ForgottenPasswordDialogComponent } from './../forgotten-password-dialog/forgotten-password-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, AbstractControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  public isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public hidePassword = true;
    public loginForm: FormGroup;
    public isLoggedIn = false;
    public rememberUsername = false;
    public errorStateMatcher = new MyErrorStateMatcher();

    constructor(
        public authenticationService: AuthenticationService,
        public matDialog: MatDialog,
        private router: Router,
        private _snackBar: MatSnackBar,
    ) {}

    public ngOnInit(): void {
        this.createForm();
        this.fetchUsername();
    }

    public login(): void {
        this.authenticationService.login({ username: this.username.value, password: this.password.value }).subscribe(
        (authResult) => {
            this.saveUsername();
            this.authenticationService.setSession(authResult);
            this.router.navigateByUrl('/main-page');
        },
        (err) => {
            this.saveUsername();
            if (err.status === 401) {
            this._snackBar.open('Fehler bei der Anmeldung: Die Benutzerdaten sind nicht korrekt', '', { duration: 3000 });
            }
        }
        );
    }

    private saveUsername(): void {
        if (this.rememberUsername) {
            localStorage.setItem('rememberedUsername', this.username.value);
        } else {
            localStorage.removeItem('rememberedUsername');
        }
    }

    private fetchUsername(): void {
        if (localStorage.getItem('rememberedUsername')) {
            this.loginForm.patchValue({ usernameFormControl: localStorage.getItem('rememberedUsername') });
            this.rememberUsername = true;
        }
    }

    private createForm(): void {
        this.loginForm = new FormGroup({
        usernameFormControl: new FormControl('', {
            validators: [Validators.required, Validators.minLength(2), Validators.maxLength(20)]
        }),
        passwordFormControl: new FormControl('', {
            validators: [Validators.required, Validators.minLength(8), Validators.maxLength(30)]
        }),
        rememberUsernameFormControl: new FormControl('')
        });
    }

    public openForgottenPasswordDialog(): void {
        const matDialogConfig: MatDialogConfig = {
            disableClose: true
        };
        const dialogRef = this.matDialog.open(ForgottenPasswordDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe(() => {
        });
    }

    get username(): AbstractControl {
        return this.loginForm.get('usernameFormControl');
    }

    get password(): AbstractControl {
        return this.loginForm.get('passwordFormControl');
    }
}
