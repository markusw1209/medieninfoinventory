import { NgModule } from '@angular/core';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from '../shared/shared.module';
import { ForgottenPasswordDialogComponent } from './forgotten-password-dialog/forgotten-password-dialog.component';

@NgModule({
  declarations: [LoginComponent, ForgottenPasswordDialogComponent],
  imports: [AuthenticationRoutingModule, SharedModule]
})
export class AuthenticationModule { }
