import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export function matchValidator(firstFormControlName: string, secondFormControlName: string): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
        const control1 = group.controls[firstFormControlName];
        const control2 = group.controls[secondFormControlName];

        control2.setErrors(null);

        if (control1.invalid && control2.dirty && control2.value !== null && control2.value !== undefined && control2.value !== '') {
            control2.setErrors({ missingFirst: true });
            return;
        }

        if (control1.value !== control2.value) {
            control2.setErrors({ doesNotMatch: true });
        }

        return;
    }
}
