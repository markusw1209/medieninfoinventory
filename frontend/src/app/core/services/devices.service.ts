import { environment } from '../../../environments/environment';
import { Device } from '../interfaces/IDevice';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

    private apiUrl: string;
    private apiPort: number;

    constructor(private http: HttpClient) {
        this.apiPort = environment.apiPort;
        this.apiUrl = environment.apiUrl;
    }

    public createDevice(deviceDto: Device): Observable<any> {
        return this.http.post(this.apiUrl + ':' + this.apiPort + '/web/createDevice', deviceDto, { responseType: 'json' });
    }

    public deleteDevice(deviceDto: Device): Observable<any> {
        return this.http.post(this.apiUrl + ':' + this.apiPort + '/web/deleteDevice', deviceDto, { responseType: 'json' });
    }

    public getDevices(): Observable<any> {
        return this.http.get(this.apiUrl + ':' + this.apiPort + '/web/getDevices', { responseType: 'json' });
    }
}