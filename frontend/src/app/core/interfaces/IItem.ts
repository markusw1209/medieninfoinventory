
export interface Item {
    id: number;
    tagId: string;
    name: string;
    numberExisting: number;
    numberRent: number;
    description: string;
    rent: boolean;
    active: boolean;
    blocked: boolean;
    addDate: Date;
    removeDate: Date;
}
