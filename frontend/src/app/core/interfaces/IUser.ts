
export interface User {
    id: number;
    cardId: string;
    firstName: string;
    lastName: string;
    role: string;
    matrikel: string;
    mail: string;
    admin: boolean;
    approved: boolean;
    active: boolean;
    lastSeen: Date;
    username: string;
    password: string;
}
