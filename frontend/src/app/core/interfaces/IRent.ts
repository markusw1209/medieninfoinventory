
export interface Rent {
    id: number;
    item: string;
    itemId: number;
    user: string;
    rentDate: Date;
    returnDate: Date;
    extensions: number;
}
