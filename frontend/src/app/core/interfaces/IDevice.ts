
export interface Device {
    id: number;
    name: string;
    mac: string;
    key: string;
    lastSeen: Date;
}
