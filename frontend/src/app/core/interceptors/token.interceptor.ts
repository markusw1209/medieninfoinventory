import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const jwtToken = sessionStorage.getItem('jwt_token');

    if (jwtToken) {
      const cloned = request.clone({
        setHeaders: {
          Authorization: `Bearer ` + jwtToken
        }
      });
      return next.handle(cloned);
    } else {
      return next.handle(request);
    }
  }
}
