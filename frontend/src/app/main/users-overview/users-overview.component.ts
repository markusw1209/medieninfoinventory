import { environment } from './../../../environments/environment';
import { User } from './../../core/interfaces/IUser';
import { EventEmitterService } from '../main-page-services/event-emitter.service';
import { UserService } from './../../core/services/user.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
  selector: 'app-users-overview',
  templateUrl: './users-overview.component.html',
  styleUrls: ['./users-overview.component.css']
})
export class UsersOverviewComponent implements OnInit, OnDestroy {
    private isAlive: boolean;
    public users: MatTableDataSource<User> = new MatTableDataSource<User>();
    public displayedColumns: string[] = [
        'id',
        'cardId',
        'firstName',
        'lastName',
        'role',
        'matrikel',
        'admin',
        'approved',
        'lastSeen',
        'mail',
        'userHistory',
        'editUser',
        'deleteUser'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<UsersOverviewComponent>,
        private eventEmitterService: EventEmitterService,
        public matDialog: MatDialog,
        private userService: UserService,
    ) {
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadAllUsers();
        this.setAutoRefresh();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadAllUsers(): void {
        this.userService
            .getAllUsers()
            .pipe(
                map((users: User[]) => {
                    this.users.data = users;
                    this.users.sort = this.sort;
                    this.users.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.users.filter = filterValue.trim().toLowerCase();

        if (this.users.paginator) {
            this.users.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
            this.loadAllUsers();
            });
    }

    public openEditUserDialog(user: User): void {
        this.eventEmitterService.onEditUserClick(user);
    }

    public openDeleteUserDialog(user: User): void {
        this.eventEmitterService.onDeleteUserClick(user);
    }

    public openUserHistoryOverview(user: User): void {
        this.eventEmitterService.onOpenUserHistoryClick(user);
    }
}
