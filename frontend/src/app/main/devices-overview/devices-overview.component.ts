import { environment } from './../../../environments/environment';
import { DevicesService } from '../../core/services/devices.service';
import { Device } from './../../core/interfaces/IDevice';
import { EventEmitterService } from './../main-page-services/event-emitter.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
  selector: 'app-devices-overview',
  templateUrl: './devices-overview.component.html',
  styleUrls: ['./devices-overview.component.css']
})
export class DevicesOverviewComponent implements OnInit, OnDestroy {

    private isAlive: boolean;
    public devices: MatTableDataSource<Device> = new MatTableDataSource<Device>();
    public displayedColumns: string[] = [
        'id',
        'name',
        'mac',
        'lastSeen',
        'delete'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<DevicesOverviewComponent>,
        private eventEmitterService: EventEmitterService,
        public matDialog: MatDialog,
        private devicesService: DevicesService
    ) {
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadDevices();
        this.setAutoRefresh();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadDevices(): void {
        this.devicesService
            .getDevices()
            .pipe(
                map((devices: Device[]) => {
                    this.devices.data = devices;
                    this.devices.sort = this.sort;
                    this.devices.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.devices.filter = filterValue.trim().toLowerCase();

        if (this.devices.paginator) {
            this.devices.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
            this.loadDevices();
            });
    }

    public openDeleteDeviceDialog(device: Device): void {
        this.eventEmitterService.onDeleteDeviceClick(device);
    }

    public openNewDeviceDialog(): void {
        this.eventEmitterService.onNewDeviceClick();
    }
}
