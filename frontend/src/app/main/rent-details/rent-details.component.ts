import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Rent } from './../../core/interfaces/IRent';
import { Component, OnInit, Inject } from '@angular/core';
import * as moment from 'moment';
moment.locale('de');

@Component({
  selector: 'app-rent-details',
  templateUrl: './rent-details.component.html',
  styleUrls: ['./rent-details.component.css']
})
export class RentDetailsComponent implements OnInit {

    public rent: Rent;

    constructor(
        public dialogRef: MatDialogRef<RentDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Rent
    ) {
        this.rent = data;
    }

    public ngOnInit(): void {}
}
