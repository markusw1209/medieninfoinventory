import { Item } from './../../core/interfaces/IItem';
import { InventoryService } from './../../core/services/inventory.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-edit-item-dialog',
  templateUrl: './edit-item-dialog.component.html',
  styleUrls: ['./edit-item-dialog.component.css']
})
export class EditItemDialogComponent implements OnInit {

    public editItemForm: FormGroup;
    public item: Item;

    constructor(
        public dialogRef: MatDialogRef<EditItemDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Item,
        private formBuilder: FormBuilder,
        private readonly inventoryService: InventoryService
    ) {
        this.item = data;
    }

    public ngOnInit(): void {
        this.createDataForm();
    }

    public changeData(): void {
        const itemDto: Item = {
            id: this.item.id,
            tagId: this.item.tagId,
            name: this.name.value,
            numberExisting: this.numberExisting.value,
            numberRent: this.item.numberRent,
            description: this.description.value,
            rent: this.item.rent,
            active: true,
            blocked: this.blocked.value,
            addDate: this.item.addDate,
            removeDate: this.item.removeDate
        };

        this.inventoryService.setItemData(itemDto).subscribe((res) => {
            this.dialogRef.close(res);
        });
    }

    private createDataForm(): void {
        this.editItemForm = this.formBuilder.group({
            nameFormControl: new FormControl(this.item.name, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(50),
                Validators.pattern('[^<>{}();]*')
            ]),
            descriptionFormControl: new FormControl(this.item.description, [
                Validators.maxLength(250),
                Validators.pattern('[^<>{}();]*')
            ]),
            blockedFormControl: new FormControl(this.item.blocked),
            numberExistingFormControl: new FormControl(this.item.numberExisting, [
                Validators.max(256),
                Validators.min(1),
                Validators.min(this.item.numberRent)
            ]),
        });
    }

    public formUnchanged(): boolean {
        return (
            this.name.value === this.item.name &&
            this.description.value === this.item.description &&
            this.blocked.value === this.item.blocked &&
            this.numberExisting.value === this.item.numberExisting
        );
    }

    get name(): AbstractControl {
        return this.editItemForm.get('nameFormControl');
    }
    get description(): AbstractControl {
        return this.editItemForm.get('descriptionFormControl');
    }
    get blocked(): AbstractControl {
        return this.editItemForm.get('blockedFormControl');
    }
    get numberExisting(): AbstractControl {
        return this.editItemForm.get('numberExistingFormControl');
    }
}
