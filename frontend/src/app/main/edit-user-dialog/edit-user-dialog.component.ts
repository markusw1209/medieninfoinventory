import { User } from './../../core/interfaces/IUser';
import { UserService } from './../../core/services/user.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, AbstractControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {

    public adminUserForm: FormGroup;
    public editUserForm: FormGroup;
    public user: User;
    public roles: string[];
    public isAlreadyAdmin: boolean;
    public hidePassword: boolean;

    public locationTooltip = 'Gibt den Ort an, an dem sich der Reader befindet';
    public customerTooltip = 'Einer ihrer Kunden';
    public expireTimeTooltip = 'Gibt die Zeit in ms an, die vergehen muss, bis von diesem Reader gelesene Tags als nicht mehr aktiv markiert wird';
    public activationTimeTooltip = 'Gibt die Zeit in ms an, die vergangen sein muss, bis ein gelesenes Tag als sicher gelesen markiert wird';
    public heartbeatIntervalTooltip = 'Gibt den Zeitintervall in ms zwischen Heartbeats des Readers an';
    public messageIntervalTooltip = 'Gibt Zeitintervall in ms zwischen Nachrichten des Readers an';
    public minimumReadsTooltip = 'Gibt die minimale Anzahl von Scans an, die ein Tag erreicht haben muss, um als sicher erkannt akzeptiert zu werden. '
        + 'Min: 1, Max: 500';
    public minimumRSSITooltip = 'Gibt den minimalen RSSI-Wert an, den ein Tag erreicht haben muss, damit es als sicher erkannt akzeptiert wird. '
        + 'Min: 1, Max: 50';
    public firstNameTooltip = 'Hallo';
    public usernameTooltip = 'Der Nutzername ist nur für Administratoren erforderlich.';

    constructor(
        public dialogRef: MatDialogRef<EditUserDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: User,
        private formBuilder: FormBuilder,
        private readonly userService: UserService
    ) {
        this.hidePassword = true;
        this.user = data;
        this.roles = ['STUDENT', 'DOZENT'];
    }

    public ngOnInit(): void {
        this.isAlreadyAdmin = this.user.admin;
        this.createDataForm(this.user.admin);
    }

    public changeData(): void {
        const userDto: User = {
            username: this.username.value,
            id: this.user.id,
            cardId: this.user.cardId,
            firstName: this.firstName.value,
            lastName: this.lastName.value,
            role: this.role.value,
            matrikel: this.matrikel.value,
            mail: this.mail.value,
            admin: this.admin.value,
            approved: this.approved.value,
            active: this.user.active,
            lastSeen: this.user.lastSeen,
            password: this.user.password
        };

        if (this.password.value !== '') {
            userDto.password = this.password.value;
        }

        this.userService.setUserData(userDto).subscribe((res) => {
            this.dialogRef.close(res);
        });

    }

    private createDataForm(isAdmin: boolean): void {
        this.editUserForm = this.formBuilder.group({
            alreadyAdminFormControl: new FormControl(this.isAlreadyAdmin),
            firstNameFormControl: new FormControl(this.user.firstName, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z\-\s\u00c4\u00e4\u00d6\u00f6\u00dc\u00fc\u00df ]*')
            ]),
            lastNameFormControl: new FormControl(this.user.lastName, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z\-\s\u00c4\u00e4\u00d6\u00f6\u00dc\u00fc\u00df ]*')
            ]),
            roleFormControl: new FormControl(this.user.role),
            matrikelFormControl: new FormControl(this.user.matrikel, [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(8),
                Validators.pattern('[0-9]{6,8}')
            ]),
            mailFormControl: new FormControl(this.user.mail, [
                Validators.required,
                Validators.email,
                Validators.maxLength(80),
                Validators.pattern('[^<>{}();]*')
            ]),
            adminFormControl: new FormControl(isAdmin),
            approvedFormControl: new FormControl(this.user.approved)
        }, {});
    
        this.adminUserForm = this.formBuilder.group({
            usernameFormControl: new FormControl({ value: this.user.username, disabled: !isAdmin }, [
                Validators.minLength(8),
                Validators.maxLength(50),
                Validators.pattern('[^<>{}();]*')
            ]),
            passwordFormControl: new FormControl({ value: '', disabled: !isAdmin }, [
                Validators.minLength(8),
                Validators.maxLength(32),
                Validators.pattern('[^<>{}();]*')
            ]),
            passwordAgainFormControl: new FormControl({ value: '', disabled: !isAdmin }, [
                Validators.minLength(8),
                Validators.maxLength(32),
                Validators.pattern('[^<>{}();]*')
            ])
        }, { validators: [ this.passwordsEqualValidator, this.adminDataValidator ] });
    }

    public enableAdminFields(checkedStatus: boolean): void {
        this.createDataForm(checkedStatus);
    }

    public formUnchanged(): boolean {
        return (
            this.username.value === this.user.username &&
            this.password.value === '' &&
            this.passwordAgain.value === '' &&
            this.firstName.value === this.user.firstName &&
            this.lastName.value === this.user.lastName &&
            this.role.value === this.user.role &&
            this.matrikel.value === this.user.matrikel &&
            this.mail.value === this.user.mail &&
            this.admin.value === this.user.admin &&
            this.approved.value === this.user.approved
        );
    }

    get username(): AbstractControl {
        return this.adminUserForm.get('usernameFormControl');
    }
    get password(): AbstractControl {
        return this.adminUserForm.get('passwordFormControl');
    }
    get passwordAgain(): AbstractControl {
        return this.adminUserForm.get('passwordAgainFormControl');
    }
    get firstName(): AbstractControl {
        return this.editUserForm.get('firstNameFormControl');
    }
    get lastName(): AbstractControl {
        return this.editUserForm.get('lastNameFormControl');
    }
    get role(): AbstractControl {
        return this.editUserForm.get('roleFormControl');
    }
    get matrikel(): AbstractControl {
        return this.editUserForm.get('matrikelFormControl');
    }
    get mail(): AbstractControl {
        return this.editUserForm.get('mailFormControl');
    }
    get admin(): AbstractControl {
        return this.editUserForm.get('adminFormControl');
    }
    get approved(): AbstractControl {
        return this.editUserForm.get('approvedFormControl');
    }
    get alreadyAdmin(): AbstractControl {
        return this.editUserForm.get('alreadyAdminFormControl');
    }

    passwordsEqualValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        const password = control.get('passwordFormControl');
        console.log(password.value)
        const passwordAgain = control.get('passwordAgainFormControl');
        console.log(passwordAgain.value);
        console.log(password && passwordAgain && password.value === passwordAgain.value);
        return password && passwordAgain && password.value === passwordAgain.value ? null : { passwordsNotEqual: true };
    };
    
    adminDataValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
        const password = control.get('passwordFormControl');
        const passwordAgain = control.get('passwordAgainFormControl');
        const username = control.get('usernameFormControl');
        const isAdmin = this.editUserForm.get('adminFormControl');
        const alreadyAdmin = this.editUserForm.get('alreadyAdminFormControl');
        if (isAdmin && !isAdmin.value) { return null; }
        if (isAdmin && isAdmin.value && alreadyAdmin && alreadyAdmin.value) { return null; }
        if (isAdmin && isAdmin.value && alreadyAdmin && !alreadyAdmin.value && password
            && password.value !== '' && passwordAgain && passwordAgain.value !== '' && username && username.value !== '') { return null; }
        return { adminDataValidationError: true };
    };
}


