import { DevicesService } from './../../core/services/devices.service';
import { Device } from './../../core/interfaces/IDevice';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-new-device-dialog',
  templateUrl: './new-device-dialog.component.html',
  styleUrls: ['./new-device-dialog.component.css']
})
export class NewDeviceDialogComponent implements OnInit {

    public newDeviceForm: FormGroup;
    public device: Device;
    public hidePassword: boolean;
    public hidePasswordAgain: boolean;

    constructor(
        public dialogRef: MatDialogRef<NewDeviceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Device,
        private formBuilder: FormBuilder,
        private readonly devicesService: DevicesService
    ) {
        this.device = data;
    }

    public ngOnInit(): void {
        this.hidePassword = true;
        this.hidePasswordAgain = true;
        this.createDataForm();
    }

    public changeData(): void {
        const deviceDto: Device = {
            id: null,
            name: this.name.value,
            mac: this.mac.value,
            lastSeen: null,
            key: this.key.value
        };

        this.devicesService.createDevice(deviceDto).subscribe((res) => {
            this.dialogRef.close(res);
        });
    }

    private createDataForm(): void {
        this.newDeviceForm = this.formBuilder.group({
            nameFormControl: new FormControl(null, [
                Validators.required,
                Validators.maxLength(30),
                Validators.pattern('[^<>{}();]*')
            ]),
            macFormControl: new FormControl(null, [
                Validators.required,
                Validators.minLength(17),
                Validators.maxLength(17),
                Validators.pattern('([0-9abcdefABCDEF]{2}:){5}[0-9abcdefABCDEF]{2}')
            ]),
            keyFormControl: new FormControl(null, [
                Validators.required,
                Validators.minLength(16),
                Validators.maxLength(32),
                Validators.pattern('[^<>{}();]*')
            ]),
            keyAgainFormControl: new FormControl(null, [
                Validators.required,
                Validators.minLength(16),
                Validators.maxLength(32),
                Validators.pattern('[^<>{}();]*')
            ])
        } ,{ validators: keysEqualValidator });
    }
    get name(): AbstractControl {
        return this.newDeviceForm.get('nameFormControl');
    }
    get mac(): AbstractControl {
        return this.newDeviceForm.get('macFormControl');
    }
    get key(): AbstractControl {
        return this.newDeviceForm.get('keyFormControl');
    }
    get keyAgain(): AbstractControl {
        return this.newDeviceForm.get('keyAgainFormControl');
    }
}

export const keysEqualValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const key = control.get('keyFormControl');
    const keyAgain = control.get('keyAgainFormControl');
    return key && keyAgain && key.value === keyAgain.value ? null : { keysNotEqual: true };
};
