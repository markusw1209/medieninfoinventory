import { environment } from './../../../environments/environment';
import { Item } from './../../core/interfaces/IItem';
import { Rent } from './../../core/interfaces/IRent';
import { RentsService } from './../../core/services/rents.service';
import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
  selector: 'app-rent-history-overview',
  templateUrl: './rent-history-overview.component.html',
  styleUrls: ['./rent-history-overview.component.css']
})
export class RentHistoryOverviewComponent implements OnInit, OnDestroy{

    private isAlive: boolean;
    public item: Item;
    public rents: MatTableDataSource<Rent> = new MatTableDataSource<Rent>();
    public displayedColumns: string[] = [
        'id',
        'user',
        'rentDate',
        'returnDate',
        'extensions'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<RentHistoryOverviewComponent>,
        public matDialog: MatDialog,
        private rentsService: RentsService,
        @Inject(MAT_DIALOG_DATA) public data: Item,
    ) {
        this.item = data;
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadRents();
        this.setAutoRefresh();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadRents(): void {
        this.rentsService
            .getRentHistory(this.item.id)
            .pipe(
                map((rents: Rent[]) => {
                    this.rents.data = rents;
                    this.rents.sort = this.sort;
                    this.rents.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.rents.filter = filterValue.trim().toLowerCase();

        if (this.rents.paginator) {
            this.rents.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
                this.loadRents();
            });
    }
}
