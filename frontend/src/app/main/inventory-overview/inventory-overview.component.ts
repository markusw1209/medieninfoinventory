import { environment } from './../../../environments/environment';
import { Item } from './../../core/interfaces/IItem';
import { InventoryService } from './../../core/services/inventory.service';
import { EventEmitterService } from '../main-page-services/event-emitter.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
    selector: 'app-inventory-overview',
    templateUrl: './inventory-overview.component.html',
    styleUrls: ['./inventory-overview.component.css']
})
export class InventoryOverviewComponent implements OnInit, OnDestroy {
    private isAlive: boolean;
    public inventory: MatTableDataSource<Item> = new MatTableDataSource<Item>();
    public displayedColumns: string[] = [
        'id',
        'tag',
        'name',
        'numberExisting',
        'description',
        'rent',
        'blocked',
        'addDate',
        'removeDate',
        'rentHistory',
        'editItem',
        'returnItem',
        'deleteItem'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<InventoryOverviewComponent>,
        private eventEmitterService: EventEmitterService,
        private inventoryService: InventoryService,
        public matDialog: MatDialog,
    ) {
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadItems();
        this.setAutoRefresh();
    }

    ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadItems(): void {
        this.inventoryService
            .getInventory()
            .pipe(
                map((items: Item[]) => {
                    this.inventory.data = items;
                    this.inventory.sort = this.sort;
                    this.inventory.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.inventory.filter = filterValue.trim().toLowerCase();

        if (this.inventory.paginator) {
            this.inventory.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
                this.loadItems();
            });
    }

    public openEditItemDialog(item: Item): void {
        this.eventEmitterService.onEditItemClick(item);
    }

    public openDeleteItemDialog(item: Item): void {
        this.eventEmitterService.onDeleteItemClick(item);
    }

    public openRentHistoryOverview(item: Item): void {
        this.eventEmitterService.onOpenRentHistoryClick(item);
    }

    public openReturnItemDialog(itemId: number, itemName: string): void {
        this.eventEmitterService.onReturnItemClick(itemId, itemName);
    }
}
