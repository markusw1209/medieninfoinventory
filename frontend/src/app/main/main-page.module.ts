import { NgModule } from '@angular/core';
import { MainPageRoutingModule } from './main-page-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { SharedModule } from '../shared/shared.module';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';
import { UsersOverviewComponent } from './users-overview/users-overview.component';
import { RentsOverviewComponent } from './rents-overview/rents-overview.component';
import { InventoryOverviewComponent } from './inventory-overview/inventory-overview.component';
import { EditItemDialogComponent } from './edit-item-dialog/edit-item-dialog.component';
import { RentHistoryOverviewComponent } from './rent-history-overview/rent-history-overview.component';
import { DevicesOverviewComponent } from './devices-overview/devices-overview.component';
import { NewDeviceDialogComponent } from './new-device-dialog/new-device-dialog.component';
import { OkCancelDialogComponent } from './ok-cancel-dialog/ok-cancel-dialog.component';
import { UserHistoryOverviewComponent } from './user-history-overview/user-history-overview.component';
import { RentDetailsComponent } from './rent-details/rent-details.component';

@NgModule({
  declarations: [
    MainPageComponent,
    EditUserDialogComponent,
    UsersOverviewComponent,
    RentsOverviewComponent,
    InventoryOverviewComponent,
    EditItemDialogComponent,
    RentHistoryOverviewComponent,
    DevicesOverviewComponent,
    NewDeviceDialogComponent,
    OkCancelDialogComponent,
    UserHistoryOverviewComponent,
    RentDetailsComponent,
  ],
  imports: [MainPageRoutingModule, SharedModule]
})
export class MainPageModule {}
