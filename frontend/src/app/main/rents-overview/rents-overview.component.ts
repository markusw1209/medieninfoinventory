import { environment } from './../../../environments/environment';
import { EventEmitterService } from './../main-page-services/event-emitter.service';
import { Rent } from './../../core/interfaces/IRent';
import { RentsService } from './../../core/services/rents.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
    selector: 'app-rents-overview',
    templateUrl: './rents-overview.component.html',
    styleUrls: ['./rents-overview.component.css']
})
export class RentsOverviewComponent implements OnInit, OnDestroy {
    private isAlive: boolean;
    public rents: MatTableDataSource<Rent> = new MatTableDataSource<Rent>();
    public displayedColumns: string[] = [
        'id',
        'item',
        'user',
        'rentDate',
        'returnDate',
        'extensions',
        'rentDelayed',
        'returnItem',
        'extendRent'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<RentsOverviewComponent>,
        private eventEmitterService: EventEmitterService,
        public matDialog: MatDialog,
        private rentsService: RentsService
    ) {
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadRents();
        this.setAutoRefresh();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadRents(): void {
        this.rentsService
            .getActiveRents()
            .pipe(
                map((rents: Rent[]) => {
                    this.rents.data = rents;
                    this.rents.sort = this.sort;
                    this.rents.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.rents.filter = filterValue.trim().toLowerCase();

        if (this.rents.paginator) {
            this.rents.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
            this.loadRents();
            });
    }

    public openExtendRentDialog(itemId: number, rentId: number, itemName: string): void {
        this.eventEmitterService.onExtendRentClick(itemId, rentId, itemName);
    }

    public openReturnItemDialog(itemId: number, itemName: string): void {
        this.eventEmitterService.onReturnItemClick(itemId, itemName);
    }
}
