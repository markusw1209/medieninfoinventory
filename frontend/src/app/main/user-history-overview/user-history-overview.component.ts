import { environment } from './../../../environments/environment';
import { User } from './../../core/interfaces/IUser';
import { UserService } from './../../core/services/user.service';
import { Rent } from './../../core/interfaces/IRent';
import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { map, takeWhile } from 'rxjs/operators';
import { interval } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
moment.locale('de');

@Component({
  selector: 'app-user-history-overview',
  templateUrl: './user-history-overview.component.html',
  styleUrls: ['./user-history-overview.component.css']
})
export class UserHistoryOverviewComponent implements OnInit, OnDestroy {

    private isAlive: boolean;
    public user: User;
    public rents: MatTableDataSource<Rent> = new MatTableDataSource<Rent>();
    public displayedColumns: string[] = [
        'id',
        'item',
        'rentDate',
        'returnDate',
        'extensions'
    ];
    @ViewChild(MatSort, { static: true }) public sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;

    constructor(
        public dialogRef: MatDialogRef<UserHistoryOverviewComponent>,
        public matDialog: MatDialog,
        private userService: UserService,
        @Inject(MAT_DIALOG_DATA) public data: User,
    ) {
        this.user = data;
        this.isAlive = true;
    }

    public ngOnInit(): void {
        this.loadRents();
        this.setAutoRefresh();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private loadRents(): void {
        this.userService
            .getUserHistory(this.user)
            .pipe(
                map((rents: Rent[]) => {
                    this.rents.data = rents;
                    this.rents.sort = this.sort;
                    this.rents.paginator = this.paginator;
                })
            ).subscribe();
    }

    public applyFilter(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.rents.filter = filterValue.trim().toLowerCase();

        if (this.rents.paginator) {
            this.rents.paginator.firstPage();
        }
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
                this.loadRents();
            });
    }
}