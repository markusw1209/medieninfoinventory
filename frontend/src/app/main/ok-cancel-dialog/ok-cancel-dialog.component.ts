import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ok-cancel-dialog',
  templateUrl: './ok-cancel-dialog.component.html',
  styleUrls: ['./ok-cancel-dialog.component.css']
})
export class OkCancelDialogComponent implements OnInit {

    public headline: string;
    public text: string;
    public cancelButtonText: string;
    public okButtonText: string;

    constructor(
        public dialogRef: MatDialogRef<OkCancelDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data
    ) {
        this.headline = data.headline;
        this.text = data.text;
        this.cancelButtonText = data.cancelButtonText
        this.okButtonText = data.okButtonText;
    }

    public ngOnInit(): void {
    }

}
