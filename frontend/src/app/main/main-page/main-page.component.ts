import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { RentDetailsComponent } from './../rent-details/rent-details.component';
import { UserHistoryOverviewComponent } from './../user-history-overview/user-history-overview.component';
import { OkCancelDialogComponent } from './../ok-cancel-dialog/ok-cancel-dialog.component';
import { NewDeviceDialogComponent } from './../new-device-dialog/new-device-dialog.component';
import { DevicesOverviewComponent } from './../devices-overview/devices-overview.component';
import { Device } from './../../core/interfaces/IDevice';
import { DevicesService } from '../../core/services/devices.service';
import { RentHistoryOverviewComponent } from './../rent-history-overview/rent-history-overview.component';
import { Item } from './../../core/interfaces/IItem';
import { User } from './../../core/interfaces/IUser';
import { Rent } from './../../core/interfaces/IRent';
import { EditItemDialogComponent } from './../edit-item-dialog/edit-item-dialog.component';
import { RentsOverviewComponent } from './../rents-overview/rents-overview.component';
import { InventoryOverviewComponent } from './../inventory-overview/inventory-overview.component';
import { EventEmitterService } from '../main-page-services/event-emitter.service';
import { EditUserDialogComponent } from './../edit-user-dialog/edit-user-dialog.component';
import { InventoryService } from '../../core/services/inventory.service';
import { UserService } from '../../core/services/user.service';
import { RentsService } from '../../core/services/rents.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { map, takeWhile, single } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { interval } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as moment from 'moment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersOverviewComponent } from '../users-overview/users-overview.component';
moment.locale('de');
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit, OnDestroy {

    public rents: MatTableDataSource<Rent> = new MatTableDataSource<Rent>();
    public columnsRents: string[] = [
        'id',
        'item',
        'user',
        'returnDate',
        'extensions',
        'rentDelayed',
        'returnItem',
        'extendRent'
    ];

    public users: MatTableDataSource<User> = new MatTableDataSource<User>();
    public columnsUsers: string[] = [
        'id',
        'firstName',
        'lastName',
        'role',
        'admin',
        'approved',
        'mail',
        'lastSeen',
        'userHistory',
        'editUser',
        'deleteUser'
    ];

    public inventory: MatTableDataSource<Item> = new MatTableDataSource<Item>();
    public columnsInventory: string[] = [
        'id',
        'tag',
        'multiple',
        'name',
        'rent',
        'blocked',
        'rentHistory',
        'editItem',
        'returnItem',
        'deleteItem'
    ];

    public devices: MatTableDataSource<Device> = new MatTableDataSource<Device>();
    public columnsDevices: string[] = [
        'id',
        'deviceName',
        'mac',
        'deleteDevice'
    ];

    public tagHistorySearchId: string;
    private isAlive = true;
    public newUsers: User[];
    public newItems: Item[];
    @ViewChild('paginatorDevices') public paginatorDevices: MatPaginator;
    @ViewChild('paginatorInventory') public paginatorInventory: MatPaginator;
    @ViewChild('paginatorRents') public paginatorRents: MatPaginator;
    @ViewChild('paginatorUser') public paginatorUsers: MatPaginator;
    @ViewChild('sortDevices') public sortDevices: MatSort;
    @ViewChild('sortInventory') public sortInventory: MatSort;
    @ViewChild('sortRents') public sortRents: MatSort;
    @ViewChild('sortUsers') public sortUsers: MatSort;

    constructor(
        private devicesService: DevicesService,
        private eventEmitterService: EventEmitterService,
        private inventoryService: InventoryService,
        public matDialog: MatDialog,
        private rentsService: RentsService,
        private router: Router,
        private snackBar: MatSnackBar,
        private userService: UserService
    ) {}

    public ngOnInit(): void {
        this.initializeNewStuff();
        this.checkSession();
        this.loadRents();
        this.loadAllUsers();
        this.loadInventory();
        this.loadDevices();
        this.setAutoRefresh();
        this.checkEventEmitter();
    }

    public ngOnDestroy(): void {
        this.isAlive = false;
    }

    private setAutoRefresh(): void {
        interval(environment.refreshInterval)
            .pipe(takeWhile(() => this.isAlive))
            .subscribe(() => {
                this.checkSession();
                this.loadDevices();
                this.loadRents();
                this.loadAllUsers();
                this.loadInventory();
        });
    }

    private checkSession(): void {
        const expireTime: number = parseInt(sessionStorage.getItem('expires_at'));
        if (expireTime - Date.now() <= 0) {
            sessionStorage.clear();
            this.router.navigateByUrl('/authentication/login');
        }
    }

    private checkEventEmitter():void {
        if (this.eventEmitterService.subsVar === undefined) {
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeDeleteDeviceFunction.subscribe((device: Device) => {
                this.openDeleteDeviceDialog(device);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeDeleteItemFunction.subscribe((item: Item) => {
                this.openDeleteItemDialog(item);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeDeleteUserFunction.subscribe((user: User) => {
                this.openDeleteUserDialog(user);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeEditItemFunction.subscribe((item: Item) => {
                this.openEditItemDialog(item);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeEditUserFunction.subscribe((user: User) => {
                this.openEditUserDialog(user);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeExtendRentFunction.subscribe((data) => {
                this.openExtendRentDialog(data.itemId, data.rentId, data.ItemName);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeNewDeviceFunction.subscribe(() => {
                this.openNewDeviceDialog();
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeNewItemsFunction.subscribe((items: Item[]) => {
                this.checkNewItems(items);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeNewUsersFunction.subscribe((users: User[]) => {
                this.checkNewUsers(users);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenUserOverview.subscribe(() => {
                this.openUsersOverview();
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenInventoryOverview.subscribe(() => {
                this.openInventoryOverview();
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenRentsOverview.subscribe(() => {
                this.openRentsOverview();
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenRentHistory.subscribe((item: Item) => {
                this.openRentHistoryOverview(item);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenUserHistory.subscribe((user: User) => {
                this.openUserHistoryOverview(user);
            });
            this.eventEmitterService.subsVar = this.eventEmitterService.
            invokeOpenRenturnItemDialog.subscribe((data) => {
                this.openReturnItemDialog(data.id, data.name);
            });
        }
    }

    public applyFilterDevices(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.devices.filter = filterValue.trim().toLowerCase();
        if (this.devices.paginator) {
            this.devices.paginator.firstPage();
        }
    }

    public applyFilterInventory(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.inventory.filter = filterValue.trim().toLowerCase();
        if (this.inventory.paginator) {
            this.inventory.paginator.firstPage();
        }
    }

    public applyFilterRents(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.rents.filter = filterValue.trim().toLowerCase();
        if (this.rents.paginator) {
            this.rents.paginator.firstPage();
        }
    }

    public applyFilterUsers(event: Event): void {
        const filterValue = (event.target as HTMLInputElement).value;
        this.users.filter = filterValue.trim().toLowerCase();
        if (this.users.paginator) {
            this.users.paginator.firstPage();
        }
    }

    private loadAllUsers(): void {
        this.userService
            .getAllUsers()
            .pipe(
                map((users: User[]) => {
                    this.users.data = users;
                    this.users.sort = this.sortUsers;
                    this.users.paginator = this.paginatorUsers;
                })
            ).subscribe();
    }

    private loadDevices(): void {
        this.devicesService
            .getDevices()
            .pipe(
                map((devices: Device[]) => {
                    this.devices.data = devices;
                    this.devices.sort = this.sortDevices;
                    this.devices.paginator = this.paginatorDevices;
                })
            ).subscribe();
    }

    private loadInventory(): void {
        this.inventoryService
            .getInventory()
            .pipe(
                map((inventory: Item[]) => {
                    this.inventory.data = inventory;
                    this.inventory.sort = this.sortInventory;
                    this.inventory.paginator = this.paginatorInventory;
                })
            ).subscribe();
    }

    private loadRents(): void {
        this.rentsService
            .getActiveRents()
            .pipe(
                map((rents: Rent[]) => {
                    this.rents.data = rents;
                    this.rents.sort = this.sortRents;
                    this.rents.paginator = this.paginatorRents;
                })
            ).subscribe();
    }

    private checkNewItems(newItems: Item[]): void {
        for (const i of newItems) {
            if (this.newItems.filter(item => item.tagId === i.tagId).length === 0) {
                this.openNewItemDialog(i);
                this.newItems.push(i);
                this.saveNewStuff();
            }
        }
    }

    private checkNewUsers(newUsers): void {
        for (const u of newUsers) {
            if (this.newUsers.filter(user => user.cardId === u.cardId).length === 0) {
                this.openNewUserDialog(u);
                this.newUsers.push(u);
                this.saveNewStuff();
            }
        }
    }

    private initializeNewStuff() {
        if (!sessionStorage.getItem('knownNewItems')) this.newItems = [];
        else this.newItems = JSON.parse(sessionStorage.getItem('knownNewItems'));
        if (!sessionStorage.getItem('knownNewUsers')) this.newUsers = [];
        else this.newUsers = JSON.parse(sessionStorage.getItem('knownNewUsers'));
    }

    private saveNewStuff() {
        sessionStorage.setItem('knownNewItems', JSON.stringify(this.newItems));
        sessionStorage.setItem('knownNewUsers', JSON.stringify(this.newUsers));
    }

    public openDevicesOverview(): void {
        const matDialogConfig: MatDialogConfig = {
            width: '50%',
            disableClose: true
        };
        this.matDialog.open(DevicesOverviewComponent, matDialogConfig);
    }

    public openDeleteDeviceDialog(device: Device): void {
        const headline = 'Gerät löschen';
        const text = 'Wollen Sie das Gerät ID ' + device.id + ' ' + device.mac + ' wirklich löschen?';
        const okButtonText = 'Löschen';
        const cancelButtonText = 'Abbrechen';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.devicesService.deleteDevice(device).subscribe((res) => {
                    this.snackBar.open('Gerät mit ID ' + res.id + ' erfolgreich gelöscht', null, { duration: 3000 });
                    this.loadDevices();
                });
            }
        });
    }

    public openDeleteItemDialog(item: Item): void {
        const headline = 'Artikel löschen';
        const text = 'Wollen Sie den Artikel ID ' + item.id + ' ' + item.name + ' wirklich löschen?';
        const okButtonText = 'Löschen';
        const cancelButtonText = 'Abbrechen';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.inventoryService.deleteItem(item).subscribe((res) => {
                    this.snackBar.open('Artikel mit ID ' + res.itemId + ' erfolgreich gelöscht', null, { duration: 3000 });
                    this.loadInventory();
                });
            }
        });
    }

    public openDeleteUserDialog(user: User): void {
        const headline = 'Nutzer löschen';
        const text = 'Wollen Sie Nutzer ID ' + user.id + ' ' + user.firstName + ' ' + user.lastName + ' wirklich löschen?';
        const okButtonText = 'Löschen';
        const cancelButtonText = 'Abbrechen';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.userService.deleteUser(user).subscribe((res) => {
                    this.snackBar.open('User mit ID ' + res.userId + ' erfolgreich gelöscht', null, { duration: 3000 });
                    this.loadAllUsers();
                });
            }
        });
    }

    public openEditItemDialog(item: Item): void {
        const matDialogConfig: MatDialogConfig = {
            data: item,
            width: '50%',
            disableClose: true
        };
        const dialogRef = this.matDialog.open(EditItemDialogComponent, matDialogConfig);
        dialogRef.afterClosed().subscribe((res) => {
            console.log(res);
            if (res.id !== null && res.id !== undefined) {
                this.loadInventory();
                this.snackBar.open('Artikeldaten erfolgreich gespeichert', null, { duration: 3000 });
            }
        });
    }

    public openEditUserDialog(user: User): void {
        const matDialogConfig: MatDialogConfig = {
            data: user,
            width: '50%',
            disableClose: true
        };
        const dialogRef = this.matDialog.open(EditUserDialogComponent, matDialogConfig);
        dialogRef.afterClosed().subscribe((res) => {
            console.log(res);
            if (res.id !== null && res.id !== undefined) {
                this.loadAllUsers();
                this.snackBar.open('Nutzerdaten erfolgreich gespeichert', null, { duration: 3000 });
            }
        });
    }

    public openExtendRentDialog(itemId, rentId, name): void {
        const headline = 'Ausleihe verlängern';
        const text = 'Wollen Sie die Ausleihe von Artikel ID ' + itemId + ' ' + name + ' wirklich verlängern?';
        const okButtonText = 'Verlängern';
        const cancelButtonText = 'Abbrechen';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.rentsService.extendRent(rentId).subscribe((res) => {
                    this.snackBar.open('Ausleihe ' + res.id + ' erfolgreich verlängert!', null, { duration: 3000 });
                    this.loadRents();
                }, (error) => {
                    console.log(error.error);
                    if (error.error === 'MaxCountExt') {
                        this.snackBar.open('Verlängerung nicht möglich!\nMaximale Zahl Verlängerungen erreicht!', null, { duration: 3000 });
                    }
                });
            }
        });
    }

    public openInventoryOverview(): void {
        const matDialogConfig: MatDialogConfig = {
            width: '80%',
            disableClose: true
        };
        this.matDialog.open(InventoryOverviewComponent, matDialogConfig);
    }

    public openNewDeviceDialog(): void {
        let device: Device;
        const matDialogConfig: MatDialogConfig = {
            data: device,
            disableClose: true
        };
        const dialogRef = this.matDialog.open(NewDeviceDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res && res.id !== null && res.id !== undefined) {
                this.loadDevices();
                this.snackBar.open('Device mit ID ' + res.id + ' erfolgreich hinzugefügt', null, { duration: 3000 });
            }
        });
    }

    public openNewItemDialog(item: Item): void {
        const headline = 'Neuer Artikel';
        const text = 'Es wurde ein neuer Artikel mit Tag-ID ' + item.tagId + " hinzugefügt.";
        const okButtonText = 'Jetzt registrieren';
        const cancelButtonText = 'Später';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.openEditItemDialog(item);
            }
        });
    }

    public openNewUserDialog(user: User): void {
        const headline = 'Neuer Nutzer';
        const text = 'Es wurde ein neuer Nutzer mit Card-ID ' + user.cardId + " hinzugefügt.";
        const okButtonText = 'Jetzt registrieren';
        const cancelButtonText = 'Später';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.openEditUserDialog(user);
            }
        });
    }

    public openRentDetails(rent: Rent): void {
        const matDialogConfig: MatDialogConfig = {
            data: rent,
            disableClose: true
        };
        this.matDialog.open(RentDetailsComponent, matDialogConfig);
    }

    public openRentHistoryOverview(item: Item): void {
        const matDialogConfig: MatDialogConfig = {
            data: item,
            width: '50%',
            disableClose: true
        };
        this.matDialog.open(RentHistoryOverviewComponent, matDialogConfig);
    }

    public openRentsOverview(): void {
        const matDialogConfig: MatDialogConfig = {
            width: '60%',
            disableClose: true
        };
        this.matDialog.open(RentsOverviewComponent, matDialogConfig);
    }

    public openReturnItemDialog(id, name): void {
        const headline = 'Artikel zurückgeben';
        const text = 'Wollen Sie den Artikel ID ' + id + ' ' + name + ' wirklich zurückgeben?';
        const okButtonText = 'Zurückgeben';
        const cancelButtonText = 'Abbrechen';
        const matDialogConfig: MatDialogConfig = {
            data: { headline, text, okButtonText, cancelButtonText },
            disableClose: true
        };
        const dialogRef = this.matDialog.open(OkCancelDialogComponent, matDialogConfig);

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.inventoryService.returnItem(id).subscribe((res) => {
                    this.snackBar.open('Item mit ID ' + res.id + ' erfolgreich zurückgegeben', null, { duration: 3000 });
                    this.loadRents();
                    this.loadInventory();
                });
            }
        });
    }

    public openUsersOverview(): void {
        const matDialogConfig: MatDialogConfig = {
            width: '90%',
            disableClose: true
        };
        this.matDialog.open(UsersOverviewComponent, matDialogConfig);
    }

    public openUserHistoryOverview(user: User): void {
        const matDialogConfig: MatDialogConfig = {
            data: user,
            width: '50%',
            disableClose: true
        };
        this.matDialog.open(UserHistoryOverviewComponent, matDialogConfig);
    }
}
