(function(window) {
    window["env"] = window["env"] || {};
  
    // Environment variables
    window["env"]["apiUrl"] = "http://localhost";
    window["env"]["apiPort"] = 3000;
    window["env"]["refreshInterval"] = 5000;
})(this);