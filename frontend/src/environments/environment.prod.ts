export const environment = {
  production: true,
  apiUrl: window['env']['apiUrl'] || 'localhost',
  apiPort: window['env']['apiPort'] || 3000,
  refreshInterval: window['env']['refreshInterval'] || 5000
};
