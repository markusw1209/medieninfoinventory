'use strict'

class Rent {

    constructor(id, user, userId, item, itemId, rentDate, returnDate, extensions) {
        this.id = id;
        this.user = user;
        this.userId = userId;
        this.item = item;
        this.itemId = itemId;
        this.rentDate = rentDate;
        this.returnDate = returnDate;
        this.extensions = extensions;
    }
}

module.exports = { Rent };