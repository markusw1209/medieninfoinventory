'use strict'

class Device {

    constructor(id, name, mac, key, lastSeen) {
        this.id = id;
        this.name = name;
        this.mac = mac;
        this.key = key;
        this.lastSeen = lastSeen;
    }
}

module.exports = { Device };