'use strict'

class User {

    constructor(id, cardId, firstName, lastName, role, matrikel, mail, admin, approved, active, lastSeen, addDate, removeDate) {
        this.id = id;
        this.cardId = cardId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.matrikel = matrikel;
        this.mail = mail;
        this.admin = admin;
        this.approved = approved;
        this.active = active;
        this.lastSeen = lastSeen;
        this.addDate = addDate;
        this.removeDate = removeDate;
    }
}

module.exports = { User };