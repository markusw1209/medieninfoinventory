'use strict'

class Item {

    constructor(id, tagId, name, numberExisting, numberRent, description, rent, active, blocked, addDate, removeDate) {
        this.id = id;
        this.tagId = tagId;
        this.name = name;
        this.numberExisting = numberExisting;
        this.numberRent = numberRent;
        this.description = description;
        this.rent = rent;
        this.active = active;
        this.blocked = blocked;
        this.addDate = addDate;
        this.removeDate = removeDate;
    }
}

module.exports = { Item };