"use strict"

let DEBUG = false;

const express = require('express');
const BackendGUI = require('./APIs/backendGUI.js');
const BackendIoT = require('./APIs/backendIoT.js');
const server = express();
const cors = require('cors');

server.use(express.json());

server.use(cors());

//Port des Servers
const port = 3000;

server.listen(port, () => {
    console.log('Server is running and listening on Port ' + port + '.');
});

const backendIoT = new BackendIoT.BackendIoT(server);
const backendGUI = new BackendGUI.BackendGUI(server);
