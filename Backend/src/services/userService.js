'use strict'

const DBConnector  = require('./dbConnector');
const bcrypt = require('bcryptjs');

class UserService {

    constructor() {
        this.dbConnector = new DBConnector.DBConnector();
    }

    deleteUser = async (userId) => {
        if (!userId || userId === '') return { error: 'no userId' };
        try {
            const res = await this.dbConnector.deleteUser(userId);
            if (!res) return { error: 'user not found' };
            return res;
        } catch (err) {
            throw err;
        }
    }

    getActiveUsers = async () => {
        try {
            const res = await this.dbConnector.getUsers();
            const admins = await this.dbConnector.getAdmins();
            const users = [];
            if (!res) return { error: 'Could not get users'};
            for (let r of res) {
                if (r.active) {
                    if (r.admin) {
                        const admin = admins.filter(admin => admin.userId === r.id);
                        if (admin[0]) r.username = admin[0].username;
                    }
                    users.push(r);
                }
            }
            return users;
        } catch (err) {
            throw err;
        }
    }

    getNewUsers = async () => {
        try {
            const res = await this.dbConnector.getUsers();
            const users = [];
            if (!res) return { error: 'Could not get users'};
            for (let r of res) {
                if (!r.active && r.removeDate === null) {
                    users.push(r);
                }
            }
            return users;
        } catch (err) {
            throw err;
        }
    }

    getUser = async (userId) => {
        if (!userId || userId === '') return { error: 'no userId'}
        try {
            const user = await this.dbConnector.getUser(userId);
            if (!user) return { error: 'Could not get users'};
            return user;
        } catch (err) {
            throw err;
        }
    }

    getUserHistory = async (userId) => {
        if (!userId || userId === '') return;
        try {
            const res = await this.dbConnector.getUserHistory(userId);
            if (!res) return { error: 'Could not get user-history'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    isUser = async (cardId) => {
        if (!cardId || cardId === '') return { error: 'no cardId'}
        try {
            const user = await this.dbConnector.getUserData(cardId);
            console.log(user);
            if (!user) return false;
            return true;
        } catch (err) {
            throw err;
        }
    }

    registerHeartbeat = (user, device) => {
        try {
            if (user) this.dbConnector.setHeartbeat(user.id, device);
            else this.dbConnector.setHeartbeat(null, device);
        } catch (err) {
            throw err;
        }
    }

    removeUser = async (userId) => {
        if (!userId || userId === '') return { error: 'no userId' };
        try {
            const res = await this.dbConnector.removeUser(userId);
            if (!res) return { error: 'user not found' };
            return res;
        } catch (err) {
            throw err;
        }
    }
 
    setUserData = async (user) => {
        if (!user) return;
        try {
            const res = await this.dbConnector.setUserData(user);
            if (!res) return { error: 'Could not set user data'};
            let admin;
            if (user.admin) {
                const adminData = await this.dbConnector.getAdminByUserId(user.id);
                if (!adminData) {
                    if (!user.password || user.password === '') return { error: 'password needed!'};
                    if (!user.username || user.username === '') return { error: 'username needed!' };
                } else if (!user.username) user.username = adminDate.username;

                if (user.password) {
                    const password = await hashPassword(user.password)
                    admin = await this.dbConnector.setAdminData(user.id, user.username, password);
                } else {
                    admin = await this.dbConnector.setAdminData(user.id, user.username, null);
                }
    
                if (!admin) return { error: 'Could not set admin data'};
            } else {
                this.dbConnector.deleteAdmin(user.id);
            }
            return res;
        } catch (err) {
            throw err;
        }
    }

}

async function hashPassword (password) {
    const salt = await bcrypt.genSalt();
    const hashedKey = await bcrypt.hash(password, salt);
    return hashedKey;
}

module.exports = { UserService };