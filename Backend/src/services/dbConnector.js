'use strict'

const mariadb = require('mariadb');
const dotenv = require('dotenv');
const { Device } = require('../classes/Device.js');
const { Item } = require('../classes/Item.js');
const { Rent } = require('../classes/Rent.js');
const { User } = require('../classes/User.js');

dotenv.config({ path: '../../deployment/.env' });

// Zugangsdaten für die Datenbank:
const pool = mariadb.createPool({
    host: 'db', //'localhost',  
    user: process.env.DB_USERNAME,
    port: process.env.DB_PORT,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    connectionLimit: 5,
    timezone: 'utc'
});

// Alle SQL-Queries zusammengefasst:
const Queries = {
    DELETE_ADMIN: 'DELETE FROM admins WHERE user = ?;',
    DELETE_DEVICE: 'DELETE FROM devices WHERE id = ?;',
    DELETE_ITEM: 'DELETE FROM inventory WHERE id = ?;',
    DELETE_RENT: 'DELETE FROM rents_active WHERE id = ?',
    DELETE_USER: 'DELETE FROM users WHERE id = ?;',
    GET_ADMIN_BY_USER_ID: 'SELECT id, user as userId, username, password FROM admins WHERE user = ?;',
    GET_ALL_ADMINS: 'SELECT id, user as userId, username, password FROM admins;',
    GET_ALL_DEVICES: 'SELECT id, name, mac, access_key as accessKey, last_seen as lastSeen FROM devices;',
    GET_ALL_USERS: 'SELECT id, first_name as firstName, last_name as lastName, card_id as cardId, '
        + 'role, matrikel, mail, admin, approved, active, last_seen as lastSeen, add_date as addDate, '
        + 'remove_date as removeDate FROM users;',
    GET_DEVICE_DATA: 'SELECT id, access_key as accessKey FROM devices WHERE mac = ?;',
    GET_INVENTORY: 'SELECT id, tag_id as tagId, name, number_existing as numberExisting, number_rent as numberRent, description, '
        + 'active, blocked, add_date as addDate, remove_date as removeDate FROM inventory;',
    GET_ITEM_BY_ID: 'SELECT id, tag_id as tagId, name, number_existing as numberExisting, number_rent as numberRent, description, '
        + 'active, blocked, add_date as addDate, remove_date as removeDate FROM inventory WHERE id = ?;',
    GET_ITEM_BY_TAG: 'SELECT id, tag_id as tagId, name, number_existing as numberExisting, number_rent as numberRent, description, '
        + 'active, blocked, add_date as addDate, remove_date as removeDate FROM inventory WHERE tag_id = ?;',
    GET_RENT_HISTORY_OF_ITEM: 'SELECT rents_history.id as id, users.first_name as firstName, users.last_name as lastName, '
        + 'rents_history.item as itemId, rents_history.rent_date as rentDate, rents_history.return_date as returnDate, '
        + 'rents_history.extensions as extensions, rents_history.user as userId FROM rents_history INNER JOIN users '
        + ' ON (rents_history.user = users.id) WHERE item = ? ORDER BY rents_history.rent_date DESC;',
    GET_RENT_BY_ID: 'SELECT id, item, user, rent_date as rentDate, return_date as returnDate, extensions FROM rents_active WHERE id = ?;',
    GET_RENTS_BY_ITEM_ID: 'SELECT rents_active.id as rentId, users.first_name as firstName, users.last_name as lastName, '
        + 'inventory.name as itemName, rents_active.item as itemId, rents_active.rent_date as rentDate, rents_active.return_date '
        + 'as returnDate, rents_active.user as userId, rents_active.extensions as extensions FROM rents_active INNER JOIN users ON '
        + '(rents_active.user = users.id) INNER JOIN inventory ON (rents_active.item = inventory.id) WHERE rents_active.item = ?;',
    GET_RENTS_BY_TAG: 'SELECT rents_active.* FROM inventory JOIN rents_active ON (inventory.id = rents_active.item) '
        + 'WHERE inventory.tag_id = ?;',
    GET_RENTS_BY_TAG_ORDER_BY_RETURN: 'SELECT rents_active.* FROM inventory JOIN rents_active ON (inventory.id = rents_active.item) '
        + 'WHERE inventory.tag_id = ? ORDER BY rents_active.return_date ASC;',
    GET_RENTS_BY_USER_ID: 'SELECT rents_active.id as rentId, users.first_name as firstName, users.last_name as lastName, '
        + 'inventory.name as itemName, rents_active.item as itemId, rents_active.rent_date as rentDate, rents_active.return_date '
        + 'as returnDate, rents_active.extensions as extensions FROM rents_active INNER JOIN users ON '
        + '(rents_active.user = users.id) INNER JOIN inventory ON (rents_active.item = inventory.id) WHERE users.id = ?;',
    GET_RENTS_ALL: 'SELECT rents_active.id as id, users.first_name as firstName, users.last_name as lastName, '
        + 'rents_active.user as userId, inventory.name as item, rents_active.item as itemId, rents_active.rent_date as rentDate, '
        + 'rents_active.return_date as returnDate, rents_active.extensions as extensions FROM rents_active INNER JOIN users ON '
        + '(rents_active.user = users.id) INNER JOIN inventory ON (rents_active.item = inventory.id);',
    GET_RENTS_DELAYED: 'SELECT rents_active.id as id, users.first_name as firstName, users.last_name as lastName, '
        + 'inventory.name as item, rents_active.item as itemId, rents_active.rent_date as rentDate, rents_active.return_date '
        + 'as returnDate, rents_active.extensions as extensions FROM rents_active INNER JOIN users ON '
        + '(rents_active.user = users.id) INNER JOIN inventory ON (rents_active.item = inventory.id) WHERE return_date < ?;',
    GET_USER: 'SELECT id, first_name as firstName, last_name as lastName, card_id as cardId, '
        + 'role, matrikel, mail, admin, approved, active, last_seen as lastSeen, add_date as addDate, remove_date as removeDate '
        + 'FROM users WHERE id = ?;',
    GET_USER_DATA: 'SELECT id, first_name as firstName, last_name as lastName, role, matrikel, mail, admin, approved, active, '
        + 'last_seen as lastSeen, add_date as addDate, remove_date as removeDate FROM users WHERE card_id = ?;',
    GET_USER_HISTORY: 'SELECT rents_history.id as rentId, rents_history.item as ItemId, rents_history.rent_date as rentDate, ' 
        + 'rents_history.return_date as returnDate, rents_history.extensions as extensions, inventory.name as itemName, ' 
        + 'users.first_name as firstName, users.last_name as lastName FROM rents_history INNER JOIN inventory ON ' 
        + '(rents_history.item = inventory.id) INNER JOIN users ON (rents_history.user = users.id) WHERE users.id = ?;',
    GET_WEB_USER_DATA: 'SELECT users.id, users.first_name as firstName, users.last_name as lastName, users.card_id as cardId, '
        + 'users.role as role, users.matrikel as matrikel, users.mail as mail, users.admin as admin, users.approved as approved, '
        + 'users.last_seen as lastSeen, admins.username as username, admins.password as password, users.active as active, '
        + 'users.add_date as addDate, users.remove_date as removeDate ' 
        + 'FROM users JOIN admins ON (users.id = admins.user) WHERE admins.username = ?;',
    INSERT_NEW_DEVICE: 'INSERT INTO devices VALUES (null, ?, ?, ?, null);',
    INSERT_NEW_ITEM: 'INSERT INTO inventory VALUES (null, ?, null, 0, 0, null, false, false, ?, null)',
    INSERT_NEW_RENT: 'INSERT INTO rents_active VALUES (null, ?, ?, ?, ?, 0);',
    INSERT_NEW_RENTHISTORY: 'INSERT INTO rents_history VALUES (null, ?, ?, ?, ?, ?)',
    INSERT_NEW_USER: 'INSERT INTO users VALUES (null, ?, null, null, null, null, null, false, false, false, null, ?, null);',
    NUMBER_RENT_DECR: 'UPDATE inventory SET number_rent = number_rent - 1 WHERE id = ? AND number_rent > 0;',
    NUMBER_RENT_INCR: 'UPDATE inventory SET number_rent = number_rent + 1 WHERE id = ? AND number_rent < number_existing;',
    REMOVE_ITEM: 'UPDATE inventory SET active = false, tag_id = ?, remove_date = ? WHERE id = ?',
    REMOVE_USER: 'UPDATE users SET active = false, card_id = ?, remove_date = ? WHERE id = ?;',
    SET_ADMIN_DATA: 'UPDATE admins SET username = ? WHERE user = ?;',
    SET_ADMIN_DATA_OR_CREATE: 'INSERT INTO admins VALUES (null, ?, ?, ?) ON DUPLICATE KEY UPDATE username = ?, password = ?;',
    SET_DEVICE_DATA: 'UPDATE devices SET name = ?, mac = ?, access_key = ? WHERE id = ?;',
    SET_DEVICE_DATA_WITHOUT_KEY: 'UPDATE devices SET name = ?, mac = ? WHERE id = ?;',
    UPDATE_ITEM_ACTIVE: 'UPDATE inventory SET active = true WHERE id = ?;',
    UPDATE_ITEM_BLOCK: 'UPDATE inventory SET blocked = ? WHERE id = ?;',
    UPDATE_ITEM_DATA: 'UPDATE inventory SET name = ?, number_existing = ?, description = ?, active = ?, blocked = ? '
         + 'WHERE id = ?;',
    UPDATE_LAST_SEEN_DEVICE: 'UPDATE devices SET last_seen = ? WHERE mac = ?;',
    UPDATE_LAST_SEEN_USER: 'UPDATE users SET last_seen = ? WHERE id = ?;',
    UPDATE_RENT_RETURNDATE: 'UPDATE rents_active SET return_date = ?, extensions = (extensions + 1) WHERE id = ?',
    UPDATE_USER_DATA: 'UPDATE users SET first_name = ?, last_name = ?, role = ?, matrikel = ?, mail = ?, '
        + 'admin = ?, approved = ?, active = true WHERE id = ?;'
}

//Ermöglicht den Zugriff auf die Datenbank und "übersetzt" die Anfragen in SQL-Statements.
class DBConnector {

    constructor() {};

    activateItem = async (itemId) => {
        if (!itemId || itemId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            const result = await con.query(Queries.UPDATE_ITEM_ACTIVE, [ itemId ]);
            con.commit();
            return result;
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    addDevice = async (device, hashedKey) => {
        if (!device) return;
        if (!hashedKey || hashedKey === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            const result = await con.query(Queries.INSERT_NEW_DEVICE, [ device.name, device.mac, hashedKey ]);
            con.commit();
            if (!result || !result.insertId) return;
            return new Device(result.insertId, device.name, device.mac, device.key, null);
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    addItem = async (tagId) => {
        if (!tagId || tagId === '') return;
        let con;
        try {
            const now = await createMariaDBDate(new Date())
            con = await pool.getConnection();
            con.beginTransaction();
            const result = await con.query(Queries.INSERT_NEW_ITEM, [ tagId, now ]);
            con.commit();
            if (!result.insertId) return;
            return { itemId: result.insertId, tagId, addDate: now };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    addUser = async (cardId) => {
        if (!cardId || cardId === '') return;
        let con;
        const addDate = await createMariaDBDate(new Date())
        try {
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.INSERT_NEW_USER, [ cardId, addDate ]);
            con.commit();
            if (!result) return;
            const userData = { userId: result.insertId, cardId };
            return userData;
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    deleteAdmin = async (userId) => {
        if (userId === null || userId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.DELETE_ADMIN, [ userId ]);
            con.commit();
            if (result.affectedRows === 0) return;
            return { userId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    deleteDevice = async (deviceId) => {
        if (deviceId === null || deviceId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.DELETE_DEVICE, [ deviceId ]);
            con.commit();
            if (result.affectedRows === 0) return;
            return { deviceId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    deleteItem = async (itemId) => {
        if (itemId === null || itemId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.DELETE_ITEM, [ itemId ]);
            con.commit();
            if (result.affectedRows === 0) return;
            return { itemId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    deleteUser = async (userId) => {
        if (userId === null || userId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.DELETE_USER, [ userId ]);
            con.commit();
            if (result.affectedRows === 0) return;
            return { userId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    extendRent = async (id, returnDate) => {
        if (!id || !returnDate) return;
        let con;
        try {
            returnDate = await createMariaDBDate(returnDate);
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.UPDATE_RENT_RETURNDATE, [ returnDate, id ]);
            con.commit();
            if (!result) return;
            return { id, newReturnDate: returnDate };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getActiveRents = async () => {
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENTS_ALL);
            if (!rows) return;
            const rents = [];
            for (let r of rows) {
                rents.push(new Rent(r.id, r.firstName + ' ' + r.lastName, r.userId, r.item, r.itemId, r.rentDate, r.returnDate, r.extensions));
            }
            return rents;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getAdminByUserId = async (userId) => {
        if (!userId || userId === '') return
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ADMIN_BY_USER_ID, [ userId ]);
            if (!rows) return;
            return rows[0];
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getAdmins = async () => {
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ALL_ADMINS);
            if (!rows) return;
            const admins = [];
            for (const r of rows) {
                admins.push(r);
            }
            return admins;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getDelayedRents = async () => {
        let con;
        let date = await createMariaDBDate(new Date());
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENTS_DELAYED, [ date ]);
            if (rows.length <= 0) return [];
            const rentsDelayed = [];
            for (let r of rows) {
                rentsDelayed.push(r);
            }
            return rentsDelayed;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getDeviceData = async (data) => {
        if (!data) return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_DEVICE_DATA, [ data.mac ]);
            //Mac nicht vorhanden -> Kein Zugriff
            if (!rows || rows.length <= 0) return;
            return rows[0];
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getDevices = async () => {
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ALL_DEVICES);
            //Mac nicht vorhanden -> Kein Zugriff
            if (!rows || rows.length <= 0) return;
            const devices = []
            for (const r of rows) {
                devices.push(new Device(r.id, r.name, r.mac, r.accessKey, r.lastSeen));
            }
            return devices;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getInventory = async () => {
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_INVENTORY);
            if (rows.length <= 0) return [];
            const inventory = [];
            for (let r of rows) {
                const rent = (r.numberRent >= r.numberExisting) && r.numberExisting > 0;
                inventory.push(new Item(r.id, r.tagId, r.name, r.numberExisting, r.numberRent, 
                    r.description, rent, r.active, r.blocked, r.addDate, r.removeDate));
            }
            return inventory;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        } 
    }

    getItemById = async (itemId) => {
        if (!itemId || itemId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ITEM_BY_ID, [ itemId ]);
            if (rows.length <= 0) return;
            const r = rows[0];
            const rent = r.numberExisting <= r.numberRent;
            return new Item(r.id, r.tagId, r.name, r.numberExisting, r.numberRent, r.description, 
                rent, r.active, r.blocked, r.addDate, r.removeDate);
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getItemByTag = async (tagId) => {
        if (!tagId || tagId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ITEM_BY_TAG, [ tagId ]);
            if (rows.length <= 0) return;
            const r = rows[0];
            const rent = r.numberExisting <= r.numberRent;
            return new Item(r.id, r.tagId, r.name, r.numberExisting, r.numberRent, r.description, 
                rent, r.active, r.blocked, r.addDate, r.removeDate);
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getRentById = async (rentId) => {
        if (!rentId || rentId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENT_BY_ID, [ rentId ]);
            if (!rows || rows.length <= 0) return;
            const r = rows[0];
            return new Rent(r.id, '', r.user, '', r.item, r.rentDate, r.returnDate, r.extensions);
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getRentsByItemId = async (itemId) => {
        if (!itemId || itemId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENTS_BY_ITEM_ID, [ itemId ]);
            if (!rows || rows.length <= 0) return;
            const rents = [];
            for (const r of rows) {
                rents.push(new Rent(null, r.firstName + ' ' + r.lastName, r.userId, r.itemName, r.itemId, r.rentDate, r.returnDate, r.extensions));
            }
            return rents;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getRentsByTag = async (tagId) => {
        if (!tagId || tagId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENTS_BY_TAG, [ tagId ]);
            if (rows.length <= 0) return;
            const rents = [];
            for (const r of rows) {
                rents.push(new Rent(r.id, '', r.user, '', r.item, r.rent_date, r.return_date, r.extensions));
            }
            return rents;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getRentsByTagOrderedByReturn = async (tagId) => {
        if (!tagId || tagId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENTS_BY_TAG_ORDER_BY_RETURN, [ tagId ]);
            if (rows.length <= 0) return;
            const rents = [];
            for (const r of rows) {
                rents.push(new Rent(r.id, '', r.user, '', r.item, r.rent_date, r.return_date, r.extensions));
            }
            return rents;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getRentHistory = async (itemId) => {
        if (!itemId || itemId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_RENT_HISTORY_OF_ITEM, [ itemId ]);
            if (!rows) return;
            const rentHistory = [];
            for (let r of rows) {
                rentHistory.push(new Rent(r.id, r.firstName + ' ' + r.lastName, r.userId, '', r.itemId, r.rentDate, r.returnDate, r.extensions));
            }
            return rentHistory;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getUser = async (userId) => {
        if (!userId || userId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_USER, [ userId ]);
            //UserTagId nicht vorhanden -> Kein Zugriff
            if (rows.length <= 0) return;
            const r = rows[0]
            const user = new User(r.id, r.cardId, r.firstName, r.lastName, r.role, r.matrikel, 
                r.mail, r.admin, r.approved, r.active, r.lastSeen, r.addDate. r.removeDate);
            return user;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getUserData = async (cardId) => {
        if (!cardId || cardId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_USER_DATA, [ cardId ]);
            //cardId nicht vorhanden -> Kein Zugriff
            if (rows.length <= 0) return;
            const r = rows[0]
            const user = new User(r.id, cardId, r.firstName, r.lastName, r.role, r.matrikel, 
                r.mail, r.admin, r.approved, r.active, r.lastSeen, r.addDate, r.removeDate);
            return { user, password: r.password };
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getUserHistory = async (userId) => {
        if (!userId || userId === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_USER_HISTORY, [ userId ]);
            const rents = await con.query(Queries.GET_RENTS_BY_USER_ID, [ userId]);
            if (rows.length <= 0) return;
            const userHistory = [];
            for (let r of rows) {
                userHistory.push(new Rent(r.rentId, r.firstName + ' ' + r.lastName, userId, r.itemName, r.itemId, r.rentDate, r.returnDate, r.extensions));
            }
            for (let r of rents) {
                userHistory.unshift(new Rent(null, r.firstName + ' ' + r.lastName, userId, r.itemName, r.itemId, r.rentDate, r.returnDate, r.extensions));
            }
            return userHistory;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getUsers = async () => {
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_ALL_USERS);
            //UserTagId nicht vorhanden -> Kein Zugriff
            if (rows.length <= 0) return;
            const users = [];
            for (let r of rows) {
                const user = new User(r.id, r.cardId, r.firstName, r.lastName, r.role, r.matrikel, 
                    r.mail, r.admin, r.approved, r.active, r.lastSeen, r.addDate, r.removeDate);
                users.push(user);
            }
            return users;
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    getWebUserData = async (userData) => {
        if (!userData | userData.username === '') return;
        let con;
        try {
            con = await pool.getConnection();
            const rows = await con.query(Queries.GET_WEB_USER_DATA, [ userData.username ]);
            //UserTagId nicht vorhanden -> Kein Zugriff
            if (rows.length <= 0) return;
            const r = rows[0]
            const user = new User(r.id, r.cardId, r.firstName, r.lastName, r.role, r.matrikel, 
                r.mail, r.admin, r.approved, r.active, r.lastSeen, r.addDate, r.removeDate);
            user.username = userData.username;
            return { user, password: r.password};
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    removeItem = async (itemId) => {
        if (itemId === null || itemId === '') return;
        let con;
        try {
            const now = await createMariaDBDate(new Date());
            con = await pool.getConnection();
            con.beginTransaction()
            const result = await con.query(Queries.REMOVE_ITEM, [ '', now, itemId ]);
            con.commit();
            if (result.affectedRows === 0) return;
            return { itemId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    removeUser = async (userId) => {
        if (!userId || userId === '') return;
        let con;
        const removeDate = await createMariaDBDate(new Date());
        try {
            con = await pool.getConnection();
            await con.beginTransaction();
            const res = await con.query(Queries.REMOVE_USER, [ '', removeDate, userId ]);
            con.commit();
            if (!res || res.affectedRows === 0) return;
            return { userId };
        } catch (err) {
            con.rollback();
            throw err;
        }
    }

    rent = async (itemId, user, returnDate) => {
        if (!itemId || !user) return;
        let con;
        try {
            const now = await createMariaDBDate(new Date());
            returnDate = await createMariaDBDate(returnDate);
            con = await pool.getConnection();
            await con.beginTransaction();
            con.query(Queries.NUMBER_RENT_INCR, [ itemId ]);
            const insertResult = await con.query(Queries.INSERT_NEW_RENT, [ user.id, itemId, now, returnDate ]);
            con.commit();
            if (insertResult.affectedRows === 0) return;
            return new Rent(insertResult.insertId, '', user.id, '', itemId, now, returnDate, 0);
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        } 
    }

    returnItem = async (rent) => {
        if (!rent) return;
        let con;
        try {
            const now = await createMariaDBDate(new Date())
            con = await pool.getConnection();
            await con.beginTransaction();
            con.query(Queries.NUMBER_RENT_DECR, [ rent.itemId ]);
            con.query(Queries.DELETE_RENT, [ rent.id ]);
            const insertResult = await con.query(Queries.INSERT_NEW_RENTHISTORY, 
                [ rent.userId, rent.itemId, rent.rentDate, now, rent.extensions ]);
            con.commit();
            if (!insertResult || insertResult.insertId === 0) return;
            return { rentHistoryId: insertResult.insertId };
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    setAdminData = async (userId, username, password) => {
        if (!userId || userId === '') return;
        if (!username || username === '') return;

        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            let res;
            if (password) res = await con.query(Queries.SET_ADMIN_DATA_OR_CREATE, [ userId, username, password, username, password ]);
            else res = await con.query(Queries.SET_ADMIN_DATA, [ username, userId ]);
            con.commit();
            if (!res || res.effectedRows === 0) return;
            return username;
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }

    setBlockItem = async (isBlocked, id) => {
        if (!id) return;
        let con
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            const res = await con.query(Queries.UPDATE_ITEM_BLOCK, [isBlocked, id ]);
            if (!res || res.affectedRows === 0) return;
            con.commit();
            return id;
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }  
    }

    setDeviceData = async (device, hashedKey) => {
        if (!device) return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            let res;
            if (hashedKey && hashedKey !== '') res = await con.query(Queries.SET_DEVICE_DATA, [ device.name, device.mac, hashedKey, data.id ]);
            else res = await con.query(Queries.SET_DEVICE_DATA_WITHOUT_KEY, [ device.name, device.mac, data.id ]);
            con.commit();
            if (!res || res.effectedRows === 0) return;
            return new Device(device.id, device.name, device.mac, device.key, device.lastSeen);
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        } 
    }

    setHeartbeat = async (userId, mac) => {
        let con;
        try {
            const now = await createMariaDBDate(new Date());
            con = await pool.getConnection();
            await con.beginTransaction();
            if (mac && mac !== '') await con.query(Queries.UPDATE_LAST_SEEN_DEVICE, [ now, mac ]);
            if (userId && userId !== '') await con.query(Queries.UPDATE_LAST_SEEN_USER, [ now, userId ]);
            con.commit();
            return;
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }
    
    setItemData = async (data) => {
        if (!data) return;
        console.log(data);
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            const res = await con.query(Queries.UPDATE_ITEM_DATA, 
            // 'UPDATE inventory SET name = ?, number_existing = ?, description = ?, active = ?, blocked = ? WHERE id = ?;',
                [ data.name, data.numberExisting, data.description, data.active, data.blocked, data.id ]);
            con.commit();
            if (!res || res.effectedRows === 0) return;
            const rent = data.numberExisting <= data.numberRent;
            return new Item(data.id, data.tagId, data.name, data.numberExisting, data.numberRent, 
                data.description, rent, data.active, data.blocked, data.addData, data.removeDate);
        } catch (err) {
            throw err;
        } finally {
            if (con) con.end();
        } 
    }

    setUserData = async (data) => {
        if (!data) return;
        let con;
        try {
            con = await pool.getConnection();
            con.beginTransaction();
            const res = await con.query(Queries.UPDATE_USER_DATA, 
                [ data.firstName, data.lastName, data.role, data.matrikel, data.mail, data.admin, data.approved, data.id ]);
            con.commit();
            if (!res || res.effectedRows === 0) return;
            return new User(data.id, data.cardId, data.firstName, data.lastName, data.role, data.matrikel, data.mail,
                data.admin, data.approved, data.active, data.lestSeen, data.addDate, data.removeDate);
        } catch (err) {
            con.rollback();
            throw err;
        } finally {
            if (con) con.end();
        }
    }
}

// Erstellt aus einem JS-Date-Objekt eine DateTime im MariaDB-Format.
const createMariaDBDate = async (data) => {
    return data.toISOString().slice(0, 19).replace('T', ' ');;
}

module.exports = { DBConnector }
