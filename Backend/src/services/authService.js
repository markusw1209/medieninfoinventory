'use strict'

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dbConnector = require('./dbConnector');
const dotenv = require('dotenv');
const { User } = require('../classes/User');

dotenv.config({ path: '../../deployment/.env' });

const jwtExpireTime = process.env.JWT_EXPIRE_TIME; //Anzahl Millisekunden Gültigkeit des JWT

class AuthService {

    constructor() {
        this.dbConnector = new dbConnector.DBConnector();
    };
    
    // Neues Gerät erstellen
    addNewDevice = async (data) => {
        if (!data) return;
        console.log(data);
        try {
            const devices = await this.dbConnector.getDevices();
            for (let d of devices) {
                if (d.mac === data.mac) return { error: 'device already exists!'};
            }
            if (!data.name || data.name === '' || !data.mac || data.mac === '') return;
            const hashedKey = await hashKey(data.key);
            const device = { name: data.name, mac: data.mac, key: data.key };
            const result = await this.dbConnector.addDevice(device, hashedKey);
            return result;
        } catch (err) {
            throw err;
        }
    }

    // Erstelle neuen User in der DB nur mit CardID, sonst leere Daten
    addNewUser = async (cardId) => {
        if (!cardId || cardId === '') return;
        try {
            const res = await this.dbConnector.addUser(cardId);
            if (!res) return;
            const user = new User(res.userId, res.cardId, null, null, null, null, null, false, false, null, new Date(), null);
            return user;
        } catch (err) {
            throw err;
        }
    }

    // Middleware authentifiziert Token
    authenticateJWTDevice = (req, res, next) => {
        const authHeader = req.headers['authorization'];
        // Keyword "BEARER" steht vor dem Token. Daher Token an Stelle [1]
        const accessToken = authHeader && authHeader.split(' ')[1];
        // Kein AccessToken -> Kein Zugriff.
        if (accessToken == null) return res.sendStatus(401);

        //Token verifizieren. Wenn OK -> Weiterleiten, sonst Code 401 zurückgeben und kein Zugriff.
        jwt.verify(accessToken, process.env.ACCESS_TOKEN_PRIVATE_KEY, (err, data) => {
            if (err) return res.sendStatus(401);
            //console.log('------------------------------------------');
            //console.log('Verifizierungsdaten:');
            //console.log(data);
            req.device = data.device;
            if (data.user) {
                req.user = data.user;
            }
            next();
        });
    }

    // Middleware authentifiziert Token
    authenticateJWTFrontend = (req, res, next) => {
        const authHeader = req.headers['authorization'];
        // Keyword "BEARER" steht vor dem Token. Daher Token an Stelle [1]
        const accessToken = authHeader && authHeader.split(' ')[1];
        // Kein AccessToken -> Kein Zugriff.
        if (accessToken == null) return res.sendStatus(401);

        //Token verifizieren. Wenn OK -> Weiterleiten, sonst Code 401 zurückgeben und kein Zugriff.
        jwt.verify(accessToken, process.env.ACCESS_TOKEN_PRIVATE_KEY, (err, data) => {
            if (err) return res.sendStatus(401);
            //console.log('------------------------------------------');
            //console.log('Verifizierungsdaten:');
            //console.log(data);
            //Nur Admins sollen Zugriff auf das Web-Frontend haben.
            if (data.user && data.user.admin) {
                req.user = data.user;
            }
            next();
        });
    }

    // Gerät einloggen und Token erstellen
    loginDevice = async (data) => {

        // Daten des Geräts aus der DB holen
        const deviceData = await this.dbConnector.getDeviceData(data);
        if (!deviceData) return;

        try {
            // Mac und Key OK (validiert mit Bcrypt) -> Zugriff gestatten
            // und JWT generieren
            if (await bcrypt.compare(data.key, deviceData.accessKey)) {
                const now = Date.now();
                const accessToken = jwt.sign(
                    { 
                        device: data.mac, 
                        timestamp: now 
                    }, 
                    process.env.ACCESS_TOKEN_PRIVATE_KEY, 
                    { expiresIn: jwtExpireTime + 's' }
                );
                console.log('Gerät ' + data.mac + ' erfolgreich eingeloggt.');
                return { accessToken };
            // Key nicht OK -> Kein Zugriff
            } else {
                console.log('Gerät ' + data.mac + ': Login fehlgeschlagen.');
                return;
            }
        //Fehler -> Kein Zugriff
        } catch (err) {
            throw err;
        }
    }

    // Gerät einloggen und Token erstellen
    loginUser = async (deviceData, userTagId) => {
        // Daten des Geräts aus der DB holen
        const dbUserData = await this.dbConnector.getUserData(userTagId);
        if (dbUserData && !dbUserData.error) {
            // und JWT generieren
            const now = Date.now();
            const accessToken = jwt.sign(
                {
                    user: dbUserData.user, 
                    device: deviceData,
                    timestamp: now
                }, 
                process.env.ACCESS_TOKEN_PRIVATE_KEY, 
                { expiresIn: jwtExpireTime + 's'  }
            );
            return { user: dbUserData.user, accessToken };
        } else {
            // Passwort nicht OK -> Kein Zugriff
            console.log('User ' + userTagId + ': Login fehlgeschlagen.');
            return;
        }
    }

    // Gerät einloggen und Token erstellen
    loginWeb = async (userData) => {
        if (!userData) return;
        if (!userData.username || userData.username === '') return; 
        if (!userData.password || userData.password === '') return;

        // Daten des Geräts aus der DB holen
        const { user, password } = await this.dbConnector.getWebUserData(userData);
        if (!user || user.error) return;

        try {
            // Username und Passwort OK (validiert mit Bcrypt) -> Zugriff gestatten
            // und JWT generieren
            console.log(user);
            console.log(jwtExpireTime)
            if (await bcrypt.compare(userData.password, password)) {
                const accessToken = jwt.sign(
                    { user },
                    process.env.ACCESS_TOKEN_PRIVATE_KEY, 
                    { expiresIn: jwtExpireTime + 's'  }
                );
                return { accessToken };
            } else {
                // Passwort nicht OK -> Kein Zugriff
                console.log('User ' + userTagId + ': Login fehlgeschlagen.');
                return;
            }
        //Fehler -> Kein Zugriff
        } catch (err) {
            throw err;
        }
    }
}

async function hashKey (key) {
    const salt = await bcrypt.genSalt();
    const hashedKey = await bcrypt.hash(key, salt);
    return hashedKey;
}

module.exports = { AuthService };
