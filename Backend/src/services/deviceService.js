'use strict'

const bcrypt = require('bcryptjs');
const dbConnector = require('./dbConnector');
const { Device } = require('../classes/Device');

class DeviceService {

    constructor() {
        this.dbConnector = new dbConnector.DBConnector();
    };

    deleteDevice = async (deviceId) => {
        if (!deviceId || deviceId === '') return { error: 'no deviceId' };
        try {
            const res = await this.dbConnector.deleteDevice(deviceId);
            if (!res) return { error: 'device not found' };
            return res;
        } catch (err) {
            throw err;
        }
    }
    
    getDevices = async () => {
        try {
            const devices = await this.dbConnector.getDevices();
            if (!devices) return { error: 'Could not get devices'};
            return devices;
        } catch (err) {
            throw err;
        }
    }

    setDeviceData = async (device) => {
        if (!device) return;
        try {
            let hashedKey;
            if (device.key && device.key !== '') {
                hashedKey = await hashKey(device.key);
            }
            const res = await this.dbConnector.setDeviceData(device, hashedKey);
            if (!res) return { error: 'Could not set device data'};
            return res;
        } catch (err) {
            throw err;
        }
    }
}

async function hashKey (key) {
    const salt = await bcrypt.genSalt();
    const hashedKey = await bcrypt.hash(key, salt);
    return hashedKey;
}

module.exports = { DeviceService };
