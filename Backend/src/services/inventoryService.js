'use strict'

const DBConnector  = require('./dbConnector');
const Item = require('../classes/Item.js').Item;
const Rent = require('../classes/Rent.js').Rent;
const dotenv = require('dotenv');

dotenv.config({ path: '../../deployment/.env' });

const RENT_DURANCE = process.env.RENT_DURANCE; // Dauer eine Ausleihe
const MAX_EXTENSIONS = process.env.MAX_EXTENSIONS; // Maximale Anzahl von Verlängerungen einer Leihe

class InventoryService {

    constructor() {
        this.dbConnector = new DBConnector.DBConnector();
    }

    // Inventar
    activateItem = async (itemId) => {
        if (!itemId || itemId === '') return { error: 'No itemId'};
        const res = await this.dbConnector.activateItem(itemId);
        if (!res) return { error: 'Item not found'};
        console.log('Item activated! ID:', res.itemId);
        return res.itemId;
    }

    addNewItem = async (tagId) => {
        console.log('ID:', tagId);
        if (!tagId || tagId === '' ) return;

        try {
            const res = await this.dbConnector.addItem(tagId);
            if (!res) return { error: 'Item not added'};
            console.log('Item added. ID:', res.itemId, 'TagId:', res.tagId, 'AddDate:', res.addDate );
            return new Item(res.itemId, res.tagId, null, null, null, false, false, res.addDate, null);
        } catch (err) {
            throw err;
        }
    }

    blockItem = async (itemId) => {
        if (!itemId || itemId === '') return { error: 'No itemId'};
        try {
            const res = await this.dbConnector.setBlockItem(true, itemId);
            if (!res) return { error: 'Could not unblock item'};
            return itemId;
        } catch (err) {
            throw err;
        }
    }

    deleteItem = async (itemId) => {
        if (!itemId || itemId === '') return { error: 'no itemId' };
        try {
            const res = await this.dbConnector.deleteItem(itemId);
            if (!res) return { error: 'item not found' };
            return res;
        } catch (err) {
            throw err;
        }
    }

    extendRentById = async (rentId) => {
        if (!rentId || rentId === '') return;
        try {
            const rent = await this.dbConnector.getRentById(rentId);

            // Prüfe, ob der Artikel verliehen werden kann.
            if (!rent) return { error: 'No item'};
            if (rent.extensions >= MAX_EXTENSIONS) return { error: 'MaxCountExt' };

            // Wenn ja: Rückgabedatum generieren und Leihe in der DB verlängern.
            const returnDate = new Date(Date.now() + RENT_DURANCE * 24 * 60 * 60 * 1000);
            const res = await this.dbConnector.extendRent(rent.id, returnDate);

            // Wenn alles OK ist => Rückgabe der Neuen Ausleih-Daten.
            if (!res) return { error: 'Rent not extended'};
            console.log('Rent Extended: ID:', rent.id, 'itemID:', rent.item, 'returnDate:', returnDate);
            rent.returnDate = returnDate;
            rent.extensions += 1;

            return rent;
        } catch (err) {
            throw err;
        }
    }

    extendRentByTag = async (tagId, user) => {
  
        // Wenn Daten vorhanden sind: Zu verlängernde Leihe aus der DB holen.
        if (!tagId || tagId === '') return { error: 'No tagId'};
        if (!user) return { error: 'No user'};

        try {
            let rents = await this.dbConnector.getRentsByTagOrderedByReturn(tagId);
            if (!rents) return { error: 'No item'};

            // Prüfe, ob der Artikel verliehen werden kann.
            rents = rents.filter(rent => rent.userId === user.id);
            if (rents.length === 0) return { error: 'Wrong user' };

            rents = rents.filter(rent => rent.extensions < MAX_EXTENSIONS);
            if (rents.length === 0) return { error: 'Maximum count of extensions reached' };
            const rent = rents[0];

            // Wenn ja: Rückgabedatum generieren und Leihe in der DB verlängern.
            const returnDate = new Date(Date.now() + RENT_DURANCE * 24 * 60 * 60 * 1000);
            const res = await this.dbConnector.extendRent(rent.id, returnDate);

            // Wenn alles OK ist => Rückgabe der Neuen Ausleih-Daten.
            if (!res) return { error: 'Rent not extended'};
            console.log('Rent Extended: ID:', rent.id, 'itemID:', rent.item, 'returnDate:', returnDate);
            rent.returnDate = returnDate;
            rent.extensions += 1;

            return rent;
        } catch (err) {
            throw err;
        }
    }

    getAllRents = async () => {
        try {
            const res = await this.dbConnector.getActiveRents();
            if (!res) return { error: 'Could not get rents'};
            for (const r of res) {
                if (r.returnDate < new Date()) r.delayed = true;
            }
            return res;
        } catch (err) {
            throw err;
        }
    }

    getDelayedRents = async () => {
        try {
        const res = await this.dbConnector.getDelayedRents();
        if (!res) return { error: 'Could not get delayed rents'};
        return res;
        } catch (err) {
            throw err;
        }
    }
    
    getExpiringRentByTag = async (tagId, user) => {
        if (!tagId || !user) return;
        try {
            let rents = await this.dbConnector.getRentsByTagOrderedByReturn(tagId);
            if (!rents) return { error: 'Could not get rent'};
            // Wenn Leihen dieses Artikels vom Nutzer vorhanden sind, gib eine davon (die,
            // die als nächstes abläuft) zurück. Ansonsten die nächst-ablaufende eines anderen
            // Nutzers.
            const temp = rents.filter(rent => rent.userId === user.id); 
            if (temp.length > 0) rents = temp;
            return rents[0];
        } catch (err) {
            throw err;
        }
    }

    getInventory = async () => {
        try {
            const res = await this.dbConnector.getInventory();
            const items = [];
            if (!res) return { error: 'Could not get items'};
            for (let r of res) {
                if (r.active) items.push(r);
            }
            return items;
        } catch (err) {
            throw err;
        }
    }

    getItem = async (itemId) => {
        if (!itemId || itemId === '') return;
        try {
            const res = await this.dbConnector.getItemById(itemId);
            if (!res) return { error: 'item not found'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    getItemByTag = async (tagId) => {
        if (!tagId || tagId === '') return;
        try {
            const res = await this.dbConnector.getItemByTag(tagId);
            if (!res) return { error: 'item not found'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    getNewItems = async () => {
        try {
            const res = await this.dbConnector.getInventory();
            const items = [];
            if (!res) return { error: 'no items found'};
            for (let r of res) {
                if (r.name === null && !r.active && r.removeDate === null) {
                    items.push(r);
                }
            }
            return items;
        } catch (err) {
            throw err;
        }
    }

    getRentHistory = async (itemId) => {
        if (!itemId || itemId === '') return;
        try {
            const history = await this.dbConnector.getRentHistory(itemId);
            if (!history) return { error: 'Could not get rentHistory'};
            const resCurrentRents = await this.dbConnector.getRentsByItemId(itemId);
            if (!resCurrentRents) return history;
            console.log(resCurrentRents);
            for (const r of resCurrentRents) {
                history.unshift(r);
            } 
            return history;
        } catch (err) {
            throw err;
        }
    }

    getRentsByTag = async (tagId) => {
        if (!tagId) return;
        try {
            const res = await this.dbConnector.getRentsByTag(tagId);
            if (!res) return { error: 'Could not get rent'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    getRentsByItemId = async (itemId) => {
        if (!itemId || itemId === '') return;
        try {
            const res = await this.dbConnector.getRentsByItemId(itemId);
            if (!res) return { error: 'Could not get rent'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    removeItem = async (itemId) => {
        if (!itemId || itemId === '') return { error: 'no itemId'};
        try {
            const res = await this.dbConnector.removeItem(itemId);
            if (!res) return { error: 'item not found'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    // Artikel aus dem Inventar ausleihen
    rentItem = async (tagId, user) => {

        // Wenn Daten vorhanden sind: Auszuleihenden Artikel aus der DB holen.
        if (!tagId || tagId === '') return { error: 'No tagId'};
        if (!user) return { error: 'No user'};
        const item = await this.dbConnector.getItemByTag(tagId);

        // Prüfe, ob der Artikel verliehen werden kann.
        if (!item) return { error: 'No item' };
        if (item.blocked) return { error: 'item blocked' };
        if (!item.active) return { error: 'item not active' };
        if (item.rent) return { error: 'item just rent' };
        // Wenn ja: Rückgabedatum generieren und Artikel in der DB ausleihen.
        const returnDate = new Date(Date.now() + (RENT_DURANCE * 24 * 60 * 60 * 1000));
        const rent = await this.dbConnector.rent(item.id, user, returnDate);

        // Wenn alles OK ist => Rückgabe der Neuen Ausleih-Daten.
        if (!rent) return { error: 'Could not rent item'};
        return rent;
    }

    returnItemByTag = async (tagId, user) => {
        
        // Wenn Daten vorhanden sind: Zurückzugebende Leihe aus der DB holen.
        if (!tagId || tagId === '') return { error: 'No tagId'};
        try {
            // Prüfe, ob der Artikel verliehen ist
            let rents = await this.dbConnector.getRentsByTagOrderedByReturn(tagId);
            if (!rents) return { error: 'Item not rent'};

            // Prüfe, ob der Artikel zurückgegeben werden kann.
            rents = rents.filter(rent => rent.userId === user.id);
            if (rents.length === 0) return { error: 'Wrong user' };
            const rent = rents[0];
            
            // Wenn ja: Leihe entfernen, in Historie speichern und Artikel wieder freigeben.
            const res = await this.dbConnector.returnItem(rent);
            if (!res) return { error: 'Could not return item'};
            return rent;
        } catch (err) {
            throw err;
        }
    }

    returnItemById = async (itemId) => {

        // Wenn Daten vorhanden sind: Zurückzugebende Leihe aus der DB holen.
        if (!itemId || itemId === '') return { error: 'No itemId'};
        try {
            const rent = await this.dbConnector.getRentByItemId(itemId);
            // Prüfe, ob der Artikel verliehen ist
            if (!rent) return { error: 'Item not rent'};
            // Wenn ja: Leihe entfernen, in Historie speichern und Artikel wieder freigeben.
            const res = await this.dbConnector.returnItem(rent);
            if (!res) return { error: 'Could not return item'};
            return rent;
        } catch (err) {
            throw err;
        }
    }

    // Daten eines Artikels aus dem Inventar ändern.
    setItemData = async (item) => {
        if (!item) return;
        try {
            const res = await this.dbConnector.setItemData(item);
            if (!res) return { error: 'Could not set item data'};
            return res;
        } catch (err) {
            throw err;
        }
    }

    // Block eines Artikels im Inventar aufheben.
    unblockItem = async (itemId) => {
        if (!itemId || itemId === '') return { error: 'No itemId'};
        try {
            const res = await this.dbConnector.setBlockItem(false, itemId);
            if (!res) return { error: 'Could not unblock item'};
            return itemId;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = { InventoryService };