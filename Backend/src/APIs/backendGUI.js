'use strict'

const { AuthService } = require('../services/authService.js');
const { DeviceService } = require('../services/deviceService.js');
const { InventoryService } = require('../services/inventoryService.js');
const { UserService } = require('../services/userService.js');

// Stellt API-Endpunkte für das Web-Frontend zur Verfügung.
class BackendGUI {

    constructor(server) {
        this.server = server;
        //Services erzeugen
        this.authService = new AuthService();
        this.deviceService = new DeviceService();
        this.inventoryService = new InventoryService();
        this.userService = new UserService();
        //Api starten
        this.run();
    }

    run = () => {

        this.server.post('/web/blockItem', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'blocks item.');
            try {
                this.userService.registerHeartbeat(user);
                const itemId = await this.inventoryService.blockItem(data.itemId);
                if (!itemId) {
                    console.log('Fehler: Item not found');
                    res.sendStatus(400);
                } else {
                    console.log('Item blocked. ID:', itemId);
                    res.sendStatus(200);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/createDevice', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'creates new device.');
            try {
                this.userService.registerHeartbeat(user);
                const device = await this.authService.addNewDevice(data);
                if (!device) {
                    console.log('Fehler: Device nicht erstellt!');
                    res.sendStatus(400);
                } else {
                    console.log('Device erstellt:', device);
                    res.send(device);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/deleteDevice', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'deletes device.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.deviceService.deleteDevice(data.id);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Device deleted. ID:', result.deviceId);
                    //console.log(data);
                    res.send(data);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/deleteItem', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'deletes item.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.inventoryService.deleteItem(data.id);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Item deleted. ID:', result.itemId);
                    console.log(data);
                    res.send(data);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/deleteUser', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'deletes user.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.userService.deleteUser(data.id);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('User deleted. ID:', result.userId);
                    res.sendStatus(400);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/extendRent', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'extends rent.');
            try {
                this.userService.registerHeartbeat(user);
                const rent = await this.inventoryService.extendRentById(data.id);
                if (!rent) res.sendStatus(400);
                if (rent.error) {
                    console.log('Fehler:', rent.error);
                    if (rent.error === 'MaxCountExt') res.status(406).send(rent.error);
                    else res.sendStatus(400);
                } else {
                    //console.log('Rent extended:');
                    //console.log(rent);
                    res.send(rent);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getDelayedRents', this.authService.authenticateJWTFrontend, async (req, res) => {
            const user = await setUserAndLog(req.user, 'asks for delayed rents.');
            try {
                this.userService.registerHeartbeat(user);
                const rents = await this.inventoryService.getDelayedRents();
                if (!rents) res.sendStatus(400);
                if (rents.error) {
                    console.log('Fehler:', rents.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Delayed rents:');
                    //console.log(rents);
                    res.send(rents);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getDevices', this.authService.authenticateJWTFrontend, async  (req, res) => {
            const { user } = await setUserAndLog(req.user, 'asks for devices.');
            try {
                this.userService.registerHeartbeat(user);
                let devices;
                devices = await this.deviceService.getDevices();
                if (!devices) res.sendStatus(400);
                if (devices.error) {
                    console.log('Fehler:', devices.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Devices:');
                    //console.log(devices);
                    res.send(devices);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getInventory', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.query, req.user, 'asks for items.');
            try {
                this.userService.registerHeartbeat(user);
                let items;
                if (!data.id) items = await this.inventoryService.getInventory();
                else items = await this.inventoryService.getItem(data.id);
                if (!items) res.sendStatus(400);
                if (items.error) {
                    console.log('Fehler:', items.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Items:');
                    //console.log(items);
                    res.send(items);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getNewUsers', this.authService.authenticateJWTFrontend,  async (req, res) => {
            const user = await setUserAndLog(req.user, 'asks for new users.');
            try {
                this.userService.registerHeartbeat(user);
                const users = await this.userService.getNewUsers();
                if (!users) res.sendStatus(400);
                if (users.error) {
                    console.log('Fehler:', users.error);
                    res.sendStatus(400);
                } else {
                    //console.log('New Users:');
                    //console.log(users);
                    res.send(users);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getNewItems', this.authService.authenticateJWTFrontend,  async (req, res) => {
            const user = await setUserAndLog(req.user, 'asks for new items.');
            try {
                this.userService.registerHeartbeat(user);
                const items = await this.inventoryService.getNewItems();
                if (!items) res.sendStatus(400);
                if (items.error) {
                    console.log('Fehler:', items.error);
                    res.sendStatus(400);
                } else {
                    //console.log('New Items:');
                    //console.log(items);
                    res.send(items);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getRents', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.query, req.user, 'asks for rents.')
            try {
                this.userService.registerHeartbeat(user);
                let rents;
                if (!data.itemId) rents = await this.inventoryService.getAllRents();
                else rents = await this.inventoryService.getRentsByItemId(data.itemId);
                if (!rents) res.sendStatus(400);
                if (rents.error) {
                    console.log('Fehler:', rents.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Rents:');
                    //console.log(rents);
                    res.send(rents);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getRentHistory', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.query, req.user, 'asks for rentHistory of an item.');
            if (!data.id || data.id <= 0) res.sendStatus(400);
            try {
                this.userService.registerHeartbeat(user);
                const rents = await this.inventoryService.getRentHistory(data.id);
                if (!rents) res.sendStatus(400);
                if (rents.error) {
                    console.log('Fehler:', rents.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Rents:');
                    //console.log(rents);
                    res.send(rents);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getUsers', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.query, req.user, 'asks for users.');
            try {
                this.userService.registerHeartbeat(user);
                let users;
                if (!data.id) users = await this.userService.getActiveUsers();
                else users = await this.userService.getUser(data.id);
                if (!users) res.sendStatus(400);
                if (users.error) {
                    console.log('Fehler:', users.error);
                    res.sendStatus(400);
                } else {
                    //console.log('Active Users:');
                    //console.log(users);
                    res.send(users);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.get('/web/getUserHistory', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.query, req.user, 'asks for rentHistory of an item.');
            console.log(data);
            if (!data.id || data.id <= 0) res.sendStatus(400);
            try {
                this.userService.registerHeartbeat(user);
                const rents = await this.userService.getUserHistory(data.id);
                if (!rents) res.sendStatus(400);
                if (rents.error) {
                    console.log('Fehler:', rents.error);
                    res.sendStatus(400);
                } else {
                    console.log('UserHistory:');
                    console.log(rents);
                    res.send(rents);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/login', async (req, res) => {
            const data = req.body;
            // Keine Datan im Body -> Kein Zugriff
            if (!data) res.send(401);

            console.log('------------------------------------------');
            console.log(new Date(Date.now()));
            console.log('Login Admin (Web)', data.username);

            try {
                const userData = await this.authService.loginWeb(data);
                console.log(userData);
                if (!userData) res.sendStatus(401);
                else {
                    this.userService.registerHeartbeat(userData.user);
                    res.send(userData);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(401);
            }
        });

        this.server.post('/web/removeItem', this.authService.authenticateJWTFrontend,  async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'removes item.');
            console.log(data);
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.inventoryService.removeItem(data.id);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Item removed. ID:', result.itemId);
                    res.send(result);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/removeUser', this.authService.authenticateJWTFrontend,  async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'removes user.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.userService.removeUser(data.id);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('User removed. ID:', result.userId);
                    res.send(result);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/returnItem', this.authService.authenticateJWTDevice, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'returns item.');
            console.log(data);
            try {
                this.userService.registerHeartbeat(user);
                const rent = await this.inventoryService.returnItemById(data.id);
                if (!rent || rent.error) {
                    if (rent) console.log(rent.error);
                    res.sendStatus(400);
                } else {
                    console.log('Returns rent:', rent);
                    res.send(rent);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/setDeviceData', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'sets Data of a device.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.deviceService.setDeviceData(data);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Devicedata set:', result);
                    res.send(result);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/setItemData', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'sets Data of a device.');
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.inventoryService.setItemData(data);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Itemdata set:', result);
                    res.send(result);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/setUserData', this.authService.authenticateJWTFrontend, async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'sets Data of a user.');
            console.log(data);
            try {
                this.userService.registerHeartbeat(user);
                const result = await this.userService.setUserData(data);
                if (!result) res.sendStatus(400);
                if (result.error) {
                    console.log('Fehler:', result.error);
                    res.sendStatus(400);
                } else {
                    console.log('Userdata set:', result);
                    res.send(result);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });

        this.server.post('/web/unblockItem', this.authService.authenticateJWTFrontend,  async (req, res) => {
            const { user, data } = await setDataAndLog(req.body, req.user, 'unblocks item.');
            try {
                this.userService.registerHeartbeat(user);
                const itemId = await this.inventoryService.unblockItem(data.itemId);
                if (!itemId) {
                    console.log('Fehler: Item not found');
                    res.sendStatus(400);
                } else {
                    console.log('Item unblocked. ID:', itemId);
                    res.sendStatus(200);
                }
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }
        });
    }
}

const setDataAndLog = async (data, user, text) => {
    console.log('------------------------------------------');
    console.log(new Date(Date.now()));
    console.log('Admin (Web)', user.firstName, user.lastName, text);
    return { user, data };
}

const setUserAndLog = async (user, text) => {
    console.log('------------------------------------------');
    console.log(new Date(Date.now()));
    console.log('Admin (Web)', user.firstName, user.lastName, text);

    return user;
}

module.exports = { BackendGUI }