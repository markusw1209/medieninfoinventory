'use strict'

const AuthService = require('../services/authService.js');
const InventoryService = require('../services/inventoryService.js');
const UserService = require('../services/userService.js');

// Stellt API-Endpunkte für das IoT-Device zur Verfügung.
// Das IoT-Device soll aufgrund seiner beschränkten Rechenleistung möglichst einfach gehalten sein
// und überträgt daher zur Identifizierung nur die IDs der RFID-Tags.
// Die Autorisierung des Devices und des Users erfolgt nach dem Login per Karte über JsonWebTokens.
class BackendIoT {

    constructor(server) {
        this.server = server;
        //DBConnector und AuthService erzeugen
        this.authService = new AuthService.AuthService();
        this.inventoryService = new InventoryService.InventoryService();
        this.userService = new UserService.UserService();
        //Api starten;
        this.run();
    }

    run = () => {
        // Neue Inventar-Item erstellen mit übertragener TagId
        this.server.post('/iot/createItem', this.authService.authenticateJWTDevice, async (req, res) => {
            // User ist kein Admin -> Kein Zugriff auf diese Funktion
            if (!req.user || !req.user.admin) res.sendStatus(401);
            else {
                const { user, data, device } =  await setDataAndLog(req.user, req.body, req.device, 'creates a new item.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const item = await this.inventoryService.addNewItem(data.tagId);
                    if (item) {
                        console.log('New Item:', item);
                        res.sendStatus(201);
                    } else {
                        res.sendStatus(400);
                    }
                } catch (err) {
                    if (err.code === 'ER_DUP_ENTRY') {
                        res.status(400).send('Duplicate Entry');
                    } else {
                        res.sendStatus(400);
                    }
                }
            }
        });

        // Neuen User erstellen mit übertragener 
        this.server.post('/iot/createUser', this.authService.authenticateJWTDevice, async (req, res) => {
            // User ist kein Admin -> Kein Zugriff auf diese Funktion
            if (!req.user || !req.user.admin) res.sendStatus(401);
            else {
                const { user, data, device } =  await setDataAndLog(req.user, req.body, req.device, 'creates a new user.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const newUser = await this.authService.addNewUser(data.cardId);
                    if (newUser) {
                        console.log('New User:', newUser);
                        res.sendStatus(201);
                    } else {
                        res.sendStatus(400);
                    }
                } catch (err) {
                    if (err.code === 'ER_DUP_ENTRY') {
                        res.status(400).send('Duplicate Entry');
                    } else {
                        res.sendStatus(400);
                    }
                }
            }
        });

        this.server.post('/iot/extendRent' , this.authService.authenticateJWTDevice, async (req, res) => {
            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user) res.sendStatus(401);
            else {
                const { user, data, device } =  await setDataAndLog(req.user, req.body, req.device, 'extends rent.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const rent = await this.inventoryService.extendRentByTag(data.tagId, user);
                    if (!rent || rent.error) {
                        if (rent) {
                            console.log('Fehler:', rent.error);
                            res.status(400).send(rent.error);
                        } else res.sendStatus(400);
                    } else {
                        console.log('Extended rent:', rent);
                        res.status(200).send(rent.returnDate.toLocaleString('de-DE'));
                    }
                } catch (err) {
                    console.log(err);
                    res.sendStatus(400);
                }
            }
        });

        this.server.get('/iot/getItem', this.authService.authenticateJWTDevice, async (req, res) => {
            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user) res.sendStatus(401);
            else {
                const { user, data, device } = await setDataAndLog(req.user, req.query, req.device, 'asks for item.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const item = await this.inventoryService.getItemByTag(data.tagId);
                    if (!item || item.error) {
                        if (item) console.log(item.error);
                        res.sendStatus(400);
                    }else {
                        console.log('Item:', item);
                        res.send(item);
                    }
                } catch (err) {
                    console.log(err);
                    res.sendStatus(400);
                }
            }
        });

        this.server.get('/iot/getExpiringRent', this.authService.authenticateJWTDevice, async (req, res) => {
            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user) res.sendStatus(401);
            else {
                const { user, data, device } = await setDataAndLog(req.user, req.query, req.device, 'asks for rent.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const rent = await this.inventoryService.getExpiringRentByTag(data.tagId, user);
                    if (!rent || rent.error) {
                        if (rent) console.log(rent.error);
                        res.sendStatus(400);
                    } else {
                        console.log('Rent:', rent);
                        res.send(rent);
                    }
                } catch (err) {
                    console.log(err);
                    res.sendStatus(400);
                }
            }
        });

        this.server.post('/iot/heartbeat', this.authService.authenticateJWTDevice, async (req, res) => {
            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            const user = req.user;
            const device = req.device;

            try {
                this.userService.registerHeartbeat(user, device);
                res.sendStatus(200);
            } catch (err) {
                console.log(err);
                res.sendStatus(400);
            }

            console.log('------------------------------------------');
            console.log(new Date(Date.now()));
            if (req.user && req.device) console.log('User', user.firstName, user.lastName, 'and device', device, 'send heartbeat.');
            else if (req.device) console.log('Device', device, 'sends heartbeat.');
            else if (req.user) console.log('User', user.firstName, user.lastName, 'sends heartbeat.');
        });

        this.server.get('/iot/isUser', this.authService.authenticateJWTDevice, async (req, res) => {
            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user || !req.user.admin) res.sendStatus(401);
            else {
                const { user, data, device } = await setDataAndLog(req.user, req.query, req.device, 'asks for User-Check.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const isUser = await this.userService.isUser(data.cardId);
                    console.log("isUser:", isUser);
                    if (isUser.error) {
                        console.log(isUser.error);
                        res.sendStatus(400);
                    } else {
                        console.log('isUser:', isUser);
                        res.send(isUser);
                    }
                } catch (err) {
                    console.log(err);
                    res.sendStatus(400);
                }
            }
        });

        //Login des Geräts mit Mac-Adresse und Key
        this.server.post('/iot/loginDevice', async (req, res) => {
            const data = req.body;
            // Keine Datan im Body -> Kein Zugriff
            if (!data) res.send(401);
            console.log('------------------------------------------');
            console.log(new Date(Date.now()));
            console.log('Login Gerät', data.mac);

            try {
                const deviceData = await this.authService.loginDevice(data);
                console.log(deviceData);
                if (!deviceData) res.sendStatus(401);
                res.send(deviceData);
            } catch (err) {
                console.log(err);
                res.sendStatus(401);
            }
        });

        this.server.post('/iot/loginUser', this.authService.authenticateJWTDevice, async (req, res) => {

            const data = req.body;
            const device = req.device;
            // Keine Datan im Body -> Kein Zugriff
            if (data === null) res.sendStatus(401);

            console.log('------------------------------------------');
            console.log(new Date(Date.now()));
            console.log('Login User', data.cardId);

            try {
                const userData = await this.authService.loginUser(req.device, data.cardId);
                if(!userData) res.sendStatus(401);
                this.userService.registerHeartbeat(userData, device);
                res.send(userData);
            } catch (err) {
                console.log(err);
                res.sendStatus(401);
            }
        });

        this.server.post('/iot/rentItem', this.authService.authenticateJWTDevice, async (req, res) => {

            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user) res.sendStatus(401);
            else {
                const { user, data, device } = await setDataAndLog(req.user, req.body, req.device, 'rents item.');
                try {
                    this.userService.registerHeartbeat(user, device);
                    const rent = await this.inventoryService.rentItem(data.tagId, user);
                    if (!rent || rent.error) {
                        if (rent) console.log(rent.error);
                        res.sendStatus(400);
                    }else {
                        console.log('Rent:', rent);
                        const returnDate = await createDateFromMariaDBDateTime(rent.returnDate);
                        returnDate.setHours(returnDate.getHours() + 1);
                        res.status(200).send(returnDate.toLocaleString('de-DE'));
                    }
                } catch (err) {
                    console.log(err);
                    res.sendStatus(400);
                }
            }
        });

        this.server.post('/iot/returnItem', this.authService.authenticateJWTDevice, async (req, res) => {

            // Kein eingeloggter User -> Kein Zugriff auf diese Funktion
            if (!req.user) res.sendStatus(401);
            else {
                const { user, data, device } =  await setDataAndLog(req.user, req.body, req.device, 'returns item.');
                try {
                    this.userService.registerHeartbeat(user, device);
                const rent = await this.inventoryService.returnItemByTag(data.tagId, user);
                if (!rent || rent.error) {
                    if (rent) {
                        console.log(rent.error);
                        res.status(400).send(rent.error);
                    } else {
                        res.sendStatus(400);
                    }
                } else {
                    console.log('Returns rent:', rent);
                    res.sendStatus(200);
                }
                } catch (err) {
                console.log(err);
                res.sendStatus(400);
                }
            }
        });

        // Test-Funktionen für Dev-Phase
        this.server.post('/iot/testPost', (req, res) => {
            console.log("Test-Post läuft.");
            res.send('Test OK');
        });

        this.server.post('/iot/testAuth', this.authService.authenticateJWTDevice, (req, res) => {
            console.log('JWT Test läuft!');
            res.send('JWT Test OK');
        });
    }
}

const setDataAndLog = async (user, data, device, text) => {
    console.log('------------------------------------------');
    console.log(new Date(Date.now()));
    console.log('User', user.firstName, user.lastName, text);
    return { user, data, device };
}

const createDateFromMariaDBDateTime = async (dateTime) => {
    let dateTimeParts = dateTime.split(/[- :]/);
    dateTimeParts[1]--;
    return new Date(...dateTimeParts);
}

module.exports = { BackendIoT }