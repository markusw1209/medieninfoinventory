'use strict'

const bcrypt = require('bcryptjs');
const crypto = require('crypto');

//const key = getKey(32);
//genPassword(key);

genPassword('MedienInfo123');

async function genPassword (key) {
    const salt = await bcrypt.genSalt();
    const hashedKey = await bcrypt.hash(key, salt);
    console.log('Hashed:', hashedKey);
}

function getKey (Length) {
    const key = crypto.randomBytes(Length).toString('hex');
    console.log('Key:', key)
    return key
}