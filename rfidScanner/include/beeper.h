#ifndef _BEEPER_H_
#define _BEEPER_H_

#include <Arduino.h>

#define BEEPER 15

class Beeper{

    public:
    Beeper();
    void beep (uint16_t durance, uint8_t number);

};

#endif
