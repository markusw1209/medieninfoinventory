#ifndef _TEST_H_
#define _TEST_H_

#include <Arduino.h>
#include <WiFi.h>
#include <SPI.h>
#include <Wire.h>
#include "connection.h"
#include "beeper.h"
#include "rfid.h"
#include "led.h"
#include "display.h"
#include "button.h"

class Test {

    public:
    void testSetup();
    void testLoop();
    void checkOutputs();

    private: 
    Display *display;
    RFID *rfid;
    LED *blueLed;
    LED *greenLed;
    LED *redLed;
    Button *redButton;
    Button *blueButton;
    Button *greenButton;
    String currentTag;
    LED *leds[3];
    uint8_t numberOfLeds;
};

#endif