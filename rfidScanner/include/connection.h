#ifndef _CONNECTION_H_
#define _CONNECTION_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include <WiFi.h>
#include "display.h"
#include "environment.h"
#include "led.h"

class Connection {

    public:
    Connection();
    void authenticateDevice();
    bool checkConnection();
    bool checkUser(String cardId);
    String createNewItem(String tagId);
    String createNewUser(String cardId);
    String extendRent(String tagId);
    StaticJsonDocument<1024> getItem(String tagId);
    String getLocalIP();
    StaticJsonDocument<512> getExpiringRent(String tagId);
    StaticJsonDocument<1024> getUserData();
    int getStrength(uint8_t points);
    bool getLoggedIn();
    void loginUser(String cardId);
    void logout();
    void newToken();
    String rentItem(String tagId);
    String returnItem(String tagId);

    private:
    void connectToWiFi(bool firstConnection, uint16_t conInterval);
    String getMacAddress();
    String newWiFiConnection();
    void printConnectionData();
    String readResponse();
    void sendHttpRequest(String content, String method, String target, String accessToken);

    WiFiClient httpClient;
    String wifiSsid;
    String wifiPassword;
    String localIP;
    char server[50] = API_URL;
    int port; // Port des Servers

    //Daten zur Autorisierung werden erst bei Login zur Authentifizierung erzeugt (JSON-Web-Token)
    StaticJsonDocument<2048> authData;
    String key;
    bool isLoggedIn;

    uint16_t connectingCounter; //Anzahl der Verbindungsversuche zum WLAN (wird bei Verbindungsabbruch gezählt).
    uint16_t connectionInterval; //Wie lange (ms) soll auf Verbindung mit WLAN gewartet werden, bevor ein neuer Versuch gestartet wird
    uint16_t connectionUpdateInterval;
    uint32_t lastConnectionUpdate;
    
};

#endif
