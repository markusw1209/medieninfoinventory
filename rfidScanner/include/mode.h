#ifndef _MODE_H_
#define _MODE_H_

#include <Arduino.h>

enum Modes { 
    adminMenu, 
    adminWelcome, 
    chooseRentOrExtend,
    confirmNewUser, 
    confirmNewItem, 
    confirmRent, 
    confirmReturn, 
    extendRent,
    login, 
    newItem, 
    newUser, 
    noConnection,
    rent, 
    returning, 
    start, 
    userMenu, 
    userWelcome 
};

class Mode {

    public:
    Modes mode;
    bool firstTime;

    Mode(Modes mode, bool firstTime);
    void set(Modes mode, bool firstTime);

};

#endif
