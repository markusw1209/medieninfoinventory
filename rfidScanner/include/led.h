#ifndef _LED_H_
#define _LED_H_

#include <Arduino.h>
#include "helpfulFunctions.h"


class LED {

    public:
    LED(uint8_t pin);
    void check();
    bool getStatus();
    uint32_t getSwitchOffTime();
    void switchOn(uint16_t durance);
    void switchOff();
    

    private:
    bool on;
    uint8_t PIN;
    uint32_t switchOffTime;

    void switchOn();

};

#endif
