#ifndef _MAIN_H_
#define _MAIN_H_

#include <Arduino.h>
#include <SPI.h>
#include <WiFi.h>
#include "beeper.h"
#include "button.h"
#include "connection.h"
#include "display.h"
#include "led.h"
#include "mode.h"
#include "rfid.h"

Beeper *beeper;
Button *redButton;
Button *greenButton;
Connection *con;
Display *display;
LED *blueLed;
LED *greenLed;
LED *redLed;
LED *leds[3];
Mode *mode;
RFID *rfid;

String currentTag;
StaticJsonDocument<512> user;
bool connectionOK;
uint16_t connectionCheckInterval;
uint32_t lastConnectionCheck;
bool firstTime;
uint8_t numberOfLeds;

void setup();
void loop();

void adminMenuMode();
void adminWelcomeMode();
String checkButtons();
void checkConnection();
void checkOutputs();
void chooseRentOrExtendMode();
void chooseRentVersion(String itemName, bool isRent);
void confirmRentMode();
void confirmReturnMode();
void confirmNewItemMode();
void confirmNewUserMode();
void createObjects();
void extendRentMode();
void loginMode();
void newItemMode();
void newUserMode();
void noConnectionMode();
void rentMode();
void returnMode();
void startMode();
void userMenuMode();
void userWelcomeMode();

#endif
