#ifndef _RFID_H_
#define _RFID_H_

#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>
#include "beeper.h"
#include "helpfulFunctions.h"

class RFID {

    public:
    RFID();
    String getCurrentTag();
    String getLastTag();
    void reset();
    
    private:
    Beeper *beeper;
    String currentTag;
    bool newTagAvailable;
    String oldTag;
    uint8_t nuidPICC[4];
    MFRC522::MIFARE_Key key;

    String idToString(byte *buffer, byte bufferSize);
    void printDec(byte *buffer, byte bufferSize);
    void printHex(byte *buffer, byte bufferSize);
    void readTags();
};

#endif
