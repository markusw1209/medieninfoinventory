#ifndef _BUTTON_H_
#define _BUTTON_H_

#include <Arduino.h>

class Button {

    public:
    Button(uint8_t Pin);
    bool checkButton();

    private:
    uint8_t PIN;
};

#endif
