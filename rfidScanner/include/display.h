#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <Arduino.h>
#include <U8g2lib.h>
#include "helpfulFunctions.h"

class Display {

    public:
    Display();
    void clear();
    void printLongString(String s);
    void setBigText(String text, uint16_t time);
    void setText(String line1, uint16_t time, bool small);
    void setText(String line1, String line2, uint16_t time, bool small);
    void setText(String line1, String line2, String line3, uint16_t time, bool small);
    void setText(String line1, String line2, String line3, String line4, uint16_t time, bool small);
    void check();
    
    private:
    int counter;
    uint32_t clearTime;
    bool cleared;

    void displayFourLines(const char line1[], const char line2[], const char line3[], const char line4[]);
    void displayMax(const char text[]);
    void displayMiddle(const char text[]);
    void displayThreeLines(const char line1[], const char line2[], const char line3[]);
    void displayThreeLinesSmall(const char line1[], const char line2[], const char line3[]);
    void displayTwoLines(const char line1[], const char line2[]);
    void displayTwoLinesSmall(const char line1[], const char line2[]);

};

#endif
