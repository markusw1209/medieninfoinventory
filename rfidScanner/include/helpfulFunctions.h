#ifndef _HELPFULFUNCTIONS_H_
#define _HELPFULFUNCTIONS_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include <iostream>
#include <string>
#include <cstring>

bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2);
const char* convertStringToChars(String s);
void printJsonDocs(StaticJsonDocument<256> doc);
void printJsonDocs(StaticJsonDocument<512> doc);
void printJsonDocs(StaticJsonDocument<1024> doc);
void printJsonDocs(StaticJsonDocument<2048> doc);
bool stringToBoolean(String str);

#endif