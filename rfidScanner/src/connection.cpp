#include "connection.h"

bool DEBUG_CONNECTION = false;

Connection::Connection() {
    
    WiFiClient httpClient;

    this->wifiSsid = WIFI_SSID;
    this->wifiPassword = WIFI_PASSWORD;
    this->port = API_PORT; // Port des Servers
    this->key = API_KEY; // Zugangs-Key für die Anmeldung bei der API

    //Daten zur Autorisierung werden erst bei Login zur Authentifizierung erzeugt (JSON-Web-Token)
    this->isLoggedIn = false;
    this->connectingCounter = 0; //Anzahl der Verbindungsversuche zum WLAN (wird bei Verbindungsabbruch gezählt).
    this->connectionInterval = CONNECTION_INTERVAL * 1000; //Wie lange (ms) soll auf Verbindung mit WLAN gewartet werden, bevor ein neuer Versuch gestartet wird
    this->lastConnectionUpdate = 10000;
    this->localIP = this->newWiFiConnection();
}

void Connection::authenticateDevice() 
{
    if (httpClient.connect(server, port)) {
        StaticJsonDocument<256> loginData;
        String loginJson = "";
        loginData["mac"] = WiFi.macAddress();
        loginData["key"] = this->key;
        serializeJson(loginData, loginJson);
        sendHttpRequest(loginJson, "POST", "iot/loginDevice", "");
        String res = readResponse();
        if (!(res == "") && (res != "Unauthorized")) {
            this->lastConnectionUpdate = millis();
            StaticJsonDocument<512> resData;
            deserializeJson(resData, res);
            String jwt = resData["accessToken"];
            String expTimeString = resData["expireTime"];
            uint32_t expTime = millis() + expTimeString.toInt();;
            this->authData.clear();
            this->authData["accessToken"] = jwt;
            this->authData["expireTime"] = expTime;
        } else if (res == "Unauthorized") {
            this->authData.clear();
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: " + res);
        }
    }
}


bool Connection::checkConnection()
{
    bool response = false;
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("", "POST", "iot/heartbeat", jwt);
        String res = readResponse();
        if ((res == "OK")) {
            this->lastConnectionUpdate = millis();
            Serial.println("Heartbeat OK");
            response = true;
        } else if (res == "Unauthorized") {
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: " + res);
        }
    }
    return response;
}


bool Connection::checkUser(String cardId) 
{   
    bool response = false;
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("cardId=" + cardId, "GET", "iot/isUser", jwt);
        String res = readResponse();
        if (res == "true") {
            this->lastConnectionUpdate = millis();
            Serial.println(cardId + " is already registered.");
            response = true;
        } else {
            this->lastConnectionUpdate = millis();
            Serial.println(cardId + " is not registered.");
        }
    }
    return response;
}


void Connection::connectToWiFi(bool firstConnection, uint16_t conInterval) {
    // Mit WLAN verbinden
    int startOfNewConnection = millis();
    WiFi.begin(wifiSsid.c_str(), wifiPassword.c_str());
    Serial.println("Verbinde mit " + wifiSsid);

    //Ausgabe auf dem seriellen Monitor und Display
    
    Serial.println();
    Serial.println("Warte auf WLAN...");

    if (firstConnection == false) {
        conInterval = connectionInterval;
    } else {
        firstConnection = false;
        //displayTwoLinesSmall("WLAN gefunden",wifiSsid.c_str());
        delay(2500);
    }

    //Max. 5 Sekunden lang wird versucht eine neue WLAN-Verbindung aufzubauen
    while (WiFi.status() != WL_CONNECTED && millis() < startOfNewConnection + conInterval) {
        delay(500);
        if (DEBUG_CONNECTION) {
            Serial.print(".");
        }
    }
}


String Connection::createNewUser(String cardId) 
{
    String response = "";
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("{\"cardId\":\"" + cardId + "\"}", "POST", "iot/createUser", jwt);
        String res = readResponse();
        if ((res == "Created")) {
            this->lastConnectionUpdate = millis();
            Serial.println("New User with CardID " + cardId + " created.");
            response = cardId;
        } else if (res == "Unauthorized") {
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: " + res);
        }
    }
    return response;
}


String Connection::createNewItem(String tagId) 
{
    String response = "";
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("{\"tagId\":\"" + tagId + "\"}", "POST", "iot/createItem", jwt);
        String res = readResponse();
        if ((res == "Created")) {
            this->lastConnectionUpdate = millis();
            Serial.println("New Item with TagID " + tagId + " created.");
            response = tagId;
        } else if (res == "Unauthorized") {
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: " + res);
        }
    }
    return response;
}


String Connection::extendRent(String tagId) 
{
    String response = "";
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("{\"tagId\":\"" + tagId + "\"}", "POST", "iot/extendRent", jwt);
        String res = readResponse();
        if ((res == "Bad Request")) {
            Serial.println("Error: " + res);
        } else if (res == "Unauthorized") {
            Serial.println("Error: Unauthorized!");
        } else {
            this->lastConnectionUpdate = millis();
            Serial.println("Rent of " + tagId + " successfully extended.");
            uint8_t commaIndex = res.indexOf(',');
            response = res.substring(0, commaIndex);
        }
    }
    return response;
}


StaticJsonDocument<1024> Connection::getItem(String tagId) 
{
    StaticJsonDocument<512> response;
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("tagId=" + tagId, "GET", "iot/getItem", jwt);
        String res = readResponse();
        if (!(res == "") && (res != "Unauthorized")) {
            this->lastConnectionUpdate = millis();
            deserializeJson(response, res);
        } else if (res == "Unauthorized") {
            this->authData.clear();
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: Unauthorized!");
        }
    }
    return response;
}


String Connection::getLocalIP() 
{
    return this->localIP;
}


bool Connection::getLoggedIn() 
{
    return this->isLoggedIn;
}


StaticJsonDocument<512> Connection::getExpiringRent(String tagId)
{
    StaticJsonDocument<512> response;
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("tagId=" + tagId, "GET", "iot/getExpiringRent", jwt);
        String res = readResponse();
        if (!(res == "") && (res != "Unauthorized")) {
            this->lastConnectionUpdate = millis();
            deserializeJson(response, res);
        } else if (res == "Unauthorized") {
            Serial.println("Error: Unauthorized!");
        } else {
            Serial.println("Error: " + res);
        }
    }
    return response;
}


//Gibt die Stärke des WiFi-Signals an, points entspricht der Anzahl der Messungen, aus denen der Mittelwert gebildet wird
int Connection::getStrength(uint8_t points) 
{
    uint8_t sc = points;
    long rssi = 0;

    while (sc--) {
        rssi += WiFi.RSSI();
    }
    return points ? static_cast<int>(rssi / points) : 0;
}


StaticJsonDocument<1024> Connection::getUserData() 
{
    return this->authData;
}


void Connection::loginUser(String cardId) 
{
    if (httpClient.connect(server, port)) {
        StaticJsonDocument<256> loginData;
        String loginJson = "";
        loginData["cardId"] = cardId;
        serializeJson(loginData, loginJson);
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest(loginJson, "POST", "iot/loginUser", jwt);
        String res = readResponse();
        if (!(res == "") && (res != "Unauthorized")) {
            this->lastConnectionUpdate = millis();
            deserializeJson(this->authData, res);
            this->isLoggedIn = true;
            String userString = "";
            serializeJson(this->authData["user"], userString);
            Serial.println("User logged in: " + userString);
        } else {
            Serial.println("Error: Unauthorized!");
        }
    }
}


void Connection::logout() 
{
    this->authData.clear();
    this->authenticateDevice();
    this->isLoggedIn = false;
}


void Connection::newToken() 
{
    String cardId = this->getUserData()["user"]["cardId"];
    this->authenticateDevice();
    this->loginUser(cardId);
    Serial.println("Got a new token! Hurra!");
}


//Stellt eine neue WiFi-Verbindung her
String Connection::newWiFiConnection() 
{
    String result = "";
    bool firstConnection = true;
    uint16_t conInterval = 1000;

    while (WiFi.status() != WL_CONNECTED) {

        this->connectToWiFi(firstConnection, conInterval);

        //Falls WiFi immer noch nicht verbunden ist: Ausgabe auf seriellem Monitor und neuer Versuch
        if (WiFi.status() != WL_CONNECTED) {
            Serial.println("WLAN seit mehr als " + String((++connectingCounter) * connectionInterval / 1000) + " Sekunden nicht erreichbar.");
            Serial.println("Starte neuen Versuch (" + String(connectingCounter) + ").");
            WiFi.disconnect();
            
        //Anderenfalls: Ausgabe, dass WiFi wieder verbunden ist.
        } else {
            IPAddress ip = WiFi.localIP();
            char * localIP = new char[16]();
            sprintf(localIP, "IP: %d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
            connectingCounter = 0;
            this->printConnectionData();
            result = String(localIP);
        }
    }
    return result;
}


void Connection::printConnectionData() 
{
    Serial.println("Mit dem WLAN verbunden: " + wifiSsid);
    Serial.print("IP Adresse: ");
    Serial.println(WiFi.localIP());
    Serial.print("MAC-Adresse: ");
    Serial.println(WiFi.macAddress());
    Serial.println("-----------------------------------------------");
}


//Antwort des Servers auslesen
String Connection::readResponse()
{
    //Response einlesen
    String response = "";
    if (httpClient.connected()) {
        while (httpClient.connected()) {
            response = httpClient.readStringUntil('\n');
            if (response == "\r") {
                break;
            }
            delay(10);
        }
        response = httpClient.readStringUntil('\n');
    } else {
        Serial.println("Nicht verbunden!!!");
        delay(1000);
    }

    if(DEBUG_CONNECTION) {
        Serial.println("Response: " + response);
    }
    if(DEBUG_CONNECTION) {
        Serial.println("Verbindung schließen.");
    }

    //Verbindung beenden
    httpClient.stop();
    delay(100);
    return response;
}


String Connection::rentItem(String tagId) 
{
    String response = "";
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("{\"tagId\":\"" + tagId + "\"}", "POST", "iot/rentItem", jwt);
        String res = readResponse();
        if ((res == "Bad Request")) {
            Serial.println("Error: " + res);
        } else if (res == "Unauthorized") {
            this->authData.clear();
            Serial.println("Error: Unauthorized!");
        } else {
            this->lastConnectionUpdate = millis();
            Serial.println("Item " + tagId + " successfully rent.");
            uint8_t commaIndex = res.indexOf(',');
            response = res.substring(0, commaIndex);
        }
    }
    return response;
}


String Connection::returnItem(String tagId) 
{
    String response = "";
    if (httpClient.connect(server, port)) {
        String jwt = (this->authData)["accessToken"];
        sendHttpRequest("{\"tagId\":\"" + tagId + "\"}", "POST", "iot/returnItem", jwt);
        String res = readResponse();
        if ((res == "OK")) {
            this->lastConnectionUpdate = millis();
            Serial.println("Item " + tagId + " successfully returned.");
            response = tagId;
        } else if (res == "Unauthorized") {
            this->authData.clear();
            Serial.println("Error: Unauthorized!");
        } else if (res == "Wrong user") {
            this->lastConnectionUpdate = millis();
            Serial.println("Error: Wrong user");
            response = res;
        } else {
            Serial.println("Error: " + res);
        }
    }
    return response;
}

//Request mit Inhalt content an Server target schicken
void Connection::sendHttpRequest(String content, String method, String target, String jwt) 
{
    // HTTP Request zusammensetzen
    String httpContent = method + " /" + target; 
    if (method == "GET") httpContent += "?" + content;
    httpContent += " HTTP/1.1";
    httpContent += "\nHost:" + String(server) + ":" + String(port);
    if(jwt != "") httpContent += "\nAuthorization: Bearer " + jwt;
    if (method == "POST") {
        httpContent += "\nContent-Length: " + String(content.length());
        httpContent += "\nContent-Type: application/json";
        httpContent += "\n";
        httpContent += "\n" + content;
    } else if (method == "GET") httpContent += "\r\n";


    // Request absenden
    httpClient.println(httpContent);

    if (DEBUG_CONNECTION) {
        Serial.println("-----------------------------------------------");
        Serial.println("sendRequest() läuft.");
        Serial.println(httpContent);
        Serial.println("-----------------------------------------------");
    }
}
