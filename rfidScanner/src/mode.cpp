#include "mode.h"

Mode::Mode(Modes mode, bool firstTime)
{
    this->mode = mode;
    this->firstTime = firstTime;
}

void Mode::set(Modes mode, bool firstTime) 
{
    this->mode = mode;
    this->firstTime = firstTime;
}