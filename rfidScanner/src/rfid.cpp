#include "rfid.h"

const bool DEBUG_RFID = false;

#define SS_PIN 4
#define RST_PIN 5 

MFRC522 mfrc(SS_PIN, RST_PIN); // Initialisierung des MFRC522-Readers

RFID::RFID() 
{
    this->currentTag = "";
    this->newTagAvailable = false;
    this->oldTag = "";
    this->beeper = new Beeper();

    SPI.begin(); // Initialisierung des SPI-Busses
    mfrc.PCD_Init(); // Starten des MFRC522-Readers

    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    Serial.println("Scannen von MIFARE Classsic NUID-Tags.");
    Serial.print("Benutze den folgenden Key: ");
    printHex(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
}


String RFID::getCurrentTag()
{
    String returnTag = "";
    this->readTags();
    delay(5);
    if (newTagAvailable) {
        returnTag = this->currentTag;
    } 
    if (DEBUG_RFID) {
        Serial.println("Ausgegebenes Tag: " + returnTag);
    }
    return returnTag;
}


String RFID::getLastTag()
{
    return this->oldTag;
}


void RFID::readTags() 
{
    if (DEBUG_RFID) {
        Serial.println("readTags() running on core " + String(xPortGetCoreID()));
    }

    this->newTagAvailable = false;
    
    // Gibt leeres Ergebnis zurück, wenn kein neues Tag vorhanden ist.
    if (!mfrc.PICC_IsNewCardPresent()) {
        if (DEBUG_RFID) Serial.println("PICC_IsNewCardPresent() returns false!");
        return;
    }

    // Stellt sicher, dass die NUID gelesen wurde.
    if ( !mfrc.PICC_ReadCardSerial()){
        if (DEBUG_RFID) Serial.println("PICC_ReadCardSerial() returns false!");
        return;
    }

    MFRC522::PICC_Type piccType = mfrc.PICC_GetType(mfrc.uid.sak);

    // Testet, ob es die richtige "Kartenart ist."
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
        piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
        piccType != MFRC522::PICC_TYPE_MIFARE_4K &&
        piccType != MFRC522::PICC_TYPE_ISO_14443_4) {
        Serial.println(F("Dieses Tag ist kein MIFARE Classic Tag."));
        this->beeper->beep(250, 1);
        this->currentTag = mfrc.PICC_GetTypeName(piccType);
        this->newTagAvailable = true;
        for (byte i = 0; i < 4; i++) {
            this->nuidPICC[i] = 0;
        }
        return;
    }

    if (mfrc.uid.uidByte[0] != nuidPICC[0] || 
        mfrc.uid.uidByte[1] != nuidPICC[1] || 
        mfrc.uid.uidByte[2] != nuidPICC[2] || 
        mfrc.uid.uidByte[3] != nuidPICC[3] ) {
        this->newTagAvailable = true;
        if (DEBUG_RFID) {
            Serial.println(F("\n----------------------------------------------------------\n"));
            Serial.println(F("Neues Tag erkannt: "));
            Serial.print(F("PICC type: "));
            Serial.println(mfrc.PICC_GetTypeName(piccType));
        }
        this->beeper->beep(250, 1);

        // NUID in nuidPICC-Array speichern
        for (byte i = 0; i < 4; i++) {
            this->nuidPICC[i] = mfrc.uid.uidByte[i];
        }

        this->currentTag = idToString(mfrc.uid.uidByte, mfrc.uid.size);
        if (DEBUG_RFID) {
            Serial.println(F("Das NUID-Tag ist: "));
            Serial.print(F("In HEX: "));
            printHex(mfrc.uid.uidByte, mfrc.uid.size);
            Serial.println();
            Serial.print(F("In DEC: "));
            printDec(mfrc.uid.uidByte, mfrc.uid.size);
            Serial.println(F("\n----------------------------------------------------------\n"));
        }
        
    } else if (DEBUG_RFID) { 
        Serial.print(F("Karte bereits gelesen."));
        Serial.println(F("\n----------------------------------------------------------\n"));
    }
}


//Speichern eines Byte-Arrays als Hex-Zahlen
void RFID::printHex(byte *buffer, byte bufferSize) 
{
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}


//Speichern eines Byte-Arrays als Dezimal-Zahlen
void RFID::printDec(byte *buffer, byte bufferSize) 
{
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], DEC);
    }
}


//Speichern des Tags aus dem Array in einen String
String RFID::idToString(byte *buffer, byte bufferSize) 
{
    String newTag = "";
    for (byte i = 0; i < bufferSize; i++) {
        newTag += String(buffer[i], HEX);
    }

    return newTag;
}


void RFID::reset() {
    Serial.println("Reset reader");
    for (int i = 0; i < 4; i++) {
        this->nuidPICC[i] = 0;
    }
    this->currentTag = "";
}
