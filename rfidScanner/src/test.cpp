#include "test.h"

void Test::testSetup()
{
    Serial.begin(115200);
    Serial.println("Läuft");

    Connection *con = new Connection();
    Beeper *beeper = new Beeper();
    rfid = new RFID();
    blueLed = new LED(26);
    this->leds[2] = blueLed;
    greenLed = new LED(33);
    this->leds[0] = greenLed;
    redLed = new LED(25);
    this->leds[1] = redLed;
    this->numberOfLeds = 2;
    redButton = new Button(32);
    greenButton = new Button(35);
    blueButton = new Button(34);
    display = new Display();

    con->authenticateDevice();
    con->loginUser("A3618b1a");
    con->rentItem("A1B2C3D4");
    delay(1000);
    con->extendRent("A1B2C3D4");
    delay(1000);
    con->returnItem("A1B2C3D4");
    delay(1000);
    con->newToken();
    delay(1000);
    con->getItem("12345678");
    delay(1000);
    con->createNewUser("AB12C3D4");
    delay(1000);
    con->createNewItem("ABC123D4");
    delay(1000);
    con->logout();

    Serial.println("Bitte grünen Button drücken!");
    greenLed->switchOn(0);
    while (!greenButton->checkButton()) {
        delay(20);
    }
    beeper->beep(250, 1);
    greenLed->switchOff();

    Serial.println("Bitte roten Button drücken!");
    redLed->switchOn(0);
    while (!redButton->checkButton()) {
        delay(20);
    }
    beeper->beep(250, 1);
    redLed->switchOff();

    Serial.println("Bitte blauen Button drücken!");
    blueLed->switchOn(0);
    while (!blueButton->checkButton()) {
        delay(20);
    }
    beeper->beep(250, 1);
    blueLed->switchOff();
}

void Test::testLoop() 
{
    checkOutputs();
    String tag = rfid->getCurrentTag();
    if (tag!="") Serial.println("Neue Karte: " + tag);
    if (this->currentTag != tag) {
        this->display->setText("Neues Tag:", tag, "", "", 3000, false);
        this->greenLed->switchOn(1500);
    }
    delay(10);
}


void Test::checkOutputs() 
{
    for (int i = 0; i < this->numberOfLeds; i++) {
        leds[i]->check();
    }
    this->display->check();
}