#include "led.h"


LED::LED(uint8_t pin)
{
    pinMode(pin, OUTPUT);
    this->PIN = pin;
    this->on = false;
    this->switchOffTime = 0;
}


void LED::check() {
    if (this->on && this->switchOffTime < millis() && this->switchOffTime != 0) {
        this->switchOff();
    }
}


bool LED::getStatus()
{
    return this->on;
}


uint32_t LED::getSwitchOffTime() 
{
    return this->switchOffTime;
}


void LED::switchOff()
{
    digitalWrite(this->PIN, LOW);
    this->on = false;
    this->switchOffTime = millis();
}


void LED::switchOn()
{
    digitalWrite(this->PIN, HIGH);
    this->on = true;
}


void LED::switchOn(uint16_t durance) 
{
    this->switchOn();
    this->switchOffTime = millis() + durance;
    if (durance == 0) this->switchOffTime = 0;
}
