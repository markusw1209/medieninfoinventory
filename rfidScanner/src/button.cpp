#include "button.h"


Button::Button(uint8_t Pin) 
{
    pinMode(Pin, INPUT);
    this->PIN = Pin;
}


bool Button::checkButton()
{
    return digitalRead(this->PIN);
}
