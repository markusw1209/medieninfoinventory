#include "main.h"

void setup() {
    Serial.begin(115200);
    Serial.println("Setup gestartet!");
    connectionCheckInterval = 30000;
    createObjects();
    while (!connectionOK) {
        con->authenticateDevice();
        checkConnection();
    }
    lastConnectionCheck = millis();
    beeper->beep(50, 4);
    // Informationen aus Start-Mode nur für Devs interessant
    // mode = new Mode(start, true);
    mode = new Mode(login, true);
}


void loop() {

    // Prüfe nach einer voreingestellten Zeitintervall Verbindung zum Server.
    if (lastConnectionCheck + connectionCheckInterval < millis() && mode->mode != noConnection) {
        checkConnection();
    }
    // Prüfe in jedem Durchlauf, ob alle Outputs auf dem aktuellen Stand sind
    // (3x LEDs und Display) -> Sonst: Update ausführen.
    checkOutputs();

    switch (mode->mode) {
        case adminMenu:
            adminMenuMode();
            break;
        case adminWelcome:
            adminWelcomeMode();
            break;
        case chooseRentOrExtend:
            chooseRentOrExtendMode();
            break;
        case confirmNewItem:
            confirmNewItemMode();
            break;
        case confirmNewUser:
            confirmNewUserMode();
            break;
        case confirmRent:
            confirmRentMode();
            break;
        case confirmReturn:
            confirmReturnMode();
            break;
        case extendRent:
            extendRentMode();
            break;
        case login:
            loginMode();
            break;
        case newItem:
            newItemMode();
            break;
        case newUser:
            newUserMode();
            break;
        case noConnection:
            noConnectionMode();
            break;
        case rent:
            rentMode();
            break;
        case returning:
            returnMode();
            break;
        case start:
            startMode();
            break;
        case userWelcome:
            userWelcomeMode();
            break;
        case userMenu:
            userMenuMode();
            break;
    }
}


void adminMenuMode() 
{
    if (mode->firstTime) {
        Serial.println("adminMenuMode()");
        delay(3000);
        display->setText("Auswahl:", "Neuer User = rot", "Neues Item = grün", 0, true);
        mode->set(adminMenu, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        mode->set(newUser, true);
        display->setText("Neuer User", 0, false);
    } else if (buttonPressed == "green") {
        mode->set(newItem, true);
        display->setText("Neues Item", 0, false);
    }
}


void adminWelcomeMode()
{
    if (mode->firstTime) {
        Serial.println("adminWelcomeMode()");
        delay(1500);
        display->setBigText("Admin", 0);
        mode->set(adminWelcome, false);
    } else {
        delay(1500);
        display->setBigText("Menü", 0);
        mode->set(adminMenu, true);
    }
}

String checkButtons() {
    bool redPressed = redButton->checkButton();
    bool greenPressed = greenButton->checkButton();
    if (redPressed || greenPressed) beeper->beep(200, 1);
    if (redPressed) return "red";
    if (greenPressed) return "green";
    return ""; 
}


void checkConnection() 
{
    connectionOK = con->checkConnection();
    lastConnectionCheck = millis();

    if (connectionOK) {
        greenLed->switchOn(0);
    } else{
        mode->set(noConnection, true);
    }
}


void checkOutputs() 
{
    for (int i = 0; i < numberOfLeds; i++) {
        leds[i]->check();
    }
    display->check();
}


void chooseRentOrExtendMode()
{
    if (mode->firstTime) {
        Serial.println("chooseRentOrExtendMode()");
        delay(2500);
        display->setText("Auswahl", "Neues leihen = rot", "Verlängern = grün", 0, true);
        mode->set(chooseRentOrExtend, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        display->setText("Leihe verlängern?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(extendRent, false);
    } else if (buttonPressed == "red") {
        display->setText("Artikel leihen?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(confirmRent, false);
    }
}


void chooseRentVersion(StaticJsonDocument<1024> item)
{
    blueLed->switchOn(0);
    StaticJsonDocument<512> rent = con->getExpiringRent(currentTag);
    blueLed->switchOff();
    bool rentExits = !rent.isNull();
    bool isRent = item["rent"];
    String rentUserId = rent["userId"];
    String currentUserId = con->getUserData()["user"]["id"];
    String extensionString = rent["extensions"];
    uint8_t extensions = extensionString.toInt();
    if (isRent && rentUserId != currentUserId) {
        display->setText("Artikel bereits", "verliehen!", 3000, true);
        redLed->switchOn(2000);
        beeper->beep(50, 4);
        rfid->reset();
        mode->set(userMenu, true);
    } else if (rentExits && rentUserId == currentUserId) {
        if (extensions >= 4) {
            display->setText("Maximale Anzahl", "Verlängerungen", "erreicht!", 3000, true);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            rfid->reset();
            mode->set(userMenu, true);
        } else {
            String itemName = item["name"];
            display->printLongString("Artikel:            " + itemName);
            if (item["numberExisting"] > 0) {
                mode->set(chooseRentOrExtend, true);
            } else {
                mode->set(extendRent, true);
            }
        }
    } else if (!rentExits || (rentExits && rentUserId != currentUserId)) {
        String itemName = item["name"];
        display->printLongString("Artikel:            " + itemName);
        mode->set(confirmRent, true);
    } 
}


void confirmNewItemMode()
{
    if (mode->firstTime) {
        Serial.println("confirmNewItemMode()");
        delay(4000);
        display->setText("Item erstellen?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(confirmNewItem, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        blueLed->switchOn(0);
        String ItemOK = con->createNewItem(currentTag);
        blueLed->switchOff();
        if (ItemOK != "") display->setText("Item erstellt!", 2500, false);
        else {
            display->setText("Fehler", 2500, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
        }
        rfid->reset();
        mode->set(adminMenu, true);
    } else if (buttonPressed == "red") {
        display->setText("Abbruch", 0, false);
        rfid->reset();
        mode->set(adminMenu, true);
    }
}


void confirmNewUserMode()
{
    if (mode->firstTime) {
        Serial.println("confirmNewUserMode()");
        delay(4000);
        display->setText("User erstellen?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(confirmNewUser, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        blueLed->switchOn(0);
        String userOK = con->createNewUser(currentTag);
        blueLed->switchOff();
        if (userOK != "") display->setText("User erstellt!", 2500, false);
        else {
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            display->setText("Fehler", 2500, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
        }
        rfid->reset();
        mode->set(adminMenu, true);
    } else if (buttonPressed == "red") {
        display->setText("Abbruch", 0, false);
        rfid->reset();
        mode->set(adminMenu, true);
    }
}


void confirmRentMode()
{
    if (mode->firstTime) {
        Serial.println("confirmRentMode()");
        delay(2500);
        display->setText("Artikel leihen?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(confirmRent, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        blueLed->switchOn(0);
        String rentOK = con->rentItem(currentTag);
        blueLed->switchOff();
        if (rentOK != "") display->setText("Artikel", "ausgeliehen bis", rentOK, 4000, false);
        else {
            display->setText("Fehler", 2500, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
        }
        rfid->reset();
        mode->set(userMenu, true);
    } else if (buttonPressed == "red") {
        display->setText("Abbruch", 0, false);
        rfid->reset();
        mode->set(userMenu, true);
    }
}


void confirmReturnMode()
{
    if (mode->firstTime) {
        Serial.println("confirmReturnMode()");
        delay(2500);
        display->setText("Artikel zurück?","OK = grün", "Abbruch = rot", 0, false);
        mode->set(confirmReturn, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        blueLed->switchOn(0);
        String returnOK = con->returnItem(currentTag);
        blueLed->switchOff();
        if (returnOK == "Wrong user") {
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            display->setText("Falscher", "Nutzer", 2500, false);
        } else if (returnOK != "") {
            display->setText("Artikel", "ist zurück!", 2500, false);
        } else {
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            display->setText("Fehler", 2500, false);
        }
        rfid->reset();
        mode->set(userMenu, true);
    } else if (buttonPressed == "red") {
        display->setText("Abbruch", 0, false);
        rfid->reset();
        mode->set(userMenu, true);
    }
}


void createObjects()
{
    greenLed = new LED(33);
    redLed = new LED(25);
    blueLed = new LED(26);
    display = new Display();
    display->setBigText("Hallo", 0);
    beeper = new Beeper();
    leds[0] = blueLed;
    leds[1] = greenLed;
    leds[2] = redLed;
    numberOfLeds = 3;
    redButton = new Button(32);
    greenButton = new Button(35);
    rfid = new RFID();
    con = new Connection();
}


void extendRentMode()
{
    if (mode->firstTime) {
        Serial.println("extendRentMode()");
        delay(2500);
        display->setText("Leihe verlängern?", "OK = grün", "Abbruch = rot", 0, false);
        mode->set(extendRent, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "green") {
        blueLed->switchOn(0);
        String extendOK = con->extendRent(currentTag);
        blueLed->switchOff();
        if (extendOK != "") display->setText("Artikel", "verlängert bis", extendOK, 3000, false);
        else {
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            display->setText("Fehler", 2500, false);
        }
        rfid->reset();
        mode->set(userMenu, true);
    } else if (buttonPressed == "red") {
        display->setText("Abbruch", 0, false);
        rfid->reset();
        mode->set(userMenu, true);
    }
}


void loginMode() 
{
    if(mode->firstTime) {
        Serial.println("loginMode()");
        display->setText("Bitte mit", "Nutzerkarte", "einloggen", 0, false);
        mode->set(login, false);
    }
    String newTag = rfid->getCurrentTag();
    if (newTag != "") {
        currentTag = newTag;
        Serial.println("Neue Karte: " + newTag);
        blueLed->switchOn(0);
        con->loginUser(newTag);
        blueLed->switchOff();
        user = con->getUserData()["user"];
        if (user.isNull()) {
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            display->setText("User", "unbekannt", 2500, false);
            rfid->reset();
            mode->set(login, true);
        } else {
            display->setText("Hallo", user["firstName"], 2000, false);
            bool admin = user["admin"];
            if (admin) mode->set(adminWelcome, true);
            else mode->set(userWelcome, true);
        }
    }
}


void newItemMode()
{
    if (mode->firstTime) {
        Serial.println("newItemMode()");
        delay(1500);
        display->setText("Neues Item:", "Tag scannen", "Abbruch = rot", 0, false);
        mode->set(newItem, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        mode->set(adminMenu, true);
        rfid->reset();
        display->setBigText("Abbruch", 2500);
    }
    String newTag = rfid->getCurrentTag();
    if (newTag != "") {
        currentTag = newTag;
        Serial.println("Neues Tag: " + newTag);
        blueLed->switchOn(0);
        StaticJsonDocument<1024> itemExists = con->getItem(newTag);
        blueLed->switchOff();
        if (!itemExists.isNull()) {
            display->setText("Fehler", "Tag bereits", "registriert.", 2500, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            rfid->reset();
            mode->set(adminMenu, true);
        } else {
            display->setText("Neues Tag:", newTag, 2500, false);
            mode->set(confirmNewItem, true);
        }
    }
}


void newUserMode()
{
    if (mode->firstTime) {
        Serial.println("newUserMode()");
        delay(1500);
        display->setText("Neuer User:", "Karte scannen", "Abbruch = rot", 0, false);
        mode->set(newUser, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        display->setBigText("Abbruch", 2500);
        rfid->reset();
        mode->set(adminMenu, true);
    }
    String newTag = rfid->getCurrentTag();
    if (newTag != "") {
        currentTag = newTag;
        Serial.println("Neue Karte: " + newTag);
        blueLed->switchOn(0);
        bool userExists = con->checkUser(newTag);
        blueLed->switchOff();
        if (userExists) {
            display->setText("Fehler", "Karte bereits", "registriert.", 2500, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            rfid->reset();
            mode->set(adminMenu, true);
        } else {
            display->setText("Neue Karte:", newTag, 2500, false);
            mode->set(confirmNewUser, true);
        }
    }
}


void noConnectionMode()
{
    if(mode->firstTime) {
        Serial.println("noConnectionMode()");
        redLed->switchOn(2000);
        beeper->beep(50, 4);
        display->setText("Keine", "Verbinung", "zum Server!", 0, false);
        redLed->switchOn(0);
        greenLed->switchOff();
        mode->set(noConnection, false);
        lastConnectionCheck = millis();
    } else if (lastConnectionCheck + connectionCheckInterval < millis()) {
        Serial.println("Hole neues Token");
        blueLed->switchOn(0);
        con->newToken();
        blueLed->switchOff();
        lastConnectionCheck = millis();
    } else if (con->checkConnection()) {
        Serial.println("Connection wieder OK!");
        connectionOK = true;
        redLed->switchOff();
        greenLed->switchOn(0);
        mode->set(login, true);
    } else {
        Serial.println("Kein Fall eingetreten -> else");
        delay(50);
    }
}


void rentMode()
{
    if (mode->firstTime) {
        Serial.println("rentMode()");
        delay(1500);
        display->setText("Leihe:", "Artikel scannen", "Abbruch = rot", 0, false);
        mode->set(rent, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        mode->set(userMenu, true);
        rfid->reset();
        display->setBigText("Abbruch", 2500);
    }
    String newTag = rfid->getCurrentTag();
    if (newTag != "") {
        currentTag = newTag;
        Serial.println("Neues Tag: " + newTag);
        blueLed->switchOn(0);
        StaticJsonDocument<1024> item = con->getItem(newTag);
        blueLed->switchOff();
        if (item.isNull()) {
            display->setText("Fehler!", "Artikel", "unbekannt", 3000, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            rfid->reset();
            mode->set(userMenu, true);
        } else {
            // Wenn das Item im System geblockt ist, kann es nicht ausgeliehen werden.
            bool isBlocked = item["blocked"];
            bool isActive = item["active"];
            if (!isActive) {
                display->setText("Entschuldigung", "Dieser Artikel ist", "nicht aktiviert!", 3000, true);
                redLed->switchOn(2000);
                beeper->beep(50, 4);
                rfid->reset();
                mode->set(userMenu, true);
            } else if (isBlocked) {
                display->setText("Entschuldigung", "Dieser Artikel", "ist blockiert!", 3000, false);
                redLed->switchOn(2000);
                beeper->beep(50, 4);
                rfid->reset();
                mode->set(userMenu, true);
            } else {
                chooseRentVersion(item);
            }
        }
    }
}


void returnMode()
{
    if (mode->firstTime) {
        Serial.println("returnMode()");
        delay(1500);
        display->setText("Rückgabe:", "Artikel scannen", "Abbruch = rot", 0, false);
        mode->set(returning, false);
    } 
    //Abbruchmöglichkeit -> Rückkehr zum Menu
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        mode->set(userMenu, true);
        rfid->reset();
        display->setBigText("Abbruch", 2500);
    }
    String newTag = rfid->getCurrentTag();
    if (newTag != "") {
        currentTag = newTag;
        Serial.println("Neues Tag: " + newTag);
        blueLed->switchOn(0);
        StaticJsonDocument<1024> item = con->getItem(newTag);
        blueLed->switchOff();
        Serial.print("Item: ");
        printJsonDocs(item);
        if (item.isNull()) {
            display->setText("Fehler!", "Artikel", "unbekannt", 3000, false);
            redLed->switchOn(2000);
            beeper->beep(50, 4);
            rfid->reset();
            mode->set(userMenu, true);
        } else {
            blueLed->switchOn(0);
            StaticJsonDocument<512> rent = con->getExpiringRent(currentTag);
            printJsonDocs(rent);
            blueLed->switchOff();
            bool isRent = !rent.isNull();
            if (isRent) {
                String itemName = item["name"];
                display->printLongString("Artikel:            " + itemName);
                mode->set(confirmReturn, true);
            } else {
                display->setText("Dieser Artikel", "ist nicht", "ausgeliehen", 2500, true);
                redLed->switchOn(2000);
                beeper->beep(50, 4);
                rfid->reset();
                mode->set(userMenu, true);
            }
        }
    }
}


void startMode() 
{
    display->setText("Verbunden", con->getLocalIP(), WiFi.macAddress(), 0, true);
    mode->set(login, true);
}


void userMenuMode() 
{
    if (mode->firstTime) {
        Serial.println("userMenuMode()");
        delay(3000);
        display->setText("Auswahl:", "Leihe = rot", "Rückgabe = grün", 0, true);
        mode->set(userMenu, false);
    }
    String buttonPressed = checkButtons();
    if (buttonPressed == "red") {
        mode->set(rent, true);
        display->setBigText("Leihe", 0);
    } else if (buttonPressed == "green") {
        mode->set(returning, true);
        display->setText("Rückgabe", 0, false);
    }
}


void userWelcomeMode()
{
    if (mode->firstTime) {
        Serial.println("userWelcomeMode()");
        delay(1500);
        display->setBigText("User", 0);
        mode->set(userWelcome, false);
    } else {
        delay(1500);
        display->setBigText("Menü", 0);
        mode->set(userMenu, true);
    }
}
