#include "display.h"

bool DEBUG_DISPLAY = false;

U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, U8X8_PIN_NONE);
u8g2_uint_t displayOffset;
u8g2_uint_t displayWidth;


Display::Display()
{
    this->counter = 0;
    this->cleared = true;
    u8g2.begin();
}


void Display::check()
{    
    if (this->clearTime != 0 && this->clearTime < millis()) {
        if (DEBUG_DISPLAY) Serial.println("Zeit abgelaufen");
        this->clearTime = 0;
        this->clear();
    }
}


void Display::printLongString(String s) 
{
    String splits[4] = {"", "", "", ""};
    int index = 0;
    while (s.length() > 0 && index < 4) {
        splits[index++] = s.substring(0, 20);
        s = s.substring(20); 
    }
    this->setText(splits[0], splits[1], splits[2], splits[3], 0, true);
}


void Display::clear() 
{
    if (DEBUG_DISPLAY) {
        Serial.println("Leere Display");
    }
    u8g2.clear();
    this->cleared = true;
}


void Display::setText(String line1, uint16_t time, bool small)
{
    this->setText(line1, "", "", "", time, small);
}


void Display::setText(String line1, String line2, uint16_t time, bool small)
{
    this->setText(line1, line2, "", "", time, small);
}


void Display::setText(String line1, String line2, String line3, uint16_t time, bool small)
{
    this->setText(line1, line2, line3, "", time, small);
}


void Display::setText(String line1, String line2, String line3, String line4, uint16_t time, bool small) {

    //Sucht die letzte nichtleere Zeile und setzt die Anzahl der leeren Zeilen auf den "Rest".
    String lines[] = {line1, line2, line3, line4};
    uint8_t emptyLines = 0;
    for (int i = 3; i >= 0; i--) {
        if (lines[i] != "") {
            emptyLines = 3 - i;
            break;
        }
    }
    if (DEBUG_DISPLAY) Serial.println("setText(). EmptyLines = " + String(emptyLines));

    if (emptyLines == 0) {
        this->displayFourLines(line1.c_str(), line2.c_str(), line3.c_str(), line4.c_str());
    } else if (emptyLines == 1) {
        if (small) this->displayThreeLinesSmall(line1.c_str(), line2.c_str(), line3.c_str());
        else this->displayThreeLines(line1.c_str(), line2.c_str(), line3.c_str());
    } else if (emptyLines == 2) {
        if (small) this->displayTwoLinesSmall(line1.c_str(), line2.c_str());
        else this->displayTwoLines(line1.c_str(), line2.c_str());
    } else if (emptyLines >= 3) {
        this->displayMiddle(line1.c_str());
    }

    this->cleared = false;
    if (time == 0) this->clearTime = 0;
    else this->clearTime = millis() + time;
    
    if (DEBUG_DISPLAY) Serial.println("Set Text: " + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4);
    if (DEBUG_DISPLAY) Serial.println("ClearTime = " + String(time) + ".");
}


void Display::setBigText(String text, uint16_t time) 
{
    if (DEBUG_DISPLAY) Serial.println("Set Big Text: " + text);
    this->displayMax(text.c_str());
    this->cleared = false;
    this->clearTime = millis() + time;
}


void Display::displayMax(const char text[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_logisoso32_tf);
    u8g2.drawUTF8(1, 50, text);
    u8g2.nextPage();
}


void Display::displayMiddle(const char text[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 38, text);
    u8g2.nextPage();
}


void Display::displayTwoLines(const char line1[], const char line2[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 27, line1);
    u8g2.drawUTF8(1, 49, line2);
    u8g2.nextPage();
}


void Display::displayTwoLinesSmall(const char line1[], const char line2[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_t0_11b_tf);
    u8g2.drawUTF8(1, 27, line1);
    u8g2.drawUTF8(1, 49, line2);
    u8g2.nextPage();
}


void Display::displayThreeLines(const char line1[], const char line2[], const char line3[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 19, line1);
    u8g2.drawUTF8(1, 38, line2);
    u8g2.drawUTF8(1, 57, line3);
    u8g2.nextPage();
}


void Display::displayThreeLinesSmall(const char line1[], const char line2[], const char line3[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_t0_11b_tf);
    u8g2.drawUTF8(1, 19, line1);
    u8g2.drawUTF8(1, 38, line2);
    u8g2.drawUTF8(1, 57, line3);
    u8g2.nextPage();
}


void Display::displayFourLines(const char line1[], const char line2[], const char line3[], const char line4[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_t0_11b_tf);
    //u8g2.setFont(u8g2_font_unifont_tf);
    u8g2.drawUTF8(1, 15, line1);
    u8g2.drawUTF8(1, 30, line2);
    u8g2.drawUTF8(1, 45, line3);
    u8g2.drawUTF8(1, 60, line4);
    u8g2.nextPage();
}
