#include "helpfulFunctions.h"

bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2)
{
    bool equal = false;
    
        if (len1 == len2) {
            equal = true;
            for (int i = 0; i < len1; i++) {
                if(!(arr1[i] == arr2[i])) {
                    equal = false;
                }
            }
        }
    return equal;
}


void printJsonDocs(StaticJsonDocument<256> doc)
{
    String printJson = "";
    serializeJson(doc, printJson);
    Serial.println(printJson);
}


void printJsonDocs(StaticJsonDocument<512> doc)
{
    String printJson = "";
    serializeJson(doc, printJson);
    Serial.println(printJson);
}


void printJsonDocs(StaticJsonDocument<1024> doc)
{
    String printJson = "";
    serializeJson(doc, printJson);
    Serial.println(printJson);
}


void printJsonDocs(StaticJsonDocument<2048> doc)
{
    String printJson = "";
    serializeJson(doc, printJson);
    Serial.println(printJson);
}


bool stringToBoolean(String str)
{
    Serial.println(str + " zu bool konvertieren.");
    if (str == "1" || str == "true") return true;
    return false;
}
