#include "beeper.h"

Beeper::Beeper()
{
    pinMode(BEEPER, OUTPUT);
}

void Beeper::beep (uint16_t durance, uint8_t number)
{
    for (int i = 0; i < number; i++) {
        digitalWrite(BEEPER, HIGH);
        delay(durance);
        digitalWrite(BEEPER, LOW);
        if (i != number - 1) {
            delay(durance);
        }
    }
}