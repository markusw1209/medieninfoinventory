#ifndef _MAIN_H_
#define _MAIN_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include "beeper.h"
#include "reader.h"
#include "ui.h"
#include "REST.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"

#define RED_BUTTON 32
#define BLUE_BUTTON 34
#define GREEN_BUTTON 35 

//Zur Erstellung von mehreren Thraeds und "lock" von gemeinsam genutzten Variablen, um "lost updates" etc. zu vermeiden 
SemaphoreHandle_t xMutex;
TaskHandle_t readerTask = NULL;
TaskHandle_t uiTask = NULL;

String res;
bool loggedIn = false;
bool isAdmin = false;
StaticJsonDocument<256> user;

String mode = "";

void setup();
void loop();
void readerLoop(void * parameter);
void uiLoop(void * parameter);
void blinkLED(LED * led, int onTime, int offTime);
void checkLED(LED * led);
void updateLEDs();
void switchAllLEDsOn();
void switchAllLEDsOff();
void updateDisplay();
void clearDisplay();
void setOutputText(String line1, String line2, String line3, String line4, uint16_t time, bool small);
void setBigText(String text, uint16_t time);
void switchOn(LED *led, uint16_t time);
void login();
void adminMode();
void userMode();
bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2);

#endif