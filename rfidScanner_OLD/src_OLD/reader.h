#ifndef _READER_H_
#define _READER_H_

#include <SPI.h>
#include <MFRC522.h>
#include "beeper.h"
#include "ui.h"

#define SS_PIN 4
#define RST_PIN 5

uint32_t startTimeReader = 0;

void initReader();
void readerLoop(void * parameter);
String getCurrentTag();
void readTags();
void printHex(byte *buffer, byte bufferSize);
void printDec(byte *buffer, byte bufferSize);
String idToString(byte *buffer, byte bufferSize);
bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2);

#endif