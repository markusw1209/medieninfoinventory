#include "REST.h"

WiFiClient client;

char mac[17]; // Mac-Adresse des Gerätes speichern
String wifi_ssid = "";
String wifi_password = "";
String wifi_ssids[] = {"AnjaMarkusJannis", "ProjektarbeitHotspot", "MarkusiPhoneXS", "Dach"};
String wifi_passwords[] = {"H,ibdmn5eegIg.W9", "ProjektarbeitMW9", "H,ibdmn5eegIg.W9", "DWgFuHDu1j5."};
const char * server = "markusw.ddns.net"; // Server IP
//const char * server = "192.168.178.36"; // Während der Entwicklungs: Server auf dem lokalen PC statt im Internet.
const uint16_t port = 443; // Port des Servers

uint16_t msgID = 0; //No der nächsten Message (ID) wird bei jeder Sendung um 1 hochgezählt
uint16_t connectingCounter = 0; //Anzahl der Verbindungsversuche zum WLAN (wird bei Verbindungsabbruch gezählt).
uint16_t connectionInterval = 5000; //Wie lange (ms) soll auf Verbindung mit WLAN gewartet werden, bevor ein neuer Versuch gestartet wird

bool DEBUG_REST = false;


void getMacAddress() 
{
    String macString = WiFi.macAddress();
    for (int i = 0; i < macString.length(); i++) {
        mac[i] = macString.charAt(i);
    }
}





String identifyUser(String content) {

    String resUser = "";

    if (client.connect(server, port)) {
        if (DEBUG_REST) {
            Serial.println("Verbunden? " + String(client.connected()));
        }
        String httpContent = createTagMsgContent(content);
        sendRequest(httpContent, "POST", "newTag");
        String res = readResponse();

        StaticJsonDocument<256> doc;
        deserializeJson(doc, res);
        String resMsgID = doc["msgID"];

        if (resMsgID == String(msgID)) {
            msgID++;
            serializeJson(doc, resUser);
            if (DEBUG_REST && res.length() > 0) {
                String firstName = doc["user"]["first_name"];
                String lastName = doc["user"]["last_name"];
                Serial.println("Erfolgreich zugestellt.");
                Serial.println("Nutzer: " + firstName + " " + lastName);
                Serial.println("-----------------------------------------------");
            }
        }
    }
    return resUser;
}





//Request mit Inhalt content an Server target schicken
void sendRequest(String content, String method, String target) 
{
    // HTTP Request zusammensetzen
    String httpContent = method + " /" + target + " HTTP/1.1";
    httpContent += "\nHost:" + String(server) + ":" + String(port);
    httpContent += "\nContent-Length: " + String(content.length());
    httpContent += "\nContent-Type: text/plain";
    httpContent += "\n";
    httpContent += "\n" + content;

    if (DEBUG_REST) {
        Serial.println("-----------------------------------------------");
        Serial.println("sendRequest() läuft.");
        Serial.println(httpContent);
        Serial.println("-----------------------------------------------");
    }
    // Request absenden
    client.println(httpContent);
}





//aus temp-String den vollständigen Inhalt der Tag-Message im erstellen (mit Header)
String createTagMsgContent(String rfidTags) 
{
    if (DEBUG_REST) {
        Serial.println("createTagMsgContent() läuft.");
    }
    uint32_t intern_time = millis();
    int strength = getStrength(10);
    //String server_pw = generateServerPW(intern_time, strength);
    
    //Content setzt sich zusammen aus der Geräte-ID, der Mac-Adresse, der internen Zeit des Arduinos, der Signalstärke und dem gescannten Tag
    String content = "{\"msgID\":\"" + String(msgID) + "\",\"mac\":\"" + String(mac) + "\",\"internTime\":" + String(intern_time) 
        + ",\"strength\":" + String(strength) + ",\"tags\": [" + rfidTags + "]}";
    
    if (DEBUG_REST) {
        Serial.println("Content: " + content);
    }

    return content;
}





//Stellt eine neue WiFi-Verbindung her
void newWiFiConnection() 
{
    getMacAddress();
    Serial.println("Starte WLAN-Scan");
    displayMiddle("WLAN-Scan...");

    // WiFi.scanNetworks gibt die Anzahl der Netzwerke zurück
    int n = WiFi.scanNetworks();
    Serial.println("Scan abgeschlossen");
    int index[] = {-1, -1, -1, -1};

    if (n == 0) {
        Serial.println("Kein WLAN gefunden");

    } else {
        Serial.print(n);
        Serial.println(" Netzwerk(e) gefunden");
        displayMiddle("WLAN gefunden");
        for (int i = 0; i < n; ++i) {
            //Alle gefundenen Netzwerke mit Signalstärke ausgeben:
            /*Serial.print((i + 1) + ": " + WiFi.SSID(i) + " (" + WiFi.RSSI(i) + ")");
            Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");*/
            //Prüfen, ob die in wifi_ssids angegebenen Netzwerke dabei sind und Indizes speichern
            for (int k = 0; k < sizeof(wifi_ssids); k++) {
                if (WiFi.SSID(i) == wifi_ssids[k]) {
                    Serial.println("Gefunden " + String(k + 1) + ": " + WiFi.SSID(i));
                    index[k] = i;
                }
            }
            delay(10);
        }

        Serial.println("");

        //Passwort und SSID auf das Netzwerk einstellen, das gefunden wurde und die höchste Priorität
        //hat (geringster Index)
        for (int i = sizeof(index)/4; i >= 0; i--) {
            if (index[i] > -1) {
                wifi_ssid = wifi_ssids[i];
                wifi_password = wifi_passwords[i]; 
            } 
        }

        bool firstConnection = true;
        uint16_t conInterval = 1000;

        while (WiFi.status() != WL_CONNECTED) {
    
            // Mit WLAN verbinden
            int startOfNewConnection = millis();
            WiFi.begin(wifi_ssid.c_str(), wifi_password.c_str());
            Serial.println("Verbinde mit " + wifi_ssid);

            //Ausgabe auf dem seriellen Monitor und Display
            
            Serial.println();
            Serial.println("Warte auf WLAN...");
            displayTwoLinesSmall("Verbindungsaufbau", mac);

            if (firstConnection == false) {
                conInterval = connectionInterval;
            }

            //Max. 5 Sekunden lang wird versucht eine neue WLAN-Verbindung aufzubauen
            while (WiFi.status() != WL_CONNECTED && millis() < startOfNewConnection + conInterval) {
                delay(500);
                if (DEBUG_REST) {
                    Serial.print(".");
                }
            }

            //Falls WiFi immer noch nicht verbunden ist: Ausgabe auf seriellem Monitor und neuer Versuch
            if (WiFi.status() != WL_CONNECTED) {
                Serial.println("WLAN seit mehr als " + String((++connectingCounter) * connectionInterval / 1000) + " Sekunden nicht erreichbar.");
                Serial.println("Starte neuen Versuch (" + String(connectingCounter) + ").");
                WiFi.disconnect();
              
            //Anderenfalls: Ausgabe, dass WiFi wieder verbunden ist.
            } else {
                Serial.println("Mit dem WLAN verbunden: " + wifi_ssid);
                connectingCounter = 0;
                Serial.print("IP Adresse: ");
                Serial.println(WiFi.localIP());
                IPAddress ip = WiFi.localIP();
                char * localIP = new char[16]();
                sprintf(localIP, "IP: %d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
                Serial.print("MAC-Adresse: ");
                Serial.println(mac);
                const char* localMAC = (String(mac)).c_str();
                Serial.println("-----------------------------------------------");
                delay(250);
                displayThreeLinesSmall("Verbunden", localIP, localMAC);
            } 

            firstConnection = false;
        }
    }
}





//Gibt die Stärke des WiFi-Signals an, points entspricht der Anzahl der Messungen, aus denen der Mittelwert gebildet wird
int getStrength(uint8_t points) 
{
    uint8_t sc = points;
    long rssi = 0;

    while (sc--) {
        rssi += WiFi.RSSI();
    }
    return points ? static_cast<int>(rssi / points) : 0;
}





//Antwort des Servers auslesen
String readResponse()
{
  //Response einlesen
  String response = "";
  while (client.connected()) {
      response = client.readStringUntil('\n');
      if (response == "\r") {
          break;
      }
  }
  response = client.readStringUntil('\n');

  if(DEBUG_REST) {
      Serial.println("Response: " + response);
  }

  //Wenn die Länge der Antwort > 0 ist -> Konfigurationsinformationen auslesen
  if (response.length() > 0) {
      //readContent(response);
  }

  if(DEBUG_REST) {
      Serial.println("Verbindung schließen.");
  }

  //Verbindung beenden
  client.stop();

  return response;
}
