#include "main.h"

bool DEBUG = false;

void setup()
{
    //Baudrate für seriellen Monitor
    Serial.begin(115200);

    //Pins definieren und einstellen
    pinMode(BEEPER, OUTPUT);
    pinMode(RED_BUTTON, INPUT);
    pinMode(BLUE_BUTTON, INPUT);
    pinMode(GREEN_BUTTON, INPUT);

    initUI(); // Setup für Display und LEDs ausführen 
    initReader(); // Setup für den RFID-Reader ausführen

    // WiFi-Verbindung herstellen und mit grüner LED bestätigen, wenn OK
    newWiFiConnection(GREEN_BUTTON);
    switchAllLEDsOff();
    switchOn(&GREEN_LED, 0);

    // Für Multi-Threading: Zwei Tasks erstellen, die auf jeweils einem Prozessorkern laufen
    Serial.println("CreateTasks()");

    //Erstelle Task für RFID Reader mit hoher Priorität
    xTaskCreatePinnedToCore(
        readerLoop, // Funktion, in dem der Task implementiert wird (analog zu loop())
        "readerTask", // Name des Tasks
        10000,  // Größe des Stacks
        NULL,  // Input-Parameter für den Task
        10,  // Priorität des Tasks
        &readerTask,  // Der Task-Handler
        0 // Prozessorkern, auf dem der Task laufen soll
    ); 

    delay(100);

    //Erstelle Task für Displayausgaben mit geringerer Priorität
    xTaskCreatePinnedToCore(
        uiLoop, // Funktion, in dem der Task implementiert wird (analog zu loop())
        "displayTask", // Name des Tasks
        10000,  // Größe des Stacks
        NULL,  // Input-Parameter für den Task
        1,  // Priorität des Tasks
        &uiTask,  // Der Task-Handler
        1 // Prozessorkern, auf dem der Task laufen soll
    );
    
    delay(100);

    //Ein Beep als Signal für den Start
    beep(500, 1);
    setBigText("Start", 2500);

    Serial.println("Setup abgeschlossen!");
}





void loop()
{
    // Leer, Funktion wird für Multithreading von reader() und display() übernommen.
}





/*Angedachte Funktionen: giveBack(), rent() (Aus loop heraus je machdem, wie die Variablen gesetzt sind).*/





void login() 
{
    setOutputText("Bitte mit", "Karte einloggen!", "", "", 0, false);

    String newTag = getCurrentTag();
    if (newTag != "") {
        Serial.println("--------------------------------------");
        displayTwoLines("Tag erkannt: ", newTag.c_str());
        uint32_t displayStart = millis();

        digitalWrite(BLUE_LED.PIN, HIGH);
        res = identifyUser("{\"id\":\"" + newTag + "\"}");
        digitalWrite(BLUE_LED.PIN, LOW);

        StaticJsonDocument<256> doc;
        deserializeJson(doc, res);
        user = doc["user"];

        if(DEBUG) {
            Serial.println("User ist null? " + user.isNull());
        }

        while (displayStart + 1500 > millis()) {
            delay(50);
        }

        if (!user.isNull()) {
            if (DEBUG){
                Serial.println("Response: " + res);
            }
            displayTwoLines("Übertragung", "erfolgreich!");
            delay(2000);
            String firstName = user["first_name"];
            loggedIn = true;
            isAdmin = user["admin"];
            String isAdminString = "Nutzer";
            if (isAdmin) {
                isAdminString = "Administrator";
            }
            displayThreeLinesSmall(("Hallo " + firstName).c_str(), isAdminString.c_str(), "Mit Grün zum Menü");
            while (digitalRead(GREEN_BUTTON) == LOW) {
                delay(50);
            }
        } else {
            Serial.println("Diese Karte ist unbekannt!");
            setOutputText("Diese Karte", "ist unbekannt", "", "", 2500, false);
        }
    }
}





void adminMode() 
{
    if (mode == "main") {

        Serial.println("Adminmodus aktiviert!");
        displayMax("ADMIN");
        delay(1000);
        mode = "menu";

    } else if (mode = "menu") {

        int redButtonStatus = digitalRead(RED_BUTTON);
        int greenButtonStatus = digitalRead(GREEN_BUTTON);
        int blueButtonStatus = digitalRead(BLUE_BUTTON);

        if (redButtonStatus == HIGH) {
            beep(100, 1);
            Serial.println("Roter Button gedrückt");
            setBigText("Rot", 2500);
            switchOn(&RED_LED, 2500);
        }
        
        if (greenButtonStatus == HIGH) {
            beep(25, 4);
            Serial.println("Grüner Button gedrückt");
            setBigText("Grün", 2500);
        }

        if (blueButtonStatus == HIGH) {
            beep(50, 2);
            Serial.println("Blauer Button gedrückt");
            setBigText("Blau", 2500);
            switchOn(&BLUE_LED, 2500);
        }
    }
    delay(10);
}





void userMode() 
{

    if (mode == "rent") {
         
        String newTag = "";
        newTag = getCurrentTag();
        if (newTag != "") {
            Serial.println("Gelesen: " + newTag);
            displayTwoLines("Tag erkannt: ", newTag.c_str());
            delay(2500);
            mode = "logout";
        }

    } else if (mode == "rentMode") {
        displayMax("Leihe");
        beep(100, 2);
        delay(700);
        mode = "rent"; 
        Serial.println("Ausleihe-Funktion");
        setOutputText("Artikel", "scannen", "", "", 0, false);

    } if (mode == "return") {
        Serial.println("Rückgabe gewählt");
        setOutputText("Artikel", "scannen", "", "", 0, false);
        String newTag = "";
        while (newTag == "") {
            newTag = getCurrentTag();
            delay(10);
        }
        Serial.println("Gelesen: " + newTag);

    } else if (mode == "returnMode") {


    
    } else if (mode == "main") {
        setOutputText("Menu", "", "Leihe = rot", "Rückgabe = grün", 0, true);
        if (digitalRead(RED_BUTTON) == HIGH) {
            Serial.println("Ausleihe gewählt.");
            mode = "rentMode";
        } else if (digitalRead(GREEN_BUTTON) == HIGH) {
            Serial.println("Rückgabe gewählt.");
            mode = "returnMode";
        } 

    } else if (mode == "") {
        Serial.println("Usermodus aktiviert!");
        displayMax("User");
        delay(1000);
        mode = "main";

    } else if (mode == "logout") {
        Serial.println("Logout");
        setBigText("Logout", 2500);
        loggedIn = false;
        isAdmin = false;
        mode = "";
    }
    delay(10);
}





bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2)
{
    bool equal = false;
    
        if (len1 == len2) {
            equal = true;
            for (int i = 0; i < len1; i++) {
                if(arr1[i] != arr2[i]) {
                    equal = false;
                }
            }
        }
    return equal;
}
