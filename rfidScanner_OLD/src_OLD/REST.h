#ifndef _REST_H_
#define _REST_H_

#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "updateProcess.h"

void getMacAddress();
String identifyUser(String content);
void sendRequest(String content, String method, String target);
String createTagMsgContent(String tagsSend);
void newWiFiConnection();
int getStrength(uint8_t points);
String readResponse();

#endif