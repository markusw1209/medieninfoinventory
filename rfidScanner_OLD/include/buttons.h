#ifndef _BUTTONS_H_
#define _BUTTONS_H_

#include <Arduino.h>

void initButtons();
bool * checkButtons();
bool checkButton(String buttonColor);
void updateButtonTimes();
uint32_t * getButtonTimes();
bool buttonPressedIn(String buttonColor, uint32_t timeslot);

#endif
