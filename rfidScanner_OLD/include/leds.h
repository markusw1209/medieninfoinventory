#ifndef _LEDS_H_
#define _LEDS_H_

#include <Arduino.h>
#include "helpfulFunctions.h"

struct LED {
    int PIN;
    bool on; 
    uint16_t onTime;
    uint32_t startOnTime;
    bool blink;
    uint16_t blinkTime;
    uint32_t startBlinkTime;
};

void initLEDs();
void checkLEDs();
void checkLED(LED * led);
void blinkLED(LED * led, int onTime, int offTime);
void switchOn(String ledName, uint16_t time);
void switchOn(LED * led, uint16_t time);
void switchAllLEDsOn();
void switchAllLEDsOff();

#endif