#ifndef _MAINPROCESS_H_
#define _MAINPROCESS_H_

#include <ArduinoJson.h>
#include <Arduino.h>
#include "connection.h"
#include "beeper.h"
#include "updateProcess.h"
#include "rfid.h"
#include "helpfulFunctions.h"
#include "buttons.h"
#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"

void initMainProcess();
void mainLoop(void * parameter);
void login();
void userMode();
void adminMode();

#endif