#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <Arduino.h>
#include <U8g2lib.h>
#include "helpfulFunctions.h"

void initDisplay();
void updateDisplay();
void checkDisplay();
void clearDisplay();
void setOutputText(String line1, String line2, String line3, String line4, uint16_t time, bool small);
void setBigText(String text, uint16_t time);
void displayMax(const char text[]);
void displayMiddle(const char text[]);
void displayTwoLines(const char line1[], const char line2[]);
void displayTwoLinesSmall(const char line1[], const char line2[]);
void displayThreeLines(const char line1[], const char line2[], const char line3[]);
void displayThreeLinesSmall(const char line1[], const char line2[], const char line3[]);
void displayFourLines(const char line1[], const char line2[], const char line3[], const char line4[]);

#endif