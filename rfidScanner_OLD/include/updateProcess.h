#ifndef _UPDATEPROCESS_H_
#define _UPDATEPROCESS_H_

#include "display.h"
#include "leds.h"
#include "beeper.h"
#include "buttons.h"
#include "rfid.h"
#include "soc/timer_group_struct.h"
#include "soc/timer_group_reg.h"
#include "connection.h"

void initUpdates();
void updateLoop(void * parameter);

#endif