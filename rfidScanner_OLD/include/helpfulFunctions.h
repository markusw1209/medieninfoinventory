#ifndef _HELPFULFUNCTIONS_H_
#define _HELPFULFUNCTIONS_H_

#include <Arduino.h>
#include <iostream>
#include <string>
#include <cstring>

bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2);
const char* convertStringToChars(String s);

#endif