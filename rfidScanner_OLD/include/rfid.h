#ifndef _RFID_H_
#define _RFID_H_

#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>
#include "beeper.h"
#include "helpfulFunctions.h"

void initRFID();
String getCurrentTag();
void readTags();
void printHex(byte *buffer, byte bufferSize);
void printDec(byte *buffer, byte bufferSize);
String idToString(byte *buffer, byte bufferSize);

#endif