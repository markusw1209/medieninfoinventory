#ifndef _CONNECTION_H_
#define _CONNECTION_H_
#define ESP32

#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "SocketIOClient.h"
#include "updateProcess.h"

void initConnection();
void webSocketLoop(); 
void authentification(String tagID);
void createUserData(String data); 
bool loggedIn();
StaticJsonDocument<256> getUserData();
void getMacAddress();
int getStrength(uint8_t points);
void newWiFiConnection();
void testPost();
void sendHttpRequest(String content, String method, String target);
String readResponse();
/*String identifyUser(String content);
String createTagMsgContent(String tagsSend);*/

#endif