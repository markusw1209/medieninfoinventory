#ifndef _BEEPER_H_
#define _BEEPER_H_

#include <Arduino.h>

void initBeeper();
void beep (uint16_t durance, uint8_t number);

#endif