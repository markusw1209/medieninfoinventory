#ifndef _MAIN_H_
#define _MAIN_H_

#include "mainProcess.h"
#include "updateProcess.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
//S#include "esp_task_wdt.h"

void setup();
void loop();

#endif