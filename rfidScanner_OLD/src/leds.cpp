#include "leds.h"

LED GREEN_LED;
LED RED_LED; 
LED BLUE_LED;
LED *leds[3];

bool DEBUG_LED = false;

void initLEDs()
{
    GREEN_LED.PIN = 33;
    RED_LED.PIN = 25;
    BLUE_LED.PIN = 26;

    pinMode(RED_LED.PIN, OUTPUT);
    pinMode(BLUE_LED.PIN, OUTPUT);
    pinMode(GREEN_LED.PIN, OUTPUT);

    //LEDs in Liste zusammenfassen
    leds[0] = &GREEN_LED;
    leds[1] = &RED_LED;
    leds[2] = &BLUE_LED;

    // Alle Pins ein, um das Einschalten visuell zu bestätigen
    switchAllLEDsOn();
}





void checkLEDs()
{
    checkLED(&GREEN_LED);
    checkLED(&BLUE_LED);
    checkLED(&RED_LED);
} 





void checkLED(LED * led) 
{
    if (led->on == true) {
        if (led->onTime != 0 && (led->startOnTime + led->onTime <= millis()))
        {
            digitalWrite(led->PIN, LOW);
            led->on = false;
            if (DEBUG_LED) {
                Serial.println("Schalte LED " + String(led->PIN) + " aus.");
            }
        } else {
            digitalWrite (led->PIN, HIGH);
        }
    } else {
        digitalWrite(led->PIN, LOW);
    }
}





void blinkLED(LED * led, int onTime, int offTime)
{
    if (led->blink == true) {
        if (led->startBlinkTime + onTime > millis())
        {
            digitalWrite(led->PIN, LOW);
            led->on = false;
        } else {
            digitalWrite (led->PIN, HIGH);
        }
    }
}





void switchOn(String ledName, uint16_t time) {
    
    LED *led = NULL;
    
    if (ledName == "green") {
        led = &GREEN_LED; 
    } else if (ledName == "red") {
        led = &RED_LED;
    } else if (ledName == "blue") {
        led = &BLUE_LED;
    }

    switchOn(led, time);
}





void switchOn(LED * led, uint16_t time) 
{
    if (DEBUG_LED) {
        Serial.println("Schalte LED " + String(led->PIN) + " ein.");
    }
    led->onTime = time;
    led->startOnTime = millis();
    led->on = true;
}





void switchAllLEDsOn()
{
    if (DEBUG_LED) {
        Serial.println("Alle LEDs einschalten!");
    }
    for (int i = 0; i < 3; i++) {
        digitalWrite(leds[i]->PIN, HIGH);
        leds[i]->on = true;
        leds[i]->onTime = 0;
        if (DEBUG_LED) {
            Serial.println("Schalte PIN " + String(leds[i]->PIN) + " ein!");
        }
    }
}





void switchAllLEDsOff()
{
    for (int i = 0; i < 3; i++) {
        digitalWrite(leds[i]->PIN, LOW);
        leds[i]->on = false;
        leds[i]->onTime = 0;
    }
}
