#include "helpfulFunctions.h"

bool arraysEqual(String arr1[], String arr2[], uint16_t len1, uint16_t len2)
{
    bool equal = false;
    
        if (len1 == len2) {
            equal = true;
            for (int i = 0; i < len1; i++) {
                if(!(arr1[i] == arr2[i])) {
                    equal = false;
                }
            }
        }
    return equal;
}