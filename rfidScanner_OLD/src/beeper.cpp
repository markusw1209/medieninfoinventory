#include "beeper.h"

#define BEEPER 15

void initBeeper()
{
    pinMode(BEEPER, OUTPUT);
}

void beep (uint16_t durance, uint8_t number)
{
    for (int i = 0; i < number; i++) {
        digitalWrite(BEEPER, HIGH);
        delay(durance);
        digitalWrite(BEEPER, LOW);
        if (i != number - 1) {
            delay(durance);
        }
    }
}