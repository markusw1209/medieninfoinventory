#include "updateProcess.h"

uint32_t counterUI = 0;
bool testBool = true;

const bool DEBUG_UI = false;


void initUpdates()
{
    Serial.println("Display Initialisieren");
    initDisplay();
    displayMax("Hallo");
    Serial.println("OK.\nBeeper Initialisieren");
    initBeeper();
    Serial.println("OK.\nLEDs Initialisieren");
    initLEDs();
    Serial.println("OK.\nButtons Initialisieren");
    initButtons();
    Serial.println("OK.");
    delay(1500);
}





//Funktion für den Display-Task (analog zu loop())
void updateLoop(void * parameter) 
{
    while(true) {

        //Einstellung für funktionierendes Multithreading
        TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE;
        TIMERG0.wdt_feed = 1; 
        TIMERG0.wdt_wprotect = 0;

        checkDisplay();
        checkLEDs();
        checkButtons();
        webSocketLoop();

        if (DEBUG_UI) {
            Serial.println("Update Durchlauf " + String(++counterUI));
        }
    }
}
