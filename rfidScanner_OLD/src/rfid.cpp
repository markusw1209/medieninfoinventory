#include "rfid.h"

#define SS_PIN 4
#define RST_PIN 5

MFRC522 rfid(SS_PIN, RST_PIN);
MFRC522::MIFARE_Key key; 

String currentTag = "";
bool newTagAvailable = false;
String oldTag = "";
uint8_t nuidPICC[4];

const bool DEBUG_RFID = false;


void initRFID() 
{
    SPI.begin(); // Initialisierung des SPI-Busses
    rfid.PCD_Init(); // Initialisierung des MFRC522-Readers

    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    Serial.println("Scannen von MIFARE Classsic NUID-Tags.");
    Serial.print("Benutze den folgenden Key: ");
    printHex(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
}





String getCurrentTag()
{
    String returnTag = "";
    readTags();
    delay(5);
    if (newTagAvailable) {
        returnTag = currentTag;
    } 
    return returnTag;
}





String getLastTag()
{
    return oldTag;
}





void readTags() 
{
    if (DEBUG_RFID) {
        Serial.println("readTags() running on core " + String(xPortGetCoreID()));
    }

    newTagAvailable = false;
    
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! rfid.PICC_IsNewCardPresent())
        return;

    // Verify if the NUID has been readed
    if ( ! rfid.PICC_ReadCardSerial())
        return;

    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
    Serial.println(rfid.PICC_GetTypeName(piccType));

    // Check is the PICC of Classic MIFARE type
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
        piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
        piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("Dieses Tag ist kein MIFARE Classic Tag."));
        beep(250, 1);
        currentTag = rfid.PICC_GetTypeName(piccType);
        newTagAvailable = true;
        for (byte i = 0; i < 4; i++) {
            nuidPICC[i] = 0;
        }
        return;
    }

    if (rfid.uid.uidByte[0] != nuidPICC[0] || 
        rfid.uid.uidByte[1] != nuidPICC[1] || 
        rfid.uid.uidByte[2] != nuidPICC[2] || 
        rfid.uid.uidByte[3] != nuidPICC[3] ) {
        newTagAvailable = true;
        Serial.println(F("Neues Tag erkannt: "));
        beep(250, 1);

        // NUID in nuidPICC-Array speichern
        for (byte i = 0; i < 4; i++) {
            nuidPICC[i] = rfid.uid.uidByte[i];
        }

        currentTag = idToString(rfid.uid.uidByte, rfid.uid.size);
        Serial.println(F("Das NUID-Tag ist: "));
        Serial.print(F("In HEX: "));
        printHex(rfid.uid.uidByte, rfid.uid.size);
        Serial.println();
        Serial.print(F("In DEC: "));
        printDec(rfid.uid.uidByte, rfid.uid.size);
        Serial.println(F("\n----------------------------------------------------------\n"));
        
    } else { 
        Serial.print(F("Karte bereits gelesen."));
        Serial.println(F("\n----------------------------------------------------------\n"));
    }
}





//Speichern eines Byte-Arrays als Hex-Zahlen
void printHex(byte *buffer, byte bufferSize) 
{
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}





//Speichern eines Byte-Arrays als Dezimal-Zahlen
void printDec(byte *buffer, byte bufferSize) 
{
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], DEC);
    }
}





//Speichern des Tags aus dem Array in einen String
String idToString(byte *buffer, byte bufferSize) 
{
    String newTag = "";
    for (byte i = 0; i < bufferSize; i++) {
        newTag += String(buffer[i], HEX);
    }

    return newTag;
}
