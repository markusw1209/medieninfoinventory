#include "main.h"

//Zur Erstellung von mehreren Thraeds und "lock" von gemeinsam genutzten Variablen, um "lost updates" etc. zu vermeiden 
SemaphoreHandle_t xMutex;
TaskHandle_t mainTask = NULL;
TaskHandle_t updateTask = NULL;

void setup()
{
    //Baudrate für seriellen Monitor
    Serial.begin(115200);

    // Ausgabe des Programmstarts
    Serial.print("\n\n-----------------------------------------------------\n");
    Serial.println("Reader-Terminal gestartet\n");
    Serial.println("Beginne Setup\n");

    //Mutex erstellen, um Globale Variablen vor Zugriff von beiden Threads gleichzeitig zu schützen
    xMutex = xSemaphoreCreateMutex();

    // Für Multi-Threading: Zwei Tasks erstellen, die auf jeweils einem Prozessorkern laufen
    Serial.println("CreateTasks()");

    initUpdates(); // Setup für die Hintergrund-Updates der Daten ausführen
    initMainProcess(); // Setup für den eigentlichen Prozess ausführen

    delay(500);

    //Erstelle Task für den eigentlichen Prozessablauf im Terminal
    xTaskCreatePinnedToCore(
        mainLoop, // Funktion, in dem der Task implementiert wird (analog zu loop())
        "Main_Task", // Name des Tasks
        10000,  // Größe des Stacks
        NULL,  // Input-Parameter für den Task
        10,  // Priorität des Tasks
        &mainTask,  // Der Task-Handler
        0 // Prozessorkern, auf dem der Task laufen soll
    ); 

    //Erstelle Task für die Datenupdates im Hintergrund, die unabhängig von Stopps im Mainprozess laufen sollen
    xTaskCreatePinnedToCore(
        updateLoop, // Funktion, in dem der Task implementiert wird (analog zu loop())
        "Update_Task", // Name des Tasks
        10000,  // Größe des Stacks
        NULL,  // Input-Parameter für den Task
        1,  // Priorität des Tasks
        &updateTask,  // Der Task-Handler
        1 // Prozessorkern, auf dem der Task laufen soll
    );

    Serial.println("\nSetup abgeschlossen!\n");
    Serial.println("-----------------------------------------------------");
}





void loop()
{
    // Leer, Funktion wird für Multithreading von reader() und display() übernommen.
}