#include "display.h"

U8G2_SH1106_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
u8g2_uint_t offset;
u8g2_uint_t width;

int counter = 0;
uint32_t startTimeDisplay = 0;
uint32_t displayTime = 0;

String outputLines[4] = {"", "", "", ""};
String outputLineBig = "";
String outputLinesOld[4] = {"", "", "", ""};
String outputLineBigOld = "";
bool smallText = false;
bool displayCleared = true;

bool DEBUG_DISPLAY = false;





void initDisplay() {
    u8g2.begin();
}





void checkDisplay()
{    
    if (!displayCleared && startTimeDisplay + displayTime < millis()) {
        clearDisplay();
    }
    if (!arraysEqual(outputLines, outputLinesOld, 4, 4) || outputLineBig != outputLineBigOld) {
        updateDisplay();
    }
}





void updateDisplay() 
{
    Serial.println("updateDisplay() running on core " + String(xPortGetCoreID()));

    if (DEBUG_DISPLAY) {
        Serial.println("Update Display");
        for (int i = 0; i < 4; i++) {
            Serial.println(outputLines[i]);
        }
    }

    if (outputLineBig != outputLineBigOld) {
        displayMax(outputLineBig.c_str());

    } else {

        //Sucht die letzte nichtleere Zeile und setzt die Anzahl der leeren Zeilen auf den "Rest".
        uint8_t linesCounter = 0;
        for (int i = 0; i < 4; i++) {
            if (outputLines[i] != "") {
                linesCounter++;
            }
        }
        uint8_t emptyLines = 4 - linesCounter;

        if (emptyLines == 0) {
            displayFourLines(outputLines[0].c_str(), outputLines[1].c_str(), outputLines[2].c_str(), outputLines[3].c_str());
        } else if (emptyLines == 1) {
            if (smallText) {
                displayThreeLinesSmall(outputLines[0].c_str(), outputLines[1].c_str(), outputLines[2].c_str());
            } else {
                displayThreeLines(outputLines[0].c_str(), outputLines[1].c_str(), outputLines[2].c_str());
            }
        } else if (emptyLines == 2) {
            if (smallText) {
                displayTwoLinesSmall(outputLines[0].c_str(), outputLines[1].c_str());
            } else {
                displayTwoLines(outputLines[0].c_str(), outputLines[1].c_str());
            }
        } else if (emptyLines >= 3) {
            displayMiddle(outputLines[0].c_str());
        }
    }

    //Wenn die Displayzeit auf 0 gesetzt wurde, soll der Text bis zum expliziten Ersetzen oder Löschen auf dem Display stehen bleiben.
    if (displayTime != 0) {
        displayCleared = false;
    }
    outputLineBigOld = outputLineBig;
    for (int i = 0; i < 4; i++) {
        outputLinesOld[i] = outputLines[i];
    }
    startTimeDisplay = millis();
}





void clearDisplay() 
{
    if (DEBUG_DISPLAY) {
        Serial.println("Leere Display");
    }
    for (int i = 0; i < 4; i++) {
        outputLines[i] = "";
    }
    outputLineBig = "";

    displayCleared = true;
}





void setOutputText(String line1, String line2, String line3, String line4, uint16_t time, bool small) {

    startTimeDisplay = millis();
    displayTime = time;

    if (DEBUG_DISPLAY) {
        Serial.println("setOutputText(), displayTime = " + String(displayTime) + ", startTimeDisplay = " + String(startTimeDisplay));
    }

    outputLines[0] = line1;
    outputLines[1] = line2;
    outputLines[2] = line3;
    outputLines[3] = line4;

    smallText = small;
}





void setBigText(String text, uint16_t time) 
{
    outputLineBig = text;
    displayTime = time;
}





void displayMax(const char text[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_logisoso32_tf);
    u8g2.drawUTF8(1, 50, text);
    u8g2.nextPage();
}





void displayMiddle(const char text[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 38, text);
    u8g2.nextPage();
}





void displayTwoLines(const char line1[], const char line2[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 27, line1);
    u8g2.drawUTF8(1, 49, line2);
    u8g2.nextPage();
}





void displayTwoLinesSmall(const char line1[], const char line2[]) 
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_t0_11b_tf);
    u8g2.drawUTF8(1, 27, line1);
    u8g2.drawUTF8(1, 49, line2);
    u8g2.nextPage();
}





void displayThreeLines(const char line1[], const char line2[], const char line3[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_helvB12_tf);
    u8g2.drawUTF8(1, 19, line1);
    u8g2.drawUTF8(1, 38, line2);
    u8g2.drawUTF8(1, 57, line3);
    u8g2.nextPage();
}





void displayThreeLinesSmall(const char line1[], const char line2[], const char line3[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_t0_11b_tf);
    u8g2.drawUTF8(1, 19, line1);
    u8g2.drawUTF8(1, 38, line2);
    u8g2.drawUTF8(1, 57, line3);
    u8g2.nextPage();
}





void displayFourLines(const char line1[], const char line2[], const char line3[], const char line4[])
{
    u8g2.clearBuffer(); 
    u8g2.setFont(u8g2_font_unifont_tf);
    u8g2.drawUTF8(1, 15, line1);
    u8g2.drawUTF8(1, 30, line2);
    u8g2.drawUTF8(1, 45, line3);
    u8g2.drawUTF8(1, 60, line4);
    u8g2.nextPage();
}