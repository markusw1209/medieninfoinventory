#include "mainProcess.h"

StaticJsonDocument<256> user;
bool isAdmin = false;
int uID = 0;
String UserName;
String firstName;
String lastName;

String mode = "";
uint32_t counterBackend = 0;
String * rents;

const bool DEBUG_BACKEND = false;

void initMainProcess() 
{
    Serial.println("RFID_Reader Initialisieren");
    initRFID();
    // WiFi-Verbindung herstellen und mit grüner LED bestätigen, wenn OK
    Serial.println("OK.\n");
    initConnection();
    switchAllLEDsOff();
    switchOn("green", 0);

    //testPost();
}





//Funktion für den Reader-Task (analog zu loop())
void mainLoop(void * parameter) 
{
    while(true) {

        //Einstellung für funktionierendes Multithreading
        TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE;
        TIMERG0.wdt_feed = 1; 
        TIMERG0.wdt_wprotect = 0;

        if (uID == 0) {
            login();
        } else {
            delay(5000);
        }

        if (DEBUG_BACKEND) {
            Serial.println("Backend Durchlauf " + String(++counterBackend));
        }
    }
}





/*Angedachte Funktionen: giveBack(), rent() (Aus loop heraus je machdem, wie die Variablen gesetzt sind).*/





void login() 
{
    setOutputText("Bitte mit", "Karte einloggen!", "", "", 0, false);
    String currentTag = getCurrentTag();
    while(currentTag == "") {
        delay(50);
        currentTag = getCurrentTag();
    }
    if (!(currentTag == "")) {
        authentification(currentTag);
        while(!loggedIn()) {
            delay(25);
        }
        String userData;
        user = getUserData();
        serializeJson(user, userData);
        Serial.println("Eingeloggter User: " + userData);
        String firstNameString = user["firstName"];
        String lastNameString = user["lastName"];
        firstName = firstNameString;
        lastName = lastNameString;
        UserName = firstName + " " + lastName;
        uID = user["uID"];
        isAdmin = user["admin"];
        String adminString = isAdmin ? "Admin" : ("Nutzer " + uID);
        setOutputText("Willkommen", firstName, adminString, "", 2500, false);
    }
}
