#include "buttons.h"

#define RED_BUTTON 32
#define BLUE_BUTTON 34
#define GREEN_BUTTON 35 

bool buttonsPressed[3] = {false, false, false};
uint32_t buttonPressTimes[] = {0, 0, 0};


void initButtons() 
{
    //Pins definieren und einstellen
    pinMode(RED_BUTTON, INPUT);
    pinMode(BLUE_BUTTON, INPUT);
    pinMode(GREEN_BUTTON, INPUT);
}





bool * checkButtons()
{
    buttonsPressed[0] = digitalRead(GREEN_BUTTON);
    buttonsPressed[1] = digitalRead(BLUE_BUTTON);
    buttonsPressed[2] = digitalRead(RED_BUTTON);

    updateButtonTimes();

    return buttonsPressed;
}





bool checkButton(String buttonColor)
{
    bool buttonPressed = false;
    uint8_t button = 0;

    if (buttonColor == "green") {
        button = GREEN_BUTTON;
    } else if (buttonColor == "blue") {
        button = BLUE_BUTTON;
    } else if (buttonColor == "red") {
        button = RED_BUTTON;
    }
    
    if (button != 0) {
        buttonPressed = digitalRead(button);
    }
    return buttonPressed;
}




void updateButtonTimes() {
    for (int i = 0; i < 3; i++) {
        if (buttonsPressed[i]) {
            buttonPressTimes[i] = millis();
        } 
    }
}





uint32_t * getButtonTimes() {
     return buttonPressTimes;
}




bool buttonPressedIn(String buttonColor, uint32_t timeslot)
{
    bool buttonPressed = false;

    if (buttonColor == "green") {
        buttonPressed = (millis() - getButtonTimes()[0] <= timeslot);
    } else if (buttonColor == "blue") {
        buttonPressed = (millis() - getButtonTimes()[1] <= timeslot);
    } else if (buttonColor == "red") {
        buttonPressed = (millis() - getButtonTimes()[2] <= timeslot);
    }
    
    return buttonPressed;
}