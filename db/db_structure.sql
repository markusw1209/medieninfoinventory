-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `mac` char(17) NOT NULL,
  `access_key` varchar(256) NOT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` varchar(8) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `number_existing` mediumint(9) NOT NULL,
  `number_rent` mediumint(9) NOT NULL,
  `description` text DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `add_date` datetime NOT NULL,
  `remove_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rents_active`;
CREATE TABLE `rents_active` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `rent_date` datetime NOT NULL,
  `return_date` datetime NOT NULL,
  `extensions` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`item`),
  KEY `user` (`user`),
  CONSTRAINT `rents_active_ibfk_1` FOREIGN KEY (`item`) REFERENCES `inventory` (`id`) ON DELETE SET NULL,
  CONSTRAINT `rents_active_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rents_history`;
CREATE TABLE `rents_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `rent_date` datetime NOT NULL,
  `return_date` datetime NOT NULL,
  `extensions` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`item`),
  KEY `user` (`user`),
  CONSTRAINT `rents_history_ibfk_1` FOREIGN KEY (`item`) REFERENCES `inventory` (`id`) ON DELETE SET NULL,
  CONSTRAINT `rents_history_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` varchar(16) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `role` enum('DOZENT','STUDENT') DEFAULT NULL,
  `matrikel` varchar(8) DEFAULT NULL,
  `mail` varchar(80) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `add_date` datetime NOT NULL,
  `remove_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2020-12-17 14:11:11
