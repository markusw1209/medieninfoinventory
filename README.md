# medienInfoInventory

Implementation of an IoT-based administration system for the inventory of media informatics at the University of Hildesheim as part of a project

Description:\n
    The system uses IoT-Devices with NearField (HF-)RFID-scanners to login users with user-cards and read-in items of the inventory with rfid-tags. Normal users can rent and return items and can extend their rents. Administrators can create new users and items by scanning unknown tags. 
    The system also provides a web-frontend for the administrators to show an overview of the devices, users, inventory, active rents and rent-history of items and users. The administrators also can set user-data and item-data, can create new devices, can extend existing rents or return items in the frontend.

This repository includes

    - PlatformIO C++ code for an IoT-Device for scanning rfid-user-cards and rfid-tagged items basend on an Espressif32-chip Nodemcu-32s microcontroller in /rfidScanner,
    - Code of an OLD, faulty and incomplete version of the IoT-Device to show an approach with multitasking and SocketIO-Client,
    - nodeJS backend-code for the administration system in /backend,
    - Angular frontend-code for the administration system in /frontend,
    - a dump an diagram for the database-structure in /db,
    - Dockerfiles, a Docker-Compose-file and .env file for the deployment of the system in the '/deployment' folder.

Deployment-guide

    - Create backend-docker-image: Change directory to '/backend', execute "npm run docker-build" and wait till completion.
    - Create frontend-docker-image: Change directory to '/frontend', execute again "npm run docker-build" and wait again to completion.
    - Set configuration data for docker-containers: Open file '/.env' an set the data. You can set ports, the API-URL, db-data and some other values.
    - Set up the database: create a folder db_entrypoint next to the docker-compose.yml. Then, you can copy one of the .sql-files from the '/db'-folder into it. The db_structure.sql-file creates an empty database with the correct structure, the quick_start_setup.sql-file additionally adds the first admin with username 'test.user' and password 'MedienInfo123' to the data. Trust me: You want the quick-start-file!
    - Start Containers: Change directory to '/' and execute "docker-compose up". If everything is set up right, the application starts.
    - Database Access: You can access an adminer Frontend to set up your database. Open http://YOUR-IP-ADDRESS:ADMINER_PORT and login with the set db-credetials from .env. 
    - Only if you have chosen the empty database structure, you have to add your first admin to get access to the web frontend: You must create one user and one admin (bind to user by user-foreign-key). Be careful: The password is a salted and hashed password by bcryptjs. You can use the program 'genKey.js' in '/backend/helpful/' folder to hash your password.
    - You have now access to the web-frontend with http://YOUR-IP-ADDRESS:FRONTEND_PORT and you can add devices, which you can use to add new items, users etc.
    - Have fun with the Application!!!

Docker-Images to TAR-Files:\n
    To pass on the created docker-images, you can 'pack' them into tar-files. For that change into the directory of the image '/backend' or '/frontend' and execute 'npm run docker-save'. The File will be created and stored in the '/'-directory. 
    After this, you can portate the file to an other host an load it there into docker by executing 'docker load -i FILENAME.tar'. Then, you can use the image.

Some usesd Hints:\n
    - Set Env-Variables in Angular: https://pumpingco.de/blog/environment-variables-angular-docker/
    - Create Dockerfile for Angular App: https://wkrzywiec.medium.com/build-and-run-angular-application-in-a-docker-container-b65dbbc50be8
    - Create Dockerfile for Backend: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
    - (Cross-)Validation for FormGroups: https://angular.io/guide/form-validation
    - Docker save and load: https://stackoverflow.com/questions/23935141/how-to-copy-docker-images-from-one-host-to-another-without-using-a-repository
    - Set user credentials in MariaDB: https://www.techonthenet.com/mariadb/grant_revoke.php

Create SQL-Dump: sudo docker exec medieninfo_db mysqldump --user root --password=MedienInfoInventory123 medieninfo_inventory > ./quick_start_setup.sql
Load SQL-Dump: sudo docker exec -i medieninfo_db mysql --user root --password=MedienInfoInventory123 medieninfo_inventory < ./quick_start_setup.sql